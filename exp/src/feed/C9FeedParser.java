package feed;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import feed.CommonFeedParser;
import feed.FeedParser;
import feed.KeyValuePair;

public class C9FeedParser {

  public void parse(String feedContent) throws CommonException {
    FeedParser feedParser = new CommonFeedParser();
    parse(feedParser, feedContent);
  }

  public List<KeyValuePair> parse(String url, String feedContent) throws CommonException {
    FeedParser feedParser = null;
    if (url.contains("jobscore.com")) {
      // remove it
    } else {
      feedParser = new CommonFeedParser();
    }
    return parse(feedParser, feedContent);
  }

  public List<KeyValuePair> parse(FeedParser feedParser, String feedContent) throws CommonException {
    InputStream is = null;
    try {
      is = new ByteArrayInputStream(feedContent.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    if (is != null) {
      List<KeyValuePair> results = feedParser.parseFeed(is);
      return results;
    }
    return null;
  }
}
