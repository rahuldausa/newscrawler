package feed;

import java.io.InputStream;
import java.util.List;

public interface FeedParser {

  public List<KeyValuePair> parseFeed(InputStream is) throws CommonException;
}
