package com.rujhaan.news.email;
import java.util.Properties;

import org.apache.commons.mail.EmailException;
import org.junit.Test;

/**
 * 
 * @author Rahul Jain
 *
 */
public class EmailSenderTest {

    @Test
    public void send() {
      
/*      Properties props = new Properties();
      props.put("mail.smtp.auth", "true");
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.host", host);
      props.put("mail.smtp.port", "587");*/
      
        EmailConfig config = new EmailConfig();
//        config.setHostName("smtp.gmail.com");// casarray01.ivycomptech.partygaming.local
//        config.setSmtpPort(587);
//        config.setFrom("dynamicrahul2020@gmail.com");
//        config.setTo(new String[]{"dynamicrahul2020@gmail.com"});
//        config.setSubject("Test message");
//        config.setMessage("Test message123");
//        config.setUsername("dynamicrahul2020@gmail.com");
//        config.setPassword("9414284044");
//        config.setDebug(true);
//        config.setTls(true);
        
        config.setHostName("smtp.fashionworldqueen.com");// casarray01.ivycomptech.partygaming.local
        config.setSmtpPort(587);
        config.setFrom("support@rujhaan.com");
        config.setFromName("Rujhaan.com");
        config.setTo(new String[]{"dynamicrahul2020@gmail.com","eranshu19@yahoo.com"});
        config.setSubject("Test message");
        config.setMessage("Test message123");
        config.setUsername("support@fashionworldqueen.com");
        config.setPassword("040AC8a1");
        config.setDebug(true);
        config.setTls(true);
        
//        String keystorePath = "d:\\work\\jssecacerts";
//
//        System.setProperty("javax.net.ssl.keyStore", keystorePath);
//        System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
//        System.setProperty("javax.net.ssl.trustStore", keystorePath);
//        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        EmailSender sender = new EmailSender(config);
        try {
            sender.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }
}
