package com.rujhaan.news.email;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

import com.career9.c9news.FileUtil;

/**
 * 
 * @author Rahul Jain
 * 
 */
public class EmailSender {

    EmailConfig config;
    public EmailSender(EmailConfig config) {
        this.config = config;
    }

    public void send() throws EmailException {
        Email email = null;
        try {
          email = createEmailMessage();
        } catch (IOException e) {
          e.printStackTrace();
        }
        email.send();
        System.out.println("Email sent successfully");
    }

    public void sendAttachement() {

    }

    public void sendHtmlEmail() {

    }

    private Email createEmailMessage() throws EmailException, IOException {
      HtmlEmail email = new HtmlEmail();
        email.setHostName(config.getHostName());
        email.setSmtpPort(config.getSmtpPort());
        email.setAuthentication(config.getUsername(), config.getPassword());
        email.setTLS((config.isTls() != null) ? config.isTls() : true);
        email.setFrom(config.getFrom(), config.getFromName());
        email.setSubject(config.getSubject());
        email.setMsg(config.getMessage());
      /*  email.setSSL(false);
        email.setTLS(true);*/
        //email.setTLS(config.getTls());
        email.setDebug(config.isDebug());
        
        email.setHtmlMsg(FileUtil.contentAsString(new File("/Users/ivy4488/Documents/d/work/c9news/Temp.html")));
        System.out.println("Sending email to " + Arrays.asList(config.getTo()));
        for (String to : config.getTo()) {
            email.addTo(to);
        }
        return email;
    }
}
