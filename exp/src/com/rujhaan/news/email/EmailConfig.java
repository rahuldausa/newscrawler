package com.rujhaan.news.email;
/**
 * 
 * @author Rahul Jain
 *
 */
public class EmailConfig {

	private String hostName;
	private int smtpPort = 25;
	private String username;
	private String password;
	private Boolean tls;
	private String from;
	private String fromName;
	private String[] to;
	private String[] cc;
	private String[] bcc;
	private String subject;
	private String message;
	private boolean debug;

	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public int getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean isTls() {
		return tls;
	}
	public void setTls(Boolean tls) {
		this.tls = tls;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String[] getTo() {
		return to;
	}
	public void setTo(String[] to) {
		this.to = to;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String[] getCc() {
		return cc;
	}
	public void setCc(String[] cc) {
		this.cc = cc;
	}
	public String[] getBcc() {
		return bcc;
	}
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}
    public boolean isDebug() {
        return debug;
    }
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
    public String getFromName() {
      return fromName;
    }
    public void setFromName(String fromName) {
      this.fromName = fromName;
    }

}
