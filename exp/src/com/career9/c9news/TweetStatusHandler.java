package com.career9.c9news;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;

import twitter4j.Status;
import twitter4j.URLEntity;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.news.impl.dao.DBMapper;
import com.rujhaan.news.impl.dao.MongoDao;
import com.sree.textbytes.jtopia.NewsTermsExtractor;

public class TweetStatusHandler {

  final DBMapper dbMapper;
  final MongoDao mongoDao;
  final static Set<String> spamAdultKeywords = new HashSet<>();
  final static Set<String> filterKeywords = new HashSet<>();
  final static Set<String> spammyScreenNames = new HashSet<>();

  private NewsClassifierV2 classifier;
  private LocationClassifier locationClassifier;

  private SolrConnector connector;
  private NewsTermsExtractor newsTermExtractor;
  private NERExtractor nerExtractor;

  public TweetStatusHandler() {
    dbMapper = new DBMapper();
    mongoDao = new MongoDao("23.227.177.112", "tweetFeed");
    //mongoDao = new MongoDao("cctweetFeed");
    
    mongoDao.addIndex(new BasicDBObject("tweet_followers_count", -1));//descending
    mongoDao.addIndex(new BasicDBObject("retweetCount", -1));//descending

    connector = new SolrConnector();
    if (connector != null) {
      connector.open("http://23.227.177.112:8983/solr/newstweet");
      //connector.open("http://localhost:8983/solr/cctweet");
      //connector.open("http://localhost:8983/solr/newstweet");
    }
    nerExtractor = new NERExtractor();

    classifier = new NewsClassifierV2();
    if (classifier != null) {
      try {
        classifier.trainAndLoadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }
    
    locationClassifier = new LocationClassifier();
    if (locationClassifier != null) {
      try {
        locationClassifier.loadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }

    newsTermExtractor = new NewsTermsExtractor();

    List<String> temp = FileUtil.loadResource(TwitterStreamingApiTest.class,
        "spam_adult_keywords.txt", false);

    for (String s : temp) {
      s = s.trim();
      spamAdultKeywords.add(s.toLowerCase());
    }

    // commentAware should be false otherwise it will ingore the hashTags to be filtered
    temp = FileUtil.loadResource(TwitterStreamingApiTest.class, "filter_keywords.txt", false);

    for (String s : temp) {
      s = s.trim();
      filterKeywords.add(s.toLowerCase());
    }
    temp = FileUtil.loadResource(TwitterStreamingApiTest.class, "spammy_screennames.txt", false);

    for (String s : temp) {
      s = s.trim();
      spammyScreenNames.add(s.toLowerCase());
    }
    // System.out.println("spamAdultKeywords:"+spamAdultKeywords);
  }

  public void handle(Status status, boolean folowersCheck) {
    handle(status, folowersCheck, false);
  }
  
  public static final String TWEET_PATTERN_STR = "http://t.co/[a-zA-Z0-9]*";
  public static final Pattern TWEET_PATTERN = Pattern.compile(TWEET_PATTERN_STR);
  
  public void handle(Status status, boolean folowersCheck, boolean uniqueCheck) {
    TweetRecord tweet = new TweetRecord();
    if (isRetweet(status)) {
      //System.out.println("seems to be a retweet:" + status.getText());
      return;
    }
    if (folowersCheck) {
      tweet.userFollowersCount = status.getUser().getFollowersCount();
      if (tweet.userFollowersCount < 50) {
        System.out.println("Followers less than 250" + status.getText());
        return;
      }
    }
    if (isSpam(status)) {
      //System.out.println("seems to be a spam:" + status.getText());
      return;
    }
    
    if (isSpammyTweet(status)) {
      //System.out.println("seems to be for filtering:" + status.getText());
      return;
    }
    
    String text = status.getText();
    
    if (haveFilterKeyword(text)) {
      //System.out.println("seems to be for filtering:" + status.getText());
      return;
    }
    
    tweet.id = status.getId();
    
    if (uniqueCheck) {
      DBObject q = new BasicDBObject("tweet_ID", tweet.id);
      if (mongoDao.exists(q)) {
        return;
      }
    }
    
    tweet.userName = status.getUser().getName();
    tweet.screenName = status.getUser().getScreenName();
    tweet.timestamp = status.getCreatedAt().getTime();

    if (text != null) {
      text= text.replaceAll("[^\\x00-\\x7f]", " ");
      text = text.replaceAll("\u00a0"," ");
      text = text.replaceAll("\\s+", " ");
    }

    // TODO: to remove
    if (text.length() < 50) {
      System.out.println("dropping less length id:");
      return;
    }
    
    tweet.text = text;
    
    if (status.getRetweetedStatus() != null) {
      tweet.retweetCount = status.getRetweetedStatus().getRetweetCount();
    }
    if (status.getMediaEntities().length > 0) {
      tweet.mediaURL = status.getMediaEntities()[0].getMediaURL();
    }
    if (status.getPlace() != null) {
      tweet.countryCode = status.getPlace().getCountryCode();
    }

    if (status.getURLEntities().length > 0) {
      tweet.urlLinks = new ArrayList<>();
      for (URLEntity ue : status.getURLEntities()) {
        tweet.urlLinks.add(ue.getURL());
      }
    }
    //status.getURLEntities()[0].getStart()
    if (status.getUser() != null) {
      tweet.userProfileImageURL = status.getUser().getProfileImageURL();
      tweet.userMiniProfileImageURL = status.getUser().getMiniProfileImageURL();
      // tweet.userURL = status.getUser().getURL();// for temporary commenting it
      tweet.userId = status.getUser().getId();
      // tweet.statusURL=
    }
    
    if (tweet.countryCode != null && tweet.countryCode.trim().length() > 0) {
      String countryCode = tweet.countryCode;
      String country = null;
      if (countryCode.equals("IN")) {
        country = "india";
      } else if (countryCode.equals("US")) {
        country = "usa";
      } else if (countryCode.equals("UK")) {
        country = "uk";
      } else {
        country = "world";// VERYIMP: TODO: remember to change this to
                          // appropriate country
      }
      tweet.country = country;
      System.out.println("Found: "+country+"-->"+tweet.text);
    }
    
    if (classifier != null) {
      try {
        tweet.category = classifier.classify(tweet.text);
        System.out.println("Classified as :" + tweet.category + "===>" + tweet.text);
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    }
    
    // if available from twitter than pick that.
    if (tweet.country==null && locationClassifier != null) {
      try {
        tweet.country = locationClassifier.classify(tweet.text);
        System.out.println("Classified as :" + tweet.country + "===>" + tweet.text);
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    }
    
    tweet.cleanedText = tweet.text;
    if (tweet.mediaURL != null) {
      Matcher m = TWEET_PATTERN.matcher(tweet.text);
      while (m.find()) {
        String s = m.group();
        tweet.cleanedText = tweet.cleanedText.replace(s, "");
      }
      tweet.cleanedText = tweet.cleanedText.replaceAll("\"", "\\\"");
      tweet.cleanedText = tweet.cleanedText.replaceAll("#", "");
    }

/*    Map<String, ArrayList<Integer>> list = newsTermExtractor.topics(tweet.text);
    if (list != null && !list.isEmpty()) {
      Set<String> set = list.keySet();
      tweet.keywords = set.toArray(new String[]{});
    }*/
    
    Set<String> keywords = new HashSet<>();
    
    Map<String, Map<String, Integer>> entityMap = nerExtractor.extractEntities(tweet.text);
    if (entityMap != null && !entityMap.isEmpty()) {
      for (String k : entityMap.keySet()) {
        Map<String, Integer> im = entityMap.get(k);
        keywords.addAll(im.keySet());
      }
    }
    
    Map<String, ArrayList<Integer>> list = newsTermExtractor.topics(tweet.text);
    if (list != null && !list.isEmpty()) {
      keywords.addAll(list.keySet());
    }
    
    tweet.keywords = keywords.toArray(new String[]{});
    
    
    DBObject dbObj = dbMapper.fromTweet(tweet);
    mongoDao.insert(dbObj);
    
    // Index into solr
    index(tweet);

    if (status.getMediaEntities().length > 0) {
      if (status.getRetweetedStatus() != null) {
        System.out.println(status.getUser().getName() + " : "
            + status.getRetweetedStatus().getRetweetCount() + " : " + status.getText() + " : "
            + status.getMediaEntities()[0].getMediaURL());
      } else {
        System.out.println(status.getUser().getName() + " : " + status.getRetweetCount() + " : "
            + status.getText() + " : " + status.getMediaEntities()[0].getMediaURL());
      }
    } else {
      if (status.getRetweetedStatus() != null) {
        System.out.println(status.getUser().getName() + " : "
            + status.getRetweetedStatus().getRetweetCount() + " : " + status.getText() + " : "
            + status.getMediaEntities());
      } else {
        System.out.println(status.getUser().getName() + " : " + status.getRetweetCount() + " : "
            + status.getText() + " : " + status.getMediaEntities());
      }
    }
  }
  
  private boolean isSpammyTweet(Status status) {
    String text = status.getText();
    return isSpammyContent(text);
  }

  private static boolean isSpammyContent(String text) {
    if (text.contains("#")) {
      int numTimes = 0;
      for (char c : text.toCharArray()) {
        if (c == '#') {
          //System.out.println("c:"+c);
          ++numTimes;
        }
      }
      if (numTimes > 3) {
        return true;
      }
    }
    return false;
  }
  
  private static AtomicInteger counter = new AtomicInteger();

  private void index(TweetRecord tweet) {
    if (connector != null) {
      try {
        counter.incrementAndGet();
        SolrInputDocument solrDoc = SolrDocMapper.tweetRecordAsSolrDoc(tweet);
        connector.addDocument(solrDoc);
      } catch (SolrServerException | IOException e) {
        e.printStackTrace();
      }
      if (counter.get() % 100 == 0) {
        try {
          connector.commit();
        } catch (IndexingException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private boolean isRetweet(Status status) {
    if (status.isRetweet()) {
      return true;
    }
    String text = status.getText();
/*    if (text.startsWith("RT @")) {
      return true;
    }*/
    //Mmmm # RT @Czar_Mo: 
    if (text.contains("RT @")) {
      return true;
    }
    return false;
  }
  
  private boolean isSpam(Status status) {
    String text = status.getText();
    String textC = text.toLowerCase();
    for (String keyword : spamAdultKeywords) {
      // if it contains spam or adult keyword ignore it
      if (textC.contains(keyword)) {
        return true;
      }
    }
    
    // if it is a spammy screen name, then ignore it
    String screenName = status.getUser().getScreenName();
    for (String name : spammyScreenNames) {
      if (screenName.equals(name)) {
        return true;
      }
    }
    
    // to check the screen names if they have adult content
    String screenName_C = screenName.toLowerCase();
    for (String keyword : spamAdultKeywords) {
      // if it contains spam or adult keyword ignore it
      if (screenName_C.contains(keyword)) {
        return true;
      }
    }
    
    return false;
  }
  
  private boolean haveFilterKeyword(String text) {
    String textC = text.toLowerCase();
    //System.out.println("filterKeywords:"+filterKeywords);
    for (String keyword : filterKeywords) {
      // if it contains spam or adult keyword ignore it
      //System.out.println(keyword);
      if (textC.contains(keyword)) {
        return true;
      }
    }
    return false;
  }
  
  public static void main(String[] args) {
    String text = "#fashion #fashionblog #beauty #moda #look #outfit #trendy #tendencia #joyas #jewellery http://t.co/m1nUQrwPoM";
    text = "#Kindle #Book #695 No Tildes on TuesdayCherrye S. Vasquez Ph.D. (Author)(23)Download: $... http://t.co/ZK8C3xyxyy";
    System.out.println("Output :"+TweetStatusHandler.isSpammyContent(text));
    TweetStatusHandler ts = new TweetStatusHandler();
    System.out.println("Output :"+ts.haveFilterKeyword(text));
  }
}
