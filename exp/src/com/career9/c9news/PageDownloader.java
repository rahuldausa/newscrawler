package com.career9.c9news;
/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpStatus;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.fetcher.PageFetchResult;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.parser.ParseData;
import edu.uci.ics.crawler4j.parser.Parser;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * This class is a demonstration of how crawler4j can be used to download a single page and extract its title and text.
 */
public class PageDownloader {

  private Parser parser;
  private C9PageFetcher pageFetcher;
  
  //https://twitter.com/PlayStationEU/status/494115634393788416    
  public static final String TWITTER_STATUS_PATTERN_STR = "https://twitter.com/(.*)/status/(.*)";

  public static Pattern TWITTER_STATUS_PATTERN = Pattern.compile(TWITTER_STATUS_PATTERN_STR);
  
  private CrawlConfig config;
  
  public PageDownloader() {
    config = new CrawlConfig();
    //config.setProxyHost("localhost");
    //config.setProxyPort(3128);
    //config.setProxyPort(5865);
    config.setConnectionTimeout(60000);
    config.setSocketTimeout(120000);
    config.setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36");
    config.setIncludeHttpsPages(true);
    config.setCrawlStorageFolder("/opt/crawl/");
    config.setResumableCrawling(true);
    config.setFollowRedirects(true);
    config.setPolitenessDelay(1000);//recently added
    parser = new Parser(config);
    pageFetcher = new C9PageFetcher(config);
  }
  
  public void setCrawlStorageFolder(String dir){
    config.setCrawlStorageFolder(dir);
  }

  public Page download(String url) {
    WebURL curURL = new WebURL();
    curURL.setURL(url);
    PageFetchResult fetchResult = null;
    try {
      Matcher m = TWITTER_STATUS_PATTERN.matcher(curURL.getURL());
      if(m.find()){
        return null;
      }
      fetchResult = pageFetcher.fetchHeader(curURL);
      if (fetchResult != null && fetchResult.getResponseHeaders()!=null) {
        for (Header header : fetchResult.getResponseHeaders()) {
          //System.out.println("header:" + header.getName() + "=>" + header.getValue());
        }
      }
      if (fetchResult.getMovedToUrl()!=null) {
        Matcher m1 = TWITTER_STATUS_PATTERN.matcher(fetchResult.getMovedToUrl());
        if(m1.find()){
          return null;
        }
        System.out.println("found url:" + fetchResult.getMovedToUrl());
        // dont fetch this page and return
        return null;
      }
      System.out.println("http status:"+fetchResult.getStatusCode());
      if (fetchResult.getStatusCode() == HttpStatus.SC_OK) {
        try {
          Page page = new Page(curURL);
          fetchResult.fetchContent(page);
          if(page==null){
            System.out.println("page is null:");
          }
          return page;//my changes
  /*        if (parser.parse(page, curURL.getURL())) {
            return page;
          }*/
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } finally {
      if (fetchResult != null) {
        fetchResult.discardContentIfNotConsumed();
      }
    }
    return null;
  }
  
  public boolean isBlackListed(String href) {
    if (href.contains("news.yahoo.com")) {
      return true;
    }

    if (href.contains("portsmouth.co.uk") || href.contains("oak.ctx.ly") || href.contains("msnbc.com")
        || href.contains("indiatoday.intoday.in/gallery/") || href.contains("indiatoday.intoday.in/video/")) {
      return true;
    }
    return false;
  }

  public Page processUrl(String url) {
    System.out.println("Processing: " + url);
    String temp = null;
    try {
      temp = expandUrl(url);
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Resolved url: " + url);
    if (temp != null) {
      url = temp;
    } else {
      // otherwise continue with the incoming url as that may not have a redirect url
    }
    
    // try to ignore again.
    if(isBlackListed(url)){
      return null;
    }

    Page page = download(url);
    
    // We have downloaded that page, but still ignore, as we know that its junk.
    if (page != null && page.getWebURL() != null) {
      if (isBlackListed(page.getWebURL().getURL())) {
        System.out.println("Marking blancklisted:"+page.getWebURL().getURL());
        return null;
      }
    }
    
    if (page != null) {
      ParseData parseData = page.getParseData();
      if (parseData != null) {
        if (parseData instanceof HtmlParseData) {
          HtmlParseData htmlParseData = (HtmlParseData) parseData;
          System.out.println("Title: " + htmlParseData.getTitle());
          System.out.println("Text length: " + htmlParseData.getText().length());
          System.out.println("Html length: " + htmlParseData.getHtml().length());
          //System.out.println("Html length: " + htmlParseData.getHtml());
        }
      } else {
        //System.out.println("Couldn't parse the content of the page.");
      }
    } else {
      System.out.println("Couldn't fetch the content of the page.");
    }
    System.out.println("==============");
    return page;
  }
  
  public static String expandUrl(String shortenedUrl) throws IOException {
    URL url = new URL(shortenedUrl);    
    // open connection
    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection(Proxy.NO_PROXY); 
    
    // stop following browser redirect
    httpURLConnection.setInstanceFollowRedirects(false);
     
    // extract location header containing the actual destination URL
    String expandedURL = httpURLConnection.getHeaderField("Location");
    httpURLConnection.disconnect();
     
    return expandedURL;
}

  public static void main(String[] args) {
    PageDownloader downloader = new PageDownloader();
    //downloader.processUrl("http://en.wikipedia.org/wiki/Main_Page/");
    //downloader.processUrl("http://www.yahoo.com/");
    String url="http://politi.co/1smrpx9";
    url="http://www.slideshare.net/rahuldausa/introduction-to-machine-learning-38791937";

    Page page = downloader.processUrl(url);
    System.out.println(new String(page.getContentData()));

    //downloader.processUrl("https://twitter.com/PlayStationEU/status/494115634393788416");

  }
}