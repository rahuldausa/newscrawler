package com.career9.c9news;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.*;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations;

import java.util.List;


/** This is a demo of calling CRFClassifier programmatically.
 *  <p>
 *  Usage: {@code java -mx400m -cp "stanford-ner.jar:." NERDemo1 [serializedClassifier [fileName]] }
 *  <p>
 *  If arguments aren't specified, they default to
 *  classifiers/english.all.3class.distsim.crf.ser.gz and some hardcoded sample text.
 *  <p>
 *  To use CRFClassifier from the command line:
 *  </p><blockquote>
 *  {@code java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier [classifier] -textFile [file] }
 *  </blockquote><p>
 *  Or if the file is already tokenized and one word per line, perhaps in
 *  a tab-separated value format with extra columns for part-of-speech tag,
 *  etc., use the version below (note the 's' instead of the 'x'):
 *  </p><blockquote>
 *  {@code java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier [classifier] -testFile [file] }
 *  </blockquote>
 *
 *  @author Jenny Finkel
 *  @author Christopher Manning
 */

public class NERDemo1 {

  public static void main(String[] args) throws Exception {

    String serializedClassifier = "D:/ProjectsHub/stanford-ner-2014-06-16/classifiers/english.all.3class.distsim.crf.ser.gz";

    args = new String[2];
    args[0] = serializedClassifier;
    args[1]="D:/work/c9news/text_data1/bangalore/timesofindia.feedsportal.com-1799aedd6d3649d970492260ac9aab95";
    
    if (args.length > 0) {
      serializedClassifier = args[0];
    }

    AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);

    /* For either a file to annotate or for the hardcoded text example,
       this demo file shows two ways to process the output, for teaching
       purposes.  For the file, it shows both how to run NER on a String
       and how to run it on a whole file.  For the hard-coded String,
       it shows how to run it on a single sentence, and how to do this
       and produce an inline XML output format.
    */
    if (args.length > 1) {
      String fileContents = IOUtils.slurpFile(args[1]);
      List<List<CoreLabel>> out = classifier.classify(fileContents);
      for (List<CoreLabel> sentence : out) {
        for (CoreLabel word : sentence) {
          String  type =  word.get(CoreAnnotations.AnswerAnnotation.class);
          if (type.equals("LOCATION") || type.equals("PERSON") || type.equals("ORGANIZATION")) {
            System.out.print(word.word() + '/' + type + ' ');
          }
        }
        System.out.println();
      }
//      System.out.println("---");
//      out = classifier.classifyFile(args[1]);
//      for (List<CoreLabel> sentence : out) {
//        for (CoreLabel word : sentence) {
//          System.out.print(word.word() + '/' + word.get(CoreAnnotations.AnswerAnnotation.class) + ' ');
//        }
//        System.out.println();
//      }

    } else {
//      String[] example = {"Good afternoon Rajat Raina, how are you today?",
//                          "I go to school at Stanford University, which is located in California." };
//      
      String[] example = {"Mumbai: Union Minister of Road Transport Nitin Gadkari on Tuesday congratulated Devendra Fadvanis after he was unanimously elected as the leader of the BJP legislature party in Maharashtra. Fadvanis is now officially appointment as the first BJP chief minister of the state. “Am sure that under Devendra Fadnavis Maharashtra will get a new direction, will develop under his leadership,” Gadkari told reporters here. On Sunday, the BJP had announced that the party was willing to form the government in Maharashtra in alliance with the Shiv Sena."};
      for (String str : example) {
        System.out.println(classifier.classifyToString(str));
      }
      System.out.println("---");

      for (String str : example) {
        // This one puts in spaces and newlines between tokens, so just print not println.
        System.out.print(classifier.classifyToString(str, "slashTags", false));
      }
      System.out.println("---");

      for (String str : example) {
        System.out.println(classifier.classifyWithInlineXML(str));
      }
      System.out.println("---");

      for (String str : example) {
        System.out.println(classifier.classifyToString(str, "xml", true));
      }
      System.out.println("---");

      int i=0;
      for (String str : example) {
        for (List<CoreLabel> lcl : classifier.classify(str)) {
          for (CoreLabel cl : lcl) {
            System.out.print(i++ + ": ");
            System.out.println(cl.toShorterString());
          }
        }
      }
      
      for (String str : example) {
        List<List<CoreLabel>> labels = classifier.classify(str);
        for (List<CoreLabel> list : labels) {
          for (CoreLabel coreLabel : list) {
            System.out.println(coreLabel.ner() + "---"+coreLabel.word());
          }
        }
      }
    }
  }

}
