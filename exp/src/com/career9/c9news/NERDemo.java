package com.career9.c9news;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;


/** This is a demo of calling CRFClassifier programmatically.
 *  <p>
 *  Usage: {@code java -mx400m -cp "stanford-ner.jar:." NERDemo [serializedClassifier [fileName]] }
 *  <p>
 *  If arguments aren't specified, they default to
 *  classifiers/english.all.3class.distsim.crf.ser.gz and some hardcoded sample text.
 *  <p>
 *  To use CRFClassifier from the command line:
 *  </p><blockquote>
 *  {@code java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier [classifier] -textFile [file] }
 *  </blockquote><p>
 *  Or if the file is already tokenized and one word per line, perhaps in
 *  a tab-separated value format with extra columns for part-of-speech tag,
 *  etc., use the version below (note the 's' instead of the 'x'):
 *  </p><blockquote>
 *  {@code java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier [classifier] -testFile [file] }
 *  </blockquote>
 *
 *  @author Jenny Finkel
 *  @author Christopher Manning
 */

public class NERDemo {

  public static void main(String[] args) throws Exception {

    String serializedClassifier = "classifiers/english.all.3class.distsim.crf.ser.gz";
    
    serializedClassifier="classifiers/english.nowiki.3class.distsim.crf.ser.gz";
    

    if (args.length > 0) {
      serializedClassifier = args[0];
    }

    AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);

    /* For either a file to annotate or for the hardcoded text example,
       this demo file shows two ways to process the output, for teaching
       purposes.  For the file, it shows both how to run NER on a String
       and how to run it on a whole file.  For the hard-coded String,
       it shows how to run it on a single sentence, and how to do this
       and produce an inline XML output format.
    */
    if (args.length > 1) {
      String fileContents = IOUtils.slurpFile(args[1]);
      List<List<CoreLabel>> out = classifier.classify(fileContents);
      for (List<CoreLabel> sentence : out) {
        for (CoreLabel word : sentence) {
          System.out.print(word.word() + '/' + word.get(CoreAnnotations.AnswerAnnotation.class) + ' ');
        }
        System.out.println();
      }
      System.out.println("---");
      out = classifier.classifyFile(args[1]);
      for (List<CoreLabel> sentence : out) {
        for (CoreLabel word : sentence) {
          System.out.print(word.word() + '/' + word.get(CoreAnnotations.AnswerAnnotation.class) + ' ');
        }
        System.out.println();
      }

    } else {
      //String[] example = {"Good afternoon Rajat Raina, how are you today?",
       //                   "I go to school at Stanford University, which is located in California." };
//      String[] example= {
//          "Taney Dragons Little League world series pitcher Mo'ne Davis throws the ceremonial first pitch before the MLB game between the Washington Nationals and the Los Angeles Dodgers at Dodger Stadium. (Kirby Lee / USA Today Sports / September 3, 2014)",
//          "Appearing at a WNBA luncheon Friday in New York City, Little League star Mo'ne Davis admitted she's \"sad\" about the NCAA reprimanding UConn with a secondary rules violation for Geno Auriemma's congratulatory phone call to her during the World Series.",
//"I heard about that this morning, and it's sad,\" Davis, 13, told reporters at the luncheon. \"There wasn't anything about recruiting in the call, he was just congratulating me.","Davis was a guest of the league at its annual luncheon honoring retiring player and future San Antonio Spurs assistant coach Becky Hammon, retiring Indiana coach Lin Dunn and political strategist Donna Brazile."
//,"Banks Planning To Leave Scotland If It Votes For Independence on Sept. 18. abandon the country for Britain. http://t.co/lb7zdZRNFO", "Why did you build Rao Tula Ram as a single flyover? asks High Court to PWD http://t.co/XieIzBzQkz"
//
//      };
      
      String[] example = {"Mumbai: Union Minister of Road Transport Nitin Gadkari on Tuesday congratulated Devendra Fadvanis after he was unanimously elected as the leader of the BJP legislature party in Maharashtra. Fadvanis is now officially appointment as the first BJP chief minister of the state. “Am sure that under Devendra Fadnavis Maharashtra will get a new direction, will develop under his leadership,” Gadkari told reporters here. On Sunday, the BJP had announced that the party was willing to form the government in Maharashtra in alliance with the Shiv Sena."};


      for (String str : example) {
        System.out.println(classifier.classifyToString(str));
      }
      System.out.println("---");

/*      for (String str : example) {
        // This one puts in spaces and newlines between tokens, so just print not println.
        System.out.print(classifier.classifyToString(str, "slashTags", false));
      }*/
      System.out.println("---");

      for (String str : example) {
        System.out.println(classifier.classifyWithInlineXML(str));
        System.out.println(extractEntities(classifier, str));
      }
      System.out.println("---");

/*      for (String str : example) {
        System.out.println(classifier.classifyToString(str, "xml", true));
      }
      System.out.println("---");

      int i=0;
      for (String str : example) {
        for (List<CoreLabel> lcl : classifier.classify(str)) {
          for (CoreLabel cl : lcl) {
            System.out.print(i++ + ": ");
            System.out.println(cl.toShorterString());
          }
        }
      }*/
    }
  }
  
  public static HashMap<String, HashMap<String, Integer>> extractEntities(
      AbstractSequenceClassifier<CoreLabel> classifier, String text) {

    HashMap<String, HashMap<String, Integer>> entities = new HashMap<String, HashMap<String, Integer>>();

    for (List<CoreLabel> lcl : classifier.classify(text)) {

      Iterator<CoreLabel> iterator = lcl.iterator();

      if (!iterator.hasNext()) continue;

      CoreLabel cl = iterator.next();

      while (iterator.hasNext()) {
        String answer = cl.getString(CoreAnnotations.AnswerAnnotation.class);

        if (answer.equals("O")) {
          cl = iterator.next();
          continue;
        }

        if (!entities.containsKey(answer)) entities.put(answer, new HashMap<String, Integer>());

        String value = cl.getString(CoreAnnotations.ValueAnnotation.class);

        while (iterator.hasNext()) {
          cl = iterator.next();
          if (answer.equals(cl.getString(CoreAnnotations.AnswerAnnotation.class)))
            value = value + " " + cl.getString(CoreAnnotations.ValueAnnotation.class);
          else {
            if (!entities.get(answer).containsKey(value)) entities.get(answer).put(value, 0);

            entities.get(answer).put(value, entities.get(answer).get(value) + 1);

            break;
          }
        }
        if (!iterator.hasNext()) break;
      }
    }
    return entities;
  }

}
