package com.career9.c9news;

import java.util.Set;

import org.apache.solr.common.SolrInputDocument;

import com.rujhaan.news.domain.NewsFeed;
import com.rujhaan.news.domain.TweetRecord;

public class SolrDocMapper {

  public static SolrInputDocument toSolrDoc(NewsFeed feed) {
    SolrInputDocument doc = new SolrInputDocument();

    doc.addField("id", feed.getId());
    //doc.addField("source_ss", feed.getSource());
    doc.addField("title_ss", feed.getTitle());
    doc.addField("summary_fss", feed.getSummary());
    doc.addField("link_ss", feed.getLink());
    doc.addField("category_label_fss", feed.getLabel());
    doc.addField("country_fss", feed.getCountry());
    doc.addField("city_fss", feed.getCity());

    //doc.addField("image_path_ss", feed.getImagePath());
    //doc.addField("is_latest_ss", feed.isLatestNews());

    doc.addField("posted_at_ls", feed.getPostedAt());

    // TODO: add classification of news feed, max 5 categories, read from json file all categories
    Set<String> cats = feed.getCategories();
    if (cats != null && cats.size() > 0) {
      doc.addField("labels_fmss", cats);
    }

    Set<String> tags = feed.getTags();
    if (tags != null && tags.size() > 0) {
      doc.addField("tags_fmss", tags);
    }

    Set<String> keywords = feed.getKeywords();
    if (keywords != null && keywords.size() > 0) {
      doc.addField("keywords_fmss", keywords);
    }
    return doc;
  }
  
  public static SolrInputDocument tweetRecordAsSolrDoc(TweetRecord tweet) {
    SolrInputDocument doc = new SolrInputDocument();

    doc.addField("id", tweet.getId());
    doc.addField("summary_fss", tweet.getText());
    doc.addField("cleaned_fss", tweet.getCleanedText());
    doc.addField("category_label_fss", tweet.getCategory());
    doc.addField("posted_at_ls", tweet.getTimestamp());
    doc.addField("country_fss", tweet.getCountry());

    String[] keywords = tweet.getKeywords();
    if (keywords != null && keywords.length > 0) {
      doc.addField("keywords_fmss", keywords);
    }
    return doc;
  }
}
