package com.career9.c9news;



public interface IndexingConnector {

  public void index(Doc doc) throws IndexingException;

  public void commit() throws IndexingException;

}
