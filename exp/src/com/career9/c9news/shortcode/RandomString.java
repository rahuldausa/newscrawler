package com.career9.c9news.shortcode;

import java.nio.ByteBuffer;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;

//http://stackoverflow.com/questions/4666647/how-to-create-user-friendly-unique-ids-uuids-or-other-unique-identifiers-in-jav
//hash = md5.md5('docs.python.org/library/uuid.html').digest()
//hash64 = base64.urlsafe_b64encode(hash)
//https://mail.python.org/pipermail/python-list/2012-November/635064.html
public class RandomString {

  public static String randomstring(int lo, int hi) {
    int n = rand(lo, hi);
    byte b[] = new byte[n];
    for (int i = 0; i < n; i++)
      b[i] = (byte) rand('a', 'z');
    return new String(b, 0);
  }

  private static int rand(int lo, int hi) {
    java.util.Random rn = new java.util.Random();
    int n = hi - lo + 1;
    int i = rn.nextInt(n);
    if (i < 0) i = -i;
    return lo + i;
  }

  public static String randomstring() {
    return randomstring(5, 25);
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    System.out.println(randomstring());
    System.out.println(createId(10));
    System.out.println(longToReverseBase62(100000000));
  }

  private final static char[] idchars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
 
  private static String createId(int len) {
      char[] id = new char[len];
      Random r = new Random(System.currentTimeMillis());
      for (int i = 0;  i < len;  i++) {
          id[i] = idchars[r.nextInt(idchars.length)];

      }
      return new String(id);
  }
  
  public static String generateShortUuid() {
    UUID uuid = UUID.randomUUID();

    long lsb = uuid.getLeastSignificantBits();
    long msb = uuid.getMostSignificantBits();

    // byte[] uuidBytes = ByteBuffer.allocate(16).putLong(msb).putLong(lsb).array();

    byte[] uuidBytes = "4de50e0d0d99a3bb274002059a316256".getBytes();

    // Strip down the '==' at the end and make it url friendly
    return Base64.encodeBase64URLSafeString(uuidBytes).substring(0, 22);
    // .replace("/", "_")
    // .replace("+", "-");
  }
  
  // For security reasons, it would be better if you make the values non-sequential, so each time a user registers, you
  // can increment the value let's say by 1024 (This would be good to generate uuids for 2^64 / 2^10 = 2^54 users which
  // is quite certainly more than you'd ever need :)
  public static String longToReverseBase62(long value /* must be positive! */) {

    final char[] LETTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        .toCharArray();

    StringBuilder result = new StringBuilder(9);
    do {
      result.append(LETTERS[(int) (value % 62)]);
      value /= 62l;
    } while (value != 0);

    return result.toString();
  }
}