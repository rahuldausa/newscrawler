package com.career9.c9news;

import com.rujhaan.news.domain.NewsFeed;

public interface Publisher {

  public void publish(NewsFeed feed);
}
