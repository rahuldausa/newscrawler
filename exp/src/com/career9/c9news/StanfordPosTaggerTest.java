package com.career9.c9news;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class StanfordPosTaggerTest {

  public static void main(String[] args) throws IOException {

    String tagged;

    // Initialize the tagger
    MaxentTagger tagger = new MaxentTagger("D:/ProjectsHub/stanford-postagger-full-2014-06-16/models/english-left3words-distsim.tagger");

    // The sample string
    String sample = "i can man the controls of this machine";

    // The tagged string
    tagged = tagger.tagString(sample);

    // output the tagged sample string onto your console
    System.out.println(tagged);
    
    HasWord sentence = new HasWord() {
      
      String sample = "i can man the controls of this machine";

      @Override
      public String word() {
        return sample;
      }
      
      @Override
      public void setWord(String arg0) {
       this.sample = arg0;
        
      }
    };
    
    List sentences = new ArrayList<>();
    sentences.add(sentence);
    
    //List sent = Sentence.toWordList("The", "slimy", "slug", "crawled", "over", "the", "long", ",", "green", "grass", ".");
    
    List sent = Sentence.toWordList(sample.split(" "));

    List<TaggedWord> taggedWords = tagger.tagSentence(sent);
    for (TaggedWord taggedWord : taggedWords) {
      System.out.println("=>"+taggedWord.word() + "--"+taggedWord.value() + "--"+taggedWord.tag());
    }
  }
}
