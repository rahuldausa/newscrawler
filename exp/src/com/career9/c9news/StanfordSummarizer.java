package com.career9.c9news;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.BreakIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class StanfordSummarizer {

  public static void main(String[] args) throws IOException {

    MaxentTagger tagger = new MaxentTagger("taggers/english-bidirectional-distsim.tagger");

    BufferedReader reader = new BufferedReader(new FileReader("D:/work/c9news/sample_text.txt"));
    File tempFile = new File("D:/work/c9news/tempFile.txt");
    Writer writerForTempFile = new BufferedWriter(new FileWriter(tempFile));

    String ls = System.getProperty("line.separator");
    Pattern tagFinder = Pattern.compile("_NNS");
    String line = null;
    while ((line = reader.readLine()) != null) {
      BreakIterator bi = BreakIterator.getSentenceInstance();
      bi.setText(line);
      int end, start = bi.first();
      while ((end = bi.next()) != BreakIterator.DONE) {
        String sentence = line.substring(start, end);
        //System.out.println(sentence);
        String tagged = tagger.tagString(sentence);
        //System.out.println("tagged:"+tagged);
        int score = 0;
        Matcher tag = tagFinder.matcher(tagged);
        while (tag.find()) {
          //System.out.println("\n"+"--->>" + tagged + "==>" + score);
          score++;
        }
        
        if (score > 1) {
          System.out.println("\nsentence:"+sentence + "\n");
        }
        start = end;
      }
    }
    writerForTempFile.flush();
    writerForTempFile.close();
  }
}
