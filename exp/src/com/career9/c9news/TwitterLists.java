package com.career9.c9news;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.news.impl.dao.DBMapper;
import com.rujhaan.news.impl.dao.MongoDao;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterLists {

  public static void main(String[] args) throws IllegalStateException, TwitterException,
      KeyManagementException, NoSuchAlgorithmException {
    Logger.getRootLogger().setLevel(Level.INFO);
    TrustManager trm = new X509TrustManager() {
      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      public void checkClientTrusted(X509Certificate[] certs, String authType) {

      }

      public void checkServerTrusted(X509Certificate[] certs, String authType) {
      }
    };

    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, new TrustManager[]{trm}, null);
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    //System.setProperty("http.proxyHost", "localhost");
    //System.setProperty("http.proxyPort", "5865");
    
    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb.setDebugEnabled(true).setOAuthConsumerKey("LWNBPJMWmCYEbM249z1JBLn7n")
        .setOAuthConsumerSecret("EmRUIOjyDbDrTa2cve1QYrMsaX1jsMRFDTkhNqCM9ip5GghsaU")
        .setOAuthAccessToken("1560501828-bkh9m55jVuJ7hqqVQWXosQNvSaVLPZCRHfpGcYB")
        .setOAuthAccessTokenSecret("4NlfQNfHOv18kSOIKSlzpofOTHcAWR0FTwjK6RpZoaeVI");
    
    //TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
    
    final Twitter twitter = new TwitterFactory(cb.build()).getInstance();
    //AccessToken accessToken = loadAccessToken(1);
    //twitter.setOAuthConsumer("uoruQoBzDFJtbnXTVpWzw", "Y1THWWulOflQKI6hD5GIok1IBrOuldGPmFdp6yrQbE");
    //twitter.setOAuthAccessToken(accessToken);
    
    long userId = twitter.getId();
    User user = twitter.showUser(userId);
    System.out.println("screen name:"+user.getScreenName());
    
/*    UserList userList = twitter.showUserList(userId);
    long listId = userList.getId();
    String slugName = userList.getSlug();*/
    
   // System.out.println(listId + " ==> " + slugName);

    //To get all the lists working code
/*    ResponseList<UserList> usersResponse = twitter.getUserLists(userId);
    for (UserList userList : usersResponse) {
      System.out.println(userList.getName() + "=="+userList.getId());
    }*/
    
/*    ArrayList<User> followers = new ArrayList<User>();
    long nextCursor = -1;
    do {
      ResponseList<UserList> usersResponse = twitter.getUserLists(userId);
      for (UserList userList : usersResponse) {
        System.out.println(userList.getName() + "=="+userList.getId());
      }
    } while (nextCursor > 0);
    System.out.println(followers.size() > 0 ? "Getting followers:" : "No followers.");
    for (User u : followers) {
      System.out.println(u.getScreenName() + ", id: " + u.getId());
    }*/
    
    //curated_news==162485284
    //news==162481830
      //  mylist==110988681
    
    ScheduledExecutorService excutor = Executors.newScheduledThreadPool(1);

    Runnable r = new Runnable() {

      @Override
      public void run() {

        int maxPages = 100;
        int maxPerPage = 100;
        final DBMapper dbMapper = new DBMapper();
        
        //dont enable as news should go to local mongodb and then Summarization will insert to this server
        //dont do => MongoDao mongoDao = new MongoDao("23.227.177.112", "newstweet");
        MongoDao mongoDao = new MongoDao("127.0.0.1", "newstweet");
        //MongoDao mongoDao = new MongoDao("127.0.0.1", "cctweet");

        //final MongoDao mongoDao = new MongoDao("newstweet");
        BasicDBObject query = new BasicDBObject("tweet_ID", -1).append("unique", true).append("dropDups", true);
        mongoDao.addIndex(query);
        
        for (int i = 1; i < maxPages; i++) {
          Paging paging = new Paging(i, maxPerPage);
          // ReadibilityTest readibilityTest = new ReadibilityTest();
          ResponseList<Status> usersResponse = null;
          try {
            usersResponse = twitter.getUserListStatuses(162485284, paging);
          } catch (TwitterException e) {
            e.printStackTrace();
          }
          for (Status status : usersResponse) {

            // If a retweet then continue;
            if (status.isRetweet()) {
              System.out.println("seems to be retweet:" + status.getText());
              continue;
            }
            TweetRecord tweet = new TweetRecord();
            tweet.id = status.getId();
            tweet.userName = status.getUser().getName();
            tweet.screenName = status.getUser().getScreenName();
            tweet.userFollowersCount = status.getUser().getFollowersCount();
            tweet.text = status.getText();
            if (status.getRetweetedStatus() != null) {
              tweet.retweetCount = status.getRetweetedStatus().getRetweetCount();
            }
            if (status.getMediaEntities().length > 0) {
              tweet.mediaURL = status.getMediaEntities()[0].getMediaURL();
            }
            if (status.getPlace() != null) {
              tweet.countryCode = status.getPlace().getCountryCode();
            }

            if (status.getUser() != null) {
              tweet.userProfileImageURL = status.getUser().getProfileImageURL();
              tweet.userMiniProfileImageURL = status.getUser().getMiniProfileImageURL();
              tweet.userURL = status.getUser().getURL();
            }
            if (status.getURLEntities() != null) {
              List<String> links = new ArrayList<>();
              for (URLEntity urlEntity : status.getURLEntities()) {
                links.add(urlEntity.getExpandedURL());
              }
              tweet.urlLinks = links;
            }

            DBObject dbObj = dbMapper.fromTweet(tweet);
            
            DBObject q = new BasicDBObject("tweet_ID", tweet.id);
            mongoDao.upsert(q, dbObj);

            if (status.getURLEntities() != null) {
              System.out.println(status.getUser().getName() + "--" + status.getText());
              for (URLEntity url : status.getURLEntities()) {
                System.out.println(url.getDisplayURL() + "--" + url.getExpandedURL() + "--"
                    + url.getURL());
                /* try { readibilityTest.fetchContent(url.getExpandedURL()); } catch (Exception e) {
                 * e.printStackTrace(); } */
              }
            } else {
              System.out.println(status.getUser().getName() + "--" + status.getText());
            }
          }
        }
        // User friend = twitter.createFriendShip("username");
        try {
          Thread.sleep(5000L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    };
    excutor.scheduleAtFixedRate(r, 0, 15, TimeUnit.SECONDS);

  }

/*  private static AccessToken loadAccessToken(int useId) {
    String token = "136921037-YOnMqXQIphSwqeB1NWMlDHlC6g2kcC3vfCUiMDwt";
    String tokenSecret = "DXXT14P1YfQAmHdMnJbY8nFFD1xPx05ndYpj3BADQ2c";
    return new AccessToken(token, tokenSecret);
  }*/
}
