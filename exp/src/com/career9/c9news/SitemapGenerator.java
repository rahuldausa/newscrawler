package com.career9.c9news;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.redfin.sitemapgenerator.ChangeFreq;
import com.redfin.sitemapgenerator.SitemapIndexGenerator;
import com.redfin.sitemapgenerator.WebSitemapGenerator;
import com.redfin.sitemapgenerator.WebSitemapUrl;
import com.rujhaan.news.domain.Response;

public class SitemapGenerator {

  public static void main(String[] args) throws MalformedURLException {
    
    if (args == null || args.length==0) {
      args = new String[5];
      args[0] = "d:/work/c9news/sitemap/";
      args[1] = "d:/work/c9news/zippednews/all_topics.txt";
      args[2] = "topics";
      args[3] = "true";
      args[4] = "45000";
    }

    ConfigOptions options = new ConfigOptions();
    options.baseDir = args[0];
    options.dataFile = args[1];
    options.type = args[2];
    options.shouldWriteSitemapIndex = Boolean.parseBoolean(args[3]);
    options.limit = Integer.parseInt(args[4]);

    SitemapGenerator generator = new SitemapGenerator();
    generator.executeRequest(options);

  }

  public static class ConfigOptions {
    String baseDir;
    String dataFile;
    String type;
    String country;
    boolean shouldWriteSitemapIndex;
    int limit;
  }

  public void executeRequest(ConfigOptions options) {
    String baseDir = options.baseDir;
    String dataFile = options.dataFile;
    String type = options.type;// keywords or domains or both
    String country = options.country;// country
    boolean shouldWriteSitemapIndex = options.shouldWriteSitemapIndex;// country
    int limit = options.limit;

    String baseUrl = BASE_URL;
    File baseDirF = new File(baseDir);

    try {
      List<String> list = commentAwareAndTabSeperatedAsList(new File(dataFile), limit);
      System.out.println("type:" + type);
      if (type.equals("topics")) {
        writeTopicSitemap(baseDirF, baseUrl, country, list);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    if (shouldWriteSitemapIndex) {
      try {
        File[] files = baseDirF.listFiles();
        // generate sitemap index for foo + bar
        SitemapIndexGenerator sig = new SitemapIndexGenerator(baseUrl, new File(baseDir,
            "sitemap_index.xml"));
        for (File file : files) {
          if (file.getAbsolutePath().contains("topics")) {
            // website.com/sitemap/topics1.xml
            sig.addUrl(baseUrl + "/" + baseDirF.getName() + "/" + file.getName());// only file Name
          }
        }
        sig.write();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    System.out.println("Completed!!");

  }

  public static List<String> commentAwareAndTabSeperatedAsList(File f, int limit)
      throws IOException {
    System.out.println("Reading from file: " + f.getAbsolutePath());
    BufferedReader br = new BufferedReader(new FileReader(f));
    String line = null;
    List<String> list = new ArrayList<String>();
    int counter = 0;
    while ((line = br.readLine()) != null) {
      if (line != null) {
        counter++;
        if (counter > limit) {
          break;
        }
        line = line.trim();
        line = line.split("\t")[0];
        if (line.startsWith("#")) {
          continue;// ignore as commented
        }
      }
      list.add(line);
    }
    return list;
  }

  public static String BASE_URL = "http://www.rujhaan.com";

  // http://www.rujhaan.com/topic/Narendra-Modi.html
  public static String TOPICS_URL = BASE_URL + "/topic/%s.html";

  public void writeTopicSitemap(File baseDir, String baseUrl, String country, List<String> values)
      throws MalformedURLException {
    WebSitemapGenerator wsg;

    // generate keywords sitemap
    wsg = WebSitemapGenerator.builder(BASE_URL, baseDir).fileNamePrefix("topics").maxUrls(45000)
        .build();

    String keywordUrl = null;
    for (String val : values) {
      val = Response.applySelectiveEncoding(val);
      keywordUrl = String.format(TOPICS_URL, val);
      try {
        wsg.addUrl(buildWebSitemapUrl(keywordUrl));
      } catch (MalformedURLException e) {
        e.printStackTrace();
      }
    }
    System.out.println("wrote to the sitemap:");
    wsg.write();
    System.out.println("Completed!!");
  }

  public WebSitemapUrl buildWebSitemapUrl(String urlString) throws MalformedURLException {
    WebSitemapUrl url = new WebSitemapUrl.Options(urlString).lastMod(new Date()).priority(1.0)
        .changeFreq(ChangeFreq.WEEKLY).build();
    return url;
  }

  public static void test() throws MalformedURLException {
    File myDir = new File("d:/work/f75/");

    WebSitemapGenerator wsg;

    // generate foo sitemap
    wsg = WebSitemapGenerator.builder("http://www.example.com", myDir).fileNamePrefix("domains")
        .build();
    for (int i = 0; i < 5; i++)
      wsg.addUrl("http://www.example.com/foo" + i + ".html");
    wsg.write();

    // generate bar sitemap
    wsg = WebSitemapGenerator.builder("http://www.example.com", myDir).fileNamePrefix("keywords")
        .build();
    for (int i = 0; i < 5; i++) {
      WebSitemapUrl url = new WebSitemapUrl.Options("http://www.example.com/index.html")
          .lastMod(new Date()).priority(1.0).changeFreq(ChangeFreq.MONTHLY).build();
      wsg.addUrl(url);
    }
    wsg.write();
    // generate sitemap index for foo + bar
    SitemapIndexGenerator sig = new SitemapIndexGenerator("http://www.example.com", new File(myDir,
        "index_sitemap.xml"));
    sig.addUrl("http://www.example.com/foo.xml");
    sig.addUrl("http://www.example.com/bar.xml");
    sig.write();
  }
}
