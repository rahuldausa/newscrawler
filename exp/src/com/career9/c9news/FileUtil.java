package com.career9.c9news;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class FileUtil {

  private static final Logger log = Logger.getLogger(FileUtil.class);

  public static String contentAsString(File f) throws IOException {
    BufferedReader br = new BufferedReader(new FileReader(f));
    StringBuilder buf = new StringBuilder();
    String line = null;
    try {
      while ((line = br.readLine()) != null) {
        if (line != null) {
          line = line.trim();
        }
        buf.append(line).append("\n");
      }
    } finally {
      if (br != null) {
        br.close();
      }
    }
    return buf.toString();
  }

  public static List<String> asList(File f) throws IOException {
    BufferedReader br = new BufferedReader(new FileReader(f));
    String line = null;
    List<String> list = new ArrayList<String>();
    try {
      while ((line = br.readLine()) != null) {
        if (line != null) {
          line = line.trim();
        }
        list.add(line);
      }
    } finally {
      if (br != null) {
        br.close();
      }
    }
    return list;
  }

  public static List<String> commentAwareAsList(File f) throws IOException {
    log.info("Reading from file: " + f.getAbsolutePath());
    BufferedReader br = new BufferedReader(new FileReader(f));
    String line = null;
    List<String> list = new ArrayList<String>();
    try {
      while ((line = br.readLine()) != null) {
        if (line != null) {
          line = line.trim();
          if (line.startsWith("#")) {
            continue;// ignore as commented
          }
        }
        list.add(line);
      }
    } finally {
      if (br != null) {
        br.close();
      }
    }
    return list;
  }

  public static void commentAwareAsList(File f, StreamingCallback callback) throws IOException {
    log.info("Reading from file: " + f.getAbsolutePath());
    BufferedReader br = new BufferedReader(new FileReader(f));
    String line = null;
    try {
      while ((line = br.readLine()) != null) {
        if (line != null) {
          line = line.trim();
          if (line.startsWith("#")) {
            continue;// ignore as commented
          }
        }
        callback.read(line);
      }
    } finally {
      if (br != null) {
        br.close();
      }
    }
  }

  public static interface StreamingCallback {
    void read(String text);
  }

  public static List<String> loadResource(Class<?> clz, String resourceName, boolean commentAware) {
    InputStream stream = clz.getClassLoader().getResourceAsStream(resourceName);
    List<String> list = new ArrayList<String>();
    if (stream == null) {
      log.error("Couldn't find " + resourceName);
      throw new IllegalArgumentException("Couldn't find " + resourceName);
    }
    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
    String line = null;
    try {
      while ((line = reader.readLine()) != null) {
        if (commentAware && line.startsWith("#")) {
          continue;// ignore this line as it is commented
        }
        list.add(line);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
        }
      }
    }
    return list;
  }

  public static String loadResourceAsString(Class<?> clz, String resourceName, boolean commentAware) {
    InputStream stream = clz.getClassLoader().getResourceAsStream(resourceName);
    StringBuilder buf = new StringBuilder();
    if (stream == null) {
      log.error("Couldn't find " + resourceName);
      throw new IllegalArgumentException("Couldn't find " + resourceName);
    }
    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
    String line = null;
    try {
      while ((line = reader.readLine()) != null) {
        if (commentAware && line.startsWith("#")) {
          continue;// ignore this line as it is commented
        }
        buf.append(line).append(" ");
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
        }
      }
    }
    return buf.toString();
  }

  public void readDirectoryRecrusively(String dir, FileCallback callback) {
    File dirF = new File(dir);
    readDirectoryRecrusively(dirF, callback);
  }

  public void readDirectoryRecrusively(File dir, FileCallback callback) {
    File[] files1 = dir.listFiles();
    if (files1 != null && files1.length > 0) {
      for (File file : files1) {
        if (file.isDirectory()) {
          readDirectoryRecrusively(dir, callback);
        } else {
          callback.handle(file);
        }
      }
    }
  }

  public interface FileCallback {
    public void handle(File f);
  }
}
