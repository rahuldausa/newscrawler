package com.career9.c9news;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.news.impl.dao.DBMapper;
import com.rujhaan.news.impl.dao.MongoDao;

import twitter4j.IDs;
import twitter4j.PagableResponseList;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterFollowers {

  public static void main(String[] args) throws IllegalStateException, TwitterException,
      KeyManagementException, NoSuchAlgorithmException {
    Logger.getRootLogger().setLevel(Level.INFO);
    TrustManager trm = new X509TrustManager() {
      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      public void checkClientTrusted(X509Certificate[] certs, String authType) {

      }

      public void checkServerTrusted(X509Certificate[] certs, String authType) {
      }
    };

    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, new TrustManager[]{trm}, null);
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    //System.setProperty("http.proxyHost", "localhost");
    //System.setProperty("http.proxyPort", "5865");
    
    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb.setDebugEnabled(true).setOAuthConsumerKey("LWNBPJMWmCYEbM249z1JBLn7n")
        .setOAuthConsumerSecret("EmRUIOjyDbDrTa2cve1QYrMsaX1jsMRFDTkhNqCM9ip5GghsaU")
        .setOAuthAccessToken("1560501828-bkh9m55jVuJ7hqqVQWXosQNvSaVLPZCRHfpGcYB")
        .setOAuthAccessTokenSecret("4NlfQNfHOv18kSOIKSlzpofOTHcAWR0FTwjK6RpZoaeVI");
    
    //TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
    
    final Twitter twitter = new TwitterFactory(cb.build()).getInstance();
    //AccessToken accessToken = loadAccessToken(1);
    //twitter.setOAuthConsumer("uoruQoBzDFJtbnXTVpWzw", "Y1THWWulOflQKI6hD5GIok1IBrOuldGPmFdp6yrQbE");
    //twitter.setOAuthAccessToken(accessToken);
    
    long userId = twitter.getId();
    //User user = twitter.showUser(userId);
//    System.out.println("screen name:"+user.getScreenName());
//    
   User user = twitter.showUser("HuffingtonPost");
   userId = user.getId();

    
/*    UserList userList = twitter.showUserList(userId);
    long listId = userList.getId();
    String slugName = userList.getSlug();*/
    
   // System.out.println(listId + " ==> " + slugName);

    //To get all the lists working code
/*    ResponseList<UserList> usersResponse = twitter.getUserLists(userId);
    for (UserList userList : usersResponse) {
      System.out.println(userList.getName() + "=="+userList.getId());
    }*/
    
/*    ArrayList<User> followers = new ArrayList<User>();
    long nextCursor = -1;
    do {
      ResponseList<UserList> usersResponse = twitter.getUserLists(userId);
      for (UserList userList : usersResponse) {
        System.out.println(userList.getName() + "=="+userList.getId());
      }
    } while (nextCursor > 0);
    System.out.println(followers.size() > 0 ? "Getting followers:" : "No followers.");
    for (User u : followers) {
      System.out.println(u.getScreenName() + ", id: " + u.getId());
    }*/
    
    //curated_news==162485284
    //news==162481830
      //  mylist==110988681
    
  //Getting followers
//    long followerCursor = -1;
//    IDs followerIds;
//    do {
//      followerIds = twitter.getFollowersIDs(screenUser.getId(), followerCursor);
//
//      long temp[] = followerIds.getIDs();
//      for (long followerID : temp) {
//        User u = twitter.showUser(followerID);
//        // eventually I will extract information from followerUserProfile
//        // for example here -
//        System.out.println(u.getName() + "--" + u.getFollowersCount() + "--" + u.getLocation() + "--");
//      }
//      
////      ResponseList<User> followers = twitter.lookupUsers(followerIds.getIDs());
////
////      for (User follower : followers) {
////        System.out.println(follower.getName() +"--"+follower.getScreenName());
////        // db.insertFollowers
////        // (
////        // follower.getId(),
////        // userId,
////        // "@" + follower.getScreenName(),
////        // follower.getName(),
////        // follower.getLocation(),
////        // follower.getFollowersCount(),
////        // follower.getFriendsCount()
////        // );
////      }
//      try {
//        Thread.sleep(10000L);
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//
//    } while ((followerCursor = followerIds.getNextCursor()) != 0);

    
    int favouritesCount = user.getFavouritesCount();
    int followersCount = user.getFollowersCount();
    int friendsCount = user.getFriendsCount();
    userId = user.getId();
    System.out.println("FavouritesCount: " + favouritesCount + ", followersCount: " + followersCount + ", friendsCount: " + friendsCount);
    ArrayList<User> followers = new ArrayList<User>();
    long nextCursor = -1;
    do {
        PagableResponseList<User> usersResponse = twitter.getFollowersList(user.getScreenName(), nextCursor);
        System.out.println("size() of first iteration:" + usersResponse.size());
        nextCursor = usersResponse.getNextCursor();
        
        for ( User user1 : usersResponse) {
          System.out.println(user1.getScreenName() + ", id: " + user1.getId());
        }
        followers.addAll(usersResponse);
    } while ( nextCursor > 0);

    System.out.println( followers.size() > 0 ? "Getting followers:" : "No followers.");

    for ( User user1 : followers) {
        System.out.println(user1.getScreenName() + ", id: " + user1.getId());
    }

  }

/*  private static AccessToken loadAccessToken(int useId) {
    String token = "136921037-YOnMqXQIphSwqeB1NWMlDHlC6g2kcC3vfCUiMDwt";
    String tokenSecret = "DXXT14P1YfQAmHdMnJbY8nFFD1xPx05ndYpj3BADQ2c";
    return new AccessToken(token, tokenSecret);
  }*/
}
