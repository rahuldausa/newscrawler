package com.career9.c9news;


public class IndexingException extends Exception {

  public IndexingException(Throwable e) {
    super(e);
  }

}
