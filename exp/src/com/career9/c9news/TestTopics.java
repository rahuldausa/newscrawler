package com.career9.c9news;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TestTopics {

  public static void main(String[] args) throws IOException {
    File f = new File("D:/work/c9news/zippednews/raw/");
    for (File f1 : f.listFiles()) {
      String content = FileUtil.contentAsString(f1);
      Document jsoupDoc = Jsoup.parse(content);

      Elements topicsElem = jsoupDoc.select("#bodyarea > div > div > a > p");
      System.out.println("=>"+multiSelect(topicsElem)+"\n");
      
      topicsElem = jsoupDoc.select("#bodyarea > div > div.col-sm-9.col-lg-9.col-md-9 > p.tags > a");
      System.out.println("==>"+multiSelect(topicsElem)+"\n");
    }
  }
  
  public static List<String> multiSelect( Elements elements ) {
    List<String> values = new ArrayList<String>();
    String val=null;
    for (Element e : elements) {
      values.add(e.ownText());
      for (Element e1 : e.children()) {
        val = e1.text();
        val = val.trim();
        if (val != null && !val.isEmpty()) {
          values.add(val);
        }
      }
    }
    return values;
  }
}
