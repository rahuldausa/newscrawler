package com.career9.c9news;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import twitter4j.Query;
import twitter4j.Query.ResultType;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;
import twitter4j.UserMentionEntity;
import twitter4j.conf.ConfigurationBuilder;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

/**
 * 
 * @author Muhammad.Saifuddin
 */
public class TwitterTask {

  private static ConfigurationBuilder cb;

  public TwitterTask() {

  }

  /**
   * static block used to construct a connection with tweeter with twitter4j configuration with provided settings. This
   * configuration builder will be used for next search action to fetch the tweets from twitter.com.
   */
  static {
    // System.setProperty("http.proxyHost", "localhost");
    // System.setProperty("http.proxyPort", String.valueOf(5865));
    cb = new ConfigurationBuilder();
    /* cb.setDebugEnabled(true); cb.setOAuthConsumerKey("*********************");
     * cb.setOAuthConsumerSecret("*************************"); cb.setOAuthAccessToken("*************************");
     * cb.setOAuthAccessTokenSecret("*************************"); */
    cb.setDebugEnabled(true);
    cb.setOAuthConsumerKey("LWNBPJMWmCYEbM249z1JBLn7n");
    cb.setOAuthConsumerSecret("EmRUIOjyDbDrTa2cve1QYrMsaX1jsMRFDTkhNqCM9ip5GghsaU");
    cb.setOAuthAccessToken("1560501828-bkh9m55jVuJ7hqqVQWXosQNvSaVLPZCRHfpGcYB");
    cb.setOAuthAccessTokenSecret("4NlfQNfHOv18kSOIKSlzpofOTHcAWR0FTwjK6RpZoaeVI");
    TwitterFactory tf = new TwitterFactory(cb.build());
    twitter = tf.getInstance();
  }

  private static Twitter twitter;

  public static void main(String[] args) {

    TwitterTask taskObj = new TwitterTask();
    taskObj.getTweetByQuery(true);
  }

/*  String[] queries = {"world", "syria", "gaza", "health", "movies", "hollywood", "bollywood",
      "politics", "business", "entertainment", "lifestyle", "fashion", "fashion week",
      "fashion blogger", "apple", "facebook", "google", "mumbai", "new york", "bangalore",
      "hyderabad", "ebola", "bacteria", "narendra modi", "bjp", "congress", "rahul gandhi",
      "trends", "obama","LoveLife", "#ALS", "#Fashion",
      "#Style",
      "#FashionBlogger",
      "#Fall",
      "#OOTD",
      "#Accessories",
      "#Design",
      "#Wedding",
      "#FashionTrends",
      "#Marketing",
      "#Dress",
      "#Model",
      "#Lookbook",
      "#SocialMediaMarketing",
      "#kikiriki",
      "#Desing",
      "#Summer",
      "#FallFashion",
      "#Fall2014",
      "#FashionBlog",
      "#Analytics",
      "#Mobile",
      "#Measure",
      "#Infographic",
      "#WebAnalytics",
      "#GoogleAnalyticstips",
      "#UniversalAnalytics",
      "#Startup",
      "#Google",
      "#News",
      "#TechNews",
      "#Android",
      "#Messenger",
      "#WhatsApp",
      "#App",
      "#Hike",
      "#Apps",
      "#SocialMedia",
      "#Facebook",
      "#Kik",
      "#Mobile",
      "#MessagingApps",
      "#iOS",
      "#SocialMediaMarketing",
      "#iPhone",
      "#Sms",
      "#Marketing",
      "#Apple",
      "#Smartphone",
      "#TechNews",
      "#EntertainmentNews",
      "#WorldNews",
      "#USNews",
      "#SportsNews",
      "#BusinessNews",
      "#IceBucketChallenge",
      "#ALS",
      "#9YearOldUzi",
      "#MarioKart8DLC",
      "#MattDamonIceBucket",
      "#StrikeOutALS",
      "#DragonAge",
      "#DarkSouls2DLC",
      "#MetroRedux",
      "#DragonAgeInquisition",
      "#Xbox",
      "#XboxOne",
      "#SupermanIceBucketChallenge",
      "#MarioKart8",
      "#News",
      "#ALS",
      "#IceBucketChallenge",
      "#MattDamonIceBucket",
      "#StrikeOutALS",
      "#Flipagram",
      "#RobinWilliams",
      "#Video",
      "#RIP",
      "#RobinWilliamsEmmyTribute",
      "#Emmys",
      "#BillyCrystal",
      "#IggyAzalea",
      "#Movie",
      "#JLoIggyAzalea",
      "#Trailer",
      "#106AndPark",
      "#Tribute",
      "#HipHop",
      "#ExistsTrailer"};*/
  
  String[] queries = {"world", "syria", "gaza", "health", "movies", "hollywood", "bollywood",
      "politics", "business", "entertainment", "lifestyle", "fashion", "fashion week",
      "fashion blogger", "apple", "facebook", "google", "mumbai", "new york", "bangalore",
      "hyderabad", "ebola", "bacteria", "narendra modi", "bjp", "congress", "rahul gandhi",
      "trends", "obama","LoveLife", "#ALS", "#Fashion",
      "Style",
      "FashionBlogger",
      "Fall",
      "OOTD",
      "Accessories",
      "Design",
      "Wedding",
      "FashionTrends",
      "Marketing",
      "Dress",
      "Model",
      "Lookbook",
      "SocialMediaMarketing",
      "kikiriki",
      "Desing",
      "Summer",
      "FallFashion",
      "Fall2014",
      "FashionBlog",
      "Analytics",
      "Mobile",
      "Measure",
      "Infographic",
      "WebAnalytics",
      "GoogleAnalyticstips",
      "UniversalAnalytics",
      "Startup",
      "Google",
      "News",
      "TechNews",
      "Android",
      "Messenger",
      "WhatsApp",
      "App",
      "Hike",
      "Apps",
      "SocialMedia",
      "Facebook",
      "Kik",
      "Mobile",
      "MessagingApps",
      "iOS",
      "SocialMediaMarketing",
      "iPhone",
      "Sms",
      "Marketing",
      "Apple",
      "Smartphone",
      "TechNews",
      "EntertainmentNews",
      "WorldNews",
      "USNews",
      "SportsNews",
      "BusinessNews",
      "IceBucketChallenge",
      "ALS",
      "9YearOldUzi",
      "MarioKart8DLC",
      "MattDamonIceBucket",
      "StrikeOutALS",
      "DragonAge",
      "DarkSouls2DLC",
      "MetroRedux",
      "DragonAgeInquisition",
      "Xbox",
      "XboxOne",
      "SupermanIceBucketChallenge",
      "MarioKart8",
      "News",
      "ALS",
      "IceBucketChallenge",
      "MattDamonIceBucket",
      "StrikeOutALS",
      "Flipagram",
      "RobinWilliams",
      "Video",
      "RIP",
      "RobinWilliamsEmmyTribute",
      "Emmys",
      "BillyCrystal",
      "IggyAzalea",
      "Movie",
      "JLoIggyAzalea",
      "Trailer",
      "106AndPark",
      "Tribute",
      "HipHop",
      "ExistsTrailer"};
  
  //String[] queries1 = {"slideshare AND solr"," slideshare AND hadoop"};
  //String[] queries1 = {"youtube AND solr"," youtube AND hadoop"};
  //String[] queries1 = {"vizag", "odisha", "Cyclone Hudhud", "HudHud", "#HudHud","#HudHudHitsIndia"};


  /**
   * void getTweetByQuery method used to fetch records from twitter.com using Query class to define query for search
   * param with record count. QueryResult persist result from twitter and provide into the list to iterate records 1 by
   * one and later on item.insert is call to store this BasicDBObject into MongoDB items Collection.
   * 
   * @param url an absolute URL giving the base location of the image
   * @see BasicDBObject, DBCursor, TwitterFactory, Twitter
   */
  public void getTweetByQuery(boolean loadRecords) {
    if (cb != null) {
      try {
  TweetStatusHandler tweetStatusHandler = new TweetStatusHandler();

        for (String qString : queries) {
          //Query query = new Query(qString+"+filter:images");
          Query query = new Query(qString);
          query.setCount(2500);
          //ResultType rt  = ResultType.popular;
          query.resultType(Query.POPULAR);
          //query.resultType(Query.);

          query.setLang("en");
          QueryResult result;
          result = twitter.search(query);
          System.out.println("Getting Tweets...");
          List<Status> tweets = result.getTweets();

          System.out.println("no of tweets:"+tweets.size());
          for (Status tweet : tweets) {
            tweetStatusHandler.handle(tweet, false, true);
          }
          System.out.println("Fetched for " + qString + " and waited for 3sec");
          Thread.sleep(1000L);
        }
      } catch (TwitterException te) {
        System.out.println("te.getErrorCode() " + te.getErrorCode());
        System.out.println("te.getExceptionCode() " + te.getExceptionCode());
        System.out.println("te.getStatusCode() " + te.getStatusCode());
        if (te.getStatusCode() == 401) {
          System.out
              .println("Twitter Error : \nAuthentication credentials (https://dev.twitter.com/pages/auth) were missing or incorrect.\nEnsure that you have set valid consumer key/secret, access token/secret, and the system clock is in sync.");
        } else {
          System.out.println("Twitter Error : " + te.getMessage());
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else {
      System.out.println("MongoDB is not Connected! Please check mongoDB intance running..");
    }
  }
  
  public void getTweetByQuery( TweetStatusHandler tweetStatusHandler, Set<String> queries) {
    if (cb != null) {
      try {
        for (String qString : queries) {
          //Query query = new Query(qString+"+filter:images");
          Query query = new Query(qString);
          query.setCount(2500);
          //ResultType rt  = ResultType.popular;
          query.resultType(Query.POPULAR);
          //query.resultType(Query.);

          query.setLang("en");
          QueryResult result;
          result = twitter.search(query);
          System.out.println("Getting Tweets...");
          List<Status> tweets = result.getTweets();

          System.out.println("no of tweets:"+tweets.size());
          for (Status tweet : tweets) {
            //System.out.println(qString+"=>"+tweet.getText());
            tweetStatusHandler.handle(tweet, true, true);
          }
          System.out.println("Fetched for " + qString + " and waited for 3sec");
          Thread.sleep(1000L);
        }
      } catch (TwitterException te) {
        System.out.println("te.getErrorCode() " + te.getErrorCode());
        System.out.println("te.getExceptionCode() " + te.getExceptionCode());
        System.out.println("te.getStatusCode() " + te.getStatusCode());
        if (te.getStatusCode() == 401) {
          System.out
              .println("Twitter Error : \nAuthentication credentials (https://dev.twitter.com/pages/auth) were missing or incorrect.\nEnsure that you have set valid consumer key/secret, access token/secret, and the system clock is in sync.");
        } else {
          System.out.println("Twitter Error : " + te.getMessage());
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else {
      System.out.println("MongoDB is not Connected! Please check mongoDB intance running..");
    }
  }
  
  public List<Status> getTweetTextsWithOR(Set<String> topics) {
    StringBuilder buf = new StringBuilder();
    for (String t : topics) {
      buf.append(t).append(" OR ");
    }
    String queryStr = buf.toString();
    Query query = new Query(queryStr+"-filter:images");
    return getTweetQuery(query);
  }
  
  public List<Status> getTweetImageWithOR(Set<String> topics) {
    StringBuilder buf = new StringBuilder();
    for (String t : topics) {
      buf.append(t).append(" OR ");
    }
    String queryStr = buf.toString();
    Query query = new Query(queryStr+"-filter:images");
    return getTweetQuery(query);
  }
  
  public List<Status> getTweetTexts(String topic) {
    Query query = new Query(topic+"-filter:images");
    return getTweetQuery(query);
  }
  
  public List<Status> getTweetImagesQuery(String topic) {
    Query query = new Query(topic + "+filter:images");
    return getTweetQuery(query);
  }
  
  public List<Status> getTweetQuery(Query query) {
    if (cb != null) {
      try {
        //Query query = new Query(topic);
        query.setCount(50);
        // ResultType rt = ResultType.popular;
        query.resultType(Query.POPULAR);
        query.setLang("en");
        QueryResult result;
        result = twitter.search(query);
        System.out.println("Getting Tweets...");
        List<Status> tweets = result.getTweets();
        return tweets;
      } catch (TwitterException te) {
        System.out.println("te.getErrorCode() " + te.getErrorCode());
        System.out.println("te.getExceptionCode() " + te.getExceptionCode());
        System.out.println("te.getStatusCode() " + te.getStatusCode());
        if (te.getStatusCode() == 401) {
          System.out
              .println("Twitter Error : \nAuthentication credentials (https://dev.twitter.com/pages/auth) were missing or incorrect.\nEnsure that you have set valid consumer key/secret, access token/secret, and the system clock is in sync.");
        } else {
          System.out.println("Twitter Error : " + te.getMessage());
        }
      }
    }
    return null;
  }

}
