package com.career9.c9news;
import java.io.Serializable;

public class TweetRecord1 implements Serializable {

  private static final long serialVersionUID = 1L;
  public long id;
  public String userName;
  public String userProfileImageURL;
  public String userMiniProfileImageURL;
  public int userFollowersCount;
  public String userURL;
  public String text;
  public long timestamp;
  public int retweetCount;
  public String mediaURL;
  public String countryCode;

  public long getId() {
    return id;
  }
  public void setId(long id) {
    this.id = id;
  }
  public String getUserName() {
    return userName;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public int getUserFollowersCount() {
    return userFollowersCount;
  }
  public void setUserFollowersCount(int userFollowersCount) {
    this.userFollowersCount = userFollowersCount;
  }
  public String getText() {
    return text;
  }
  public void setText(String text) {
    this.text = text;
  }
  public long getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }
  public int getRetweetCount() {
    return retweetCount;
  }
  public void setRetweetCount(int retweetCount) {
    this.retweetCount = retweetCount;
  }
  public String getMediaURL() {
    return mediaURL;
  }
  public void setMediaURL(String mediaURL) {
    this.mediaURL = mediaURL;
  }
  public String getCountryCode() {
    return countryCode;
  }
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
  
  public String getUserProfileImageURL() {
    return userProfileImageURL;
  }
  public void setUserProfileImageURL(String userProfileImageURL) {
    this.userProfileImageURL = userProfileImageURL;
  }
  public String getUserMiniProfileImageURL() {
    return userMiniProfileImageURL;
  }
  public void setUserMiniProfileImageURL(String userMiniProfileImageURL) {
    this.userMiniProfileImageURL = userMiniProfileImageURL;
  }
  public String getUserURL() {
    return userURL;
  }
  public void setUserURL(String userURL) {
    this.userURL = userURL;
  }
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TweetRecord [id=");
    builder.append(id);
    builder.append(", userName=");
    builder.append(userName);
    builder.append(", userFollowersCount=");
    builder.append(userFollowersCount);
    builder.append(", text=");
    builder.append(text);
    builder.append(", timestamp=");
    builder.append(timestamp);
    builder.append(", retweetCount=");
    builder.append(retweetCount);
    builder.append(", mediaURL=");
    builder.append(mediaURL);
    builder.append(", countryCode=");
    builder.append(countryCode);
    builder.append("]");
    return builder.toString();
  }

}