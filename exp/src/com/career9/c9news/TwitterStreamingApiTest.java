package com.career9.c9news;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterStreamingApiTest {

  public static void main(String[] args) throws TwitterException, IOException,
      NoSuchAlgorithmException, KeyManagementException {

    //final TweetStatusHandler tweetStatusHandler = new TweetStatusHandler();
    //BasicConfigurator.configure();
    TrustManager trm = new X509TrustManager() {
      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      public void checkClientTrusted(X509Certificate[] certs, String authType) {

      }

      public void checkServerTrusted(X509Certificate[] certs, String authType) {
      }
    };

    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, new TrustManager[]{trm}, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

    // Create all-trusting host name verifier
    HostnameVerifier allHostsValid = new HostnameVerifier() {
      public boolean verify(String hostname, SSLSession session) {
        return true;
      }
    };
    
    // Install the all-trusting host verifier
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    // System.setProperty("https.proxyHost", "localhost");
    // System.setProperty("https.proxyPort", "5865");
    
    StatusListener listener = new StatusListener() {
      public void onStatus(Status status) {
        //tweetStatusHandler.handle(status, true);
        System.out.println("-->"+status.getText());
      }
      
      public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
      }
      public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
      }
      public void onException(Exception ex) {
        ex.printStackTrace();
      }
      @Override
      public void onScrubGeo(long arg0, long arg1) {
        // TODO Auto-generated method stub

      }
      @Override
      public void onStallWarning(StallWarning arg0) {
        // TODO Auto-generated method stub

      }
    };
    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb.setDebugEnabled(false).setOAuthConsumerKey("LWNBPJMWmCYEbM249z1JBLn7n")
        .setOAuthConsumerSecret("EmRUIOjyDbDrTa2cve1QYrMsaX1jsMRFDTkhNqCM9ip5GghsaU")
        .setOAuthAccessToken("1560501828-bkh9m55jVuJ7hqqVQWXosQNvSaVLPZCRHfpGcYB")
        .setOAuthAccessTokenSecret("4NlfQNfHOv18kSOIKSlzpofOTHcAWR0FTwjK6RpZoaeVI");
    
    TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
    twitterStream.addListener(listener);
    FilterQuery fq = new FilterQuery();
    //fq = fq.track(new String[]{"solr"});
    //fq = fq.language(new String[]{"en"});

//    String keywords[] = {"France", "Germany", "#Solr"};
    //String keywords[] = {"Iphone 5s","May Allah","Wonder Woman", "Pankaj Singh","Sunil Grover","Too Much Fun","Torres"};//,"@narendramodi";//Katrina Kaif

    //    String keywords[] = {"news", "politics", "india", "USA", "fashion", "bollywood", "technology",
//        "startup", "sports", "world", "trends", "business", "war", "entertainment", "health",
//        "lifestyle", "cricket", "foosball", "hockey", "apple", "google", "facebook", "ebola", "amazon", "congress", "BJP", "narendra modi", "rahul gandhi"};

    String keywords[] = {"to"};
    fq.track(keywords);
    fq.language(new String[]{"en"});
    //71.89,8.3,88.78,30.98
    //72.24,9.25,87.29,29.92
    //fq.locations(new double[][]{new double[]{72.24,9.25},new double[]{87.29,29.92}});
    //fq.locations(new double[][]{new double[]{30.5800,8.1700},new double[]{71.5300,88.4600}});
    //"71.5300,88.4600,30.5800,8.1700"
    twitterStream.filter(fq);
    // sample() method internally creates a thread which manipulates TwitterStream and calls these adequate listener
    // methods continuously.
    //twitterStream.sample();
    
/*    TwitterFactory twitterFactory = new TwitterFactory(cb.build());
    Twitter twitter = twitterFactory.getInstance();
    Query query = new Query("we are hiring");
    query.setCount(100);
    QueryResult result = twitter.search(query);
    for (Status status : result.getTweets()) {
        System.out.println("@" + status.getUser().getScreenName() + ":" + status.getText());
        for (URLEntity entity : status.getURLEntities()) {
          System.out.println("\t\t"+entity.getExpandedURL()+ "==>"+resolveLocation(entity.getExpandedURL())+ "==>"+entity.getDisplayURL()+"==>"+entity.getURL());
        }
    }*/
  }
  
  public static String resolveLocation(String link) {
    try {
      final URL url = new URL(link);
      final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
      urlConnection.setInstanceFollowRedirects(false);
      final String location = urlConnection.getHeaderField("location");
      return location;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return link;
  }
}
