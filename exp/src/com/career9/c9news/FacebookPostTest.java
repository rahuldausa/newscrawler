package com.career9.c9news;

import java.net.MalformedURLException;
import java.net.URL;

import facebook4j.Account;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.PostUpdate;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;

public class FacebookPostTest {

  public static void main(String[] args) throws FacebookException, MalformedURLException {
    Facebook facebook = new FacebookFactory().getInstance();
    facebook.setOAuthAppId("1484087258490995", "7333247ec30b0371334a40e86a37a666");
    facebook.setOAuthPermissions("publish_stream,create_event");

   // AccessToken accessToken = facebook.getOAuthAppAccessToken();

    facebook.setOAuthAccessToken(new AccessToken("CAACEdEose0cBAHGysypqZCFt1nkLN5rDDiMN0wB7OQrsbiyy060DLx5QTJOYYu1MjzI71yO2MB72K7kRoXggMZCl0fj1pSD8avfKbJkCYZA0AKgMLDFPCruvCZCz5F9sHMZBZBio3foibQnegdCsURS4U8c02HRKlXuHlnOZC07eG4C7hxkKrgc1TZBjUcCSw9Loy1ZBnGPehL3b60q0ZA8piF", null));
    //facebook.setOAuthAccessToken(accessToken);
    ResponseList<Account> accounts = facebook.getAccounts();
    for (Account account : accounts) {
      System.out.println(account.getId() + "--"+account.getName()+"--"+account.getAccessToken());
    }
    //738811356179913--Rujhaan.com
    //375525422569883--Career9
    
    Account yourPageAccount = accounts.get(0); // if index 0 is your page account.
    String pageAccessToken = yourPageAccount.getAccessToken();
    System.out.println(pageAccessToken);
    
/*    PostUpdate post = new PostUpdate(new URL("http://facebook4j.org"))
    .picture(new URL("http://facebook4j.org/images/hero.png"))
    .name("Facebook4J - A Java library for the Facebook Graph API")
    .caption("facebook4j.org")
    .description("Facebook4J is a Java library for the Facebook Graph API.");
facebook.postFeed(post);*/

    // facebook.postLink(new URL("http://facebook4j.org"));
/*    facebook
        .postLink(
            new URL(
                "http://www.rujhaan.com/news/7-Year-Old-Fighting-Cancer-Has-Completely-Appropriate-Reaction-To-Learning-He-Can-Go-Home-9f95512f6a88ca9b73ff1187d5608b5d.html"),
            "7-Year-Old Fighting Cancer Has Completely Appropriate Reaction To Learning He Can Go Home");*/
//    
//    PostUpdate post = new PostUpdate(new URL("http://facebook4j.org"))
//        .picture(new URL("http://facebook4j.org/images/hero.png"))
//        .name("Facebook4J - A Java library for the Facebook Graph API").caption("facebook4j.org")
//        .description("Facebook4J is a Java library for the Facebook Graph API.");
    
    PostUpdate post = new PostUpdate(new URL("http://www.rujhaan.com/news/Rajnath-Singh-suggests-exemption-of-environment-clearance-for-border-projects-8e29f9333e8a001040e0229a2b12e58e.html"))
    //.picture(new URL("http://facebook4j.org/images/hero.png"))
    //.name("7-Year-Old Fighting Cancer Has Completely Appropriate Reaction To Learning He Can Go Home").caption("rujhaan.com")
    //.description("7-Year-Old Fighting Cancer Has Completely Appropriate Reaction To Learning He Can Go Home");
    .message("Rajnath Singh suggests exemption of environment clearance for border projects");
    //facebook.postFeed(post);
    facebook.postFeed(yourPageAccount.getId(), post);
  }
}
