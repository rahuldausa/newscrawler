package com.career9.c9news;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.ConfusionMatrix;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.JointClassifier;
import com.aliasi.classify.JointClassifierEvaluator;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.Files;

public class NewsClassifier {

  
  private static File TRAINING_DIR_20NEWSGROUP_DATASET = new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/20_newsgroups");
  
  private static File TRAINING_DIR = TRAINING_DIR_20NEWSGROUP_DATASET;//new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/fourNewsGroups/4news-train");
  
  private static File TESTING_DIR = new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/fourNewsGroups/4news-test");

/*  private static String[] CATEGORIES = {"soc.religion.christian", "talk.religion.misc",
      "alt.atheism", "misc.forsale"};*/
  
  private static String[] CATEGORIES = {"alt.atheism", "comp.graphics", "comp.os.ms-windows.misc",
      "comp.sys.ibm.pc.hardware", "comp.sys.mac.hardware", "comp.windows.x", "misc.forsale",
      "rec.autos", "rec.motorcycles", "rec.sport.baseball", "rec.sport.hockey", "sci.crypt",
      "sci.electronics", "sci.med", "sci.space", "soc.religion.christian", "talk.politics.guns",
      "talk.politics.mideast", "talk.politics.misc", "talk.religion.misc"};

  private static int NGRAM_SIZE = 6;

  public static void main(String[] args) throws ClassNotFoundException, IOException {
    String[] f = TRAINING_DIR_20NEWSGROUP_DATASET.list();
    System.out.println(Arrays.toString(f));
    long start = System.currentTimeMillis();
    NewsClassifier classifier = new NewsClassifier();
    classifier.loadClassifier(false);
    System.out.println("Time taken in loading classifier:"+(System.currentTimeMillis()-start));
    
    start = System.currentTimeMillis();
    String text = "I want to buy a motorcycle to travel to United States";
    //text = "The ASUS Z97-DELUXE is a mid-range socket LGA1150 motherboard based on the new Intel Z97 chipset, supporting the fourth and the forthcoming fifth generation Core i processors. It brings a high-end audio codec, 10 SATA-600 ports (allowing two SATA Express connections), 10 USB 3.0 ports, and Wi-Fi interface. Let�s take a good look at it.";
    String category = classifier.classify(text);
    System.out.println("Time taken in classifying:"+(System.currentTimeMillis()-start));

    System.out.println(category);
  }

  private JointClassifier<CharSequence> compiledClassifier;
  
  private File trainedClassifiedModel = new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/classifier_model");

  public void loadClassifier(boolean shouldTrain) throws IOException, ClassNotFoundException {
    // compiling
    System.out.println("Loading classifier");
    // we created object so know it's safe
    //compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable.compile(classifier);
    if (shouldTrain) {
      compiledClassifier = train();
    } else {
      if (!trainedClassifiedModel.exists()) {
        compiledClassifier = train();
      } else {
        compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable
            .readObject(trainedClassifiedModel);
      }
    }
  }
  
  private DynamicLMClassifier<NGramProcessLM> train() throws IOException, ClassNotFoundException {
    System.out.println(TRAINING_DIR.getAbsolutePath());
    DynamicLMClassifier<NGramProcessLM> classifier = DynamicLMClassifier.createNGramProcess(
        CATEGORIES, NGRAM_SIZE);

    for (int i = 0; i < CATEGORIES.length; ++i) {
      File classDir = new File(TRAINING_DIR, CATEGORIES[i]);
      if (!classDir.isDirectory()) {
        String msg = "Could not find training directory=" + classDir
            + "\nHave you unpacked 4 newsgroups?";
        System.out.println(msg); // in case exception gets lost in shell
        throw new IllegalArgumentException(msg);
      }

      String[] trainingFiles = classDir.list();
      for (int j = 0; j < trainingFiles.length; ++j) {
        File file = new File(classDir, trainingFiles[j]);
        String text = Files.readFromFile(file, "ISO-8859-1");
        System.out.println("Training on " + CATEGORIES[i] + "/" + trainingFiles[j]);
        Classification classification = new Classification(CATEGORIES[i]);
        Classified<CharSequence> classified = new Classified<CharSequence>(text, classification);
        classifier.handle(classified);
      }
    }
    // compiling
    System.out.println("Compiling");
    // we created object so know it's safe
    //compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable.compile(classifier);
    AbstractExternalizable.compileTo(classifier, trainedClassifiedModel);
    return classifier;
  }

  public String classify(String text) throws IOException {
    JointClassification jc = compiledClassifier.classify(text);
    String bestCategory = jc.bestCategory();
    String details = jc.toString();
    System.out.println("Got best category of: " + bestCategory);
    System.out.println(jc.toString());
    System.out.println("---------------");
    return bestCategory;
  }

  public String classify1(String content) throws IOException {

    boolean storeCategories = true;
    JointClassifierEvaluator<CharSequence> evaluator = new JointClassifierEvaluator<CharSequence>(
        compiledClassifier, CATEGORIES, storeCategories);
    for (int i = 0; i < CATEGORIES.length; ++i) {
      File classDir = new File(TESTING_DIR, CATEGORIES[i]);
      String[] testingFiles = classDir.list();
      for (int j = 0; j < testingFiles.length; ++j) {
        String text = Files.readFromFile(new File(classDir, testingFiles[j]), "ISO-8859-1");
        System.out.print("Testing on " + CATEGORIES[i] + "/" + testingFiles[j] + " ");
        Classification classification = new Classification(CATEGORIES[i]);
        Classified<CharSequence> classified = new Classified<CharSequence>(text, classification);
        evaluator.handle(classified);
        JointClassification jc = compiledClassifier.classify(text);
        String bestCategory = jc.bestCategory();
        String details = jc.toString();
        System.out.println("Got best category of: " + bestCategory);
        System.out.println(jc.toString());
        System.out.println("---------------");
      }
    }
    ConfusionMatrix confMatrix = evaluator.confusionMatrix();
    System.out.println("Total Accuracy: " + confMatrix.totalAccuracy());

    System.out.println("\nFULL EVAL");
    System.out.println(evaluator);
    return content;
  }
}
