package com.career9.c9news;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.sree.textbytes.network.HtmlFetcher;
import com.sree.textbytes.readabilityBUNDLE.Article;
import com.sree.textbytes.readabilityBUNDLE.ContentExtractor;
import com.sree.textbytes.readabilityBUNDLE.extractor.GooseExtractor;
import com.sree.textbytes.readabilityBUNDLE.extractor.ReadabilityExtractor;

public class ReadibilityTest {

  public static void main(String[] args) {
    
  }
  
  public void fetchContent(String url) throws Exception{
    Article article = new Article();

    ReadabilityExtractor re = new ReadabilityExtractor();
    //GooseExtractor goose = new GooseExtractor();

    ContentExtractor ce = new ContentExtractor();
    HtmlFetcher htmlFetcher = new HtmlFetcher();

    String html = null;
    
    html =  htmlFetcher.getHtml(url, 2000);

//    html = FileUtil.loadResourceAsString(ReadibilityTest.class, "mashable_com.html", false);
    //html = FileUtil.loadResourceAsString(ReadibilityTest.class, "freshersworld_com.html", false);

    //article = ce.extractContent(html);

    article = ce.extractContent(html, "ReadabilitySnack");
    //article = ce.extractContent(html, "ReadabilityGoose");
    //System.out.println(html);
    
    Document doc = Jsoup.parse(html);
    System.out.println("=>"+doc.select("p").text());
    System.out.println("===================================================");

    //article = ce.extractContent(html, "ReadabilityCore");

    // System.out.println("Content : " + article.getCleanedArticleText());

    // System.out.println("Content : " + article.getTopNode());
    
    
  /*  GooseExtractor goose = new GooseExtractor();
    System.out.println(re.getLinkDensity(article.getCleanedDocument()));
    
    System.out.println("=>"+goose.grabArticle(article));*/

    //System.out.println(article.getCleanedDocument().getAllElements());
    List<Text> texts = new ArrayList<Text>();
    for (Element e : article.getCleanedDocument().getAllElements()) {
      //System.out.println(e.attr("algoscore") + "--" + e.text());
      Text t = new Text();
      String val = e.attr("algoscore");
      if (val != null && !val.isEmpty()) {
        t.score = Double.parseDouble(val);
        t.content = e.text();
        texts.add(t);
      } else {
        //System.err.println(e.attr("algoscore") + "--" + e.text());
      }
    }

    Collections.sort(texts, Collections.reverseOrder(new Comparator<Text>() {

      @Override
      public int compare(Text t1, Text t2) {
        if (t1.score > t2.score)
          return 1;
        else if (t1.score < t2.score)
          return -1;
        else return 0;
      }
    }));

    for (Text text : texts) {
      System.out.println(text.score + "=>" + text.content);
    }
  }
  
  public static class Text {
    double score;
    String content;
  }
}
