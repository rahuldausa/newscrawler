package com.career9.c9news;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

import feed.CommonException;

public class Utils {

  private static final Logger log = Logger.getLogger(Utils.class);

  private static MessageDigest md;

  static {
    try {
      md = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  public static String uniqueId(String text) throws CommonException {
    try {
      String digest = null;
      byte[] bytesOfMessage = text.getBytes("UTF-8");
      byte[] hash = md.digest(bytesOfMessage);
      StringBuilder sb = new StringBuilder(2 * hash.length);
      for (byte b : hash) {
        sb.append(String.format("%02x", b & 0xff));
      }
      digest = sb.toString();
      return digest;
    } catch (Exception e) {
      throw new CommonException(e);
    }
  }
}
