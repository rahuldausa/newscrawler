package com.career9.c9news;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadFile {

  public static void main(String[] args) throws IOException {
    File f = new File("D:/work/f75/keywords_nov_uk");
    File f1 = new File("D:/work/f75/keywords_nov_uk_only_single");
    FileWriter fw = new FileWriter(f1);
    BufferedReader br = new BufferedReader(new FileReader(f));
    String line = null;
    while ((line = br.readLine()) != null) {
      if (line != null) {
        line = line.trim();
        if (line.contains(" ") || line.contains(".com")) {
          continue;
        }
        System.out.println(line);
        fw.write(line + "\n");
      }
    }
    fw.flush();
    fw.close();
  }
}
