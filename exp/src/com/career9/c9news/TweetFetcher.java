package com.career9.c9news;

import java.util.Iterator;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.news.impl.dao.DBMapper;
import com.rujhaan.news.impl.dao.MongoDao;

public class TweetFetcher {

  private MongoDao dao;

  public TweetFetcher() {
    String server = "127.0.0.1";
    //server = "63.142.240.102"; // TODO: to change
    
    this.dao = new MongoDao(server, "newstweet");
    //this.dao = new MongoDao("cctweetFeed");
  }

  public void update(ObjectId id, DBObject u) {
    BasicDBObject idObj = new BasicDBObject("_id", id);
    dao.update(idObj, u);
  }

  public Iterator<DBObject> getTweetRecord(ObjectId id) {
    BasicDBObject idObj = new BasicDBObject("_id", id);
    return dao.findByObjectId(idObj);
  }

  public Iterator<TweetRecord> fetchTweets(long lastTimestamp) {
    Iterator<DBObject> cursor = getTweetsRecords(lastTimestamp);
    return new TweetRecordIterator(cursor);
  }

  private Iterator<DBObject> getTweetsRecords(long lastTimestamp) {
    BasicDBObject queryFields = new BasicDBObject("_id", true).append("tweet_text", true)
        .append("url_links", true).append("ts", true);
    BasicDBObject queryCondition = new BasicDBObject();
    queryCondition.put("url_links", new BasicDBObject("$exists", true));
    queryCondition.put("url_links", new BasicDBObject("$ne", null));
    if (lastTimestamp == 0) {
      // Nope
    } else {
      queryCondition.put("ts", new BasicDBObject("$gt", lastTimestamp));
    }
    Iterator<DBObject> cursor = dao.findRecords(queryCondition, queryFields);
    return cursor;
  }

  class TweetRecordIterator implements Iterator<TweetRecord> {

    private Iterator<DBObject> dbObjectItr;

    public TweetRecordIterator(Iterator<DBObject> dbObjectItr) {
      this.dbObjectItr = dbObjectItr;
    }

    @Override
    public boolean hasNext() {
      return dbObjectItr.hasNext();
    }

    @Override
    public TweetRecord next() {
      DBObject doc = dbObjectItr.next();
      return DBMapper.toTweet(doc);
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }

}
