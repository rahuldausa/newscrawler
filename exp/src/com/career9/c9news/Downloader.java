package com.career9.c9news;
/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.regex.Pattern;

import org.apache.http.HttpStatus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.fetcher.PageFetchResult;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.parser.ParseData;
import edu.uci.ics.crawler4j.parser.Parser;
import edu.uci.ics.crawler4j.url.WebURL;
import feed.CommonException;

/**
 * This class is a demonstration of how crawler4j can be used to download a single page and extract its title and text.
 */
public class Downloader {

  private Parser parser;
  private C9PageFetcher pageFetcher;

  private Thread fetcherThread;
  private Thread contentParserThread;
  
  public Downloader() {
    CrawlConfig config = new CrawlConfig();
    //config.setProxyHost("localhost");
    //config.setProxyPort(5865);
    config.setConnectionTimeout(3000000);
    config.setSocketTimeout(60000);
    //config.setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36");
    config.setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0");
    parser = new Parser(config);
    pageFetcher = new C9PageFetcher(config);

    contentParserThread = new Thread(new ContentParser(this));
    contentParserThread.start();
    
    fetcherThread = new Thread(new Fetcher(this));
    fetcherThread.start();
    //BasicConfigurator.configure();
  }

  private Page download(String url) {
    WebURL curURL = new WebURL();
    curURL.setURL(url);
    PageFetchResult fetchResult = null;
    try {
      fetchResult = pageFetcher.fetchHeader(curURL);
      if (fetchResult.getStatusCode() == HttpStatus.SC_OK) {
        try {
          Page page = new Page(curURL);
          fetchResult.fetchContent(page);
          if (parser.parse(page, curURL.getURL())) {
            if(page==null) {
              System.out.println("in parse page is null Fetched content:");
            }
            return page;
          }else{
            System.out.println("Fetched content:"+new String(page.getContentData()));
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } finally {
      if (fetchResult != null) {
        fetchResult.discardContentIfNotConsumed();
      }
    }
    System.out.println("returing page null:"+fetchResult.getStatusCode());
    return null;
  }
  
/*  private File dir = new File("d://work/mytweetjob/techgig/");
  private File combined = new File("d://work/mytweetjob/techgig/combined_data");
  private File skills = new File("d://work/mytweetjob/techgig/skills.txt");
  private File location = new File("d://work/mytweetjob/techgig/location.txt");
  private File jobTitles = new File("d://work/mytweetjob/techgig/jobTitles.txt");
  private File companies = new File("d://work/mytweetjob/techgig/companies.txt");*/
  
  private File dir = new File("d://work/c9news/zippednews/raw/");
  private File topics = new File("d://work/c9news/zippednews/topics1.txt");
  private File pendingurls = new File("d://work/c9news/zippednews/pending_urls.txt");

  public boolean isAlreadyRead(String url){
    File file =null;
    try {
      file = new File(dir, Utils.uniqueId(url));
      if(file.exists()){
        return true;
      }
    } catch (CommonException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  public void processUrl(String url) {
    Page page = processPage(url);
    if (page != null) {
      parsePage(page);
    } else {
      System.out.println("Couldn't fetch the content of the page.");
    }
    System.out.println("==============");
  }
  
  public Page processPage(String url) {
    if (!dir.exists()) {
      dir.mkdirs();
    }

    System.out.println("Processing: " + url);
    Page page = download(url);
    if (page != null) {
      
      FileWriter dataFileWriter =null;
      File file = null;
      //String url = page.getWebURL().getURL();
      try {
        file = new File(dir, Utils.uniqueId(url));
        file.createNewFile();
      } catch (CommonException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
      try {
        dataFileWriter = new FileWriter(file);
      } catch (IOException e) {
        e.printStackTrace();
      }
      String html  = new String(page.getContentData());
      System.out.println("html: size:"+html.length() + " for url:"+url);
      try {
        dataFileWriter.write(html);
        dataFileWriter.flush();
        dataFileWriter.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
      return page;
    } else {
      System.out.println("page is null");
    }
    System.out.println("==============");
    return page;
  }
  
  public void parsePage(Page page){
    ParseData parseData = page.getParseData();
    //System.out.println(parseData + "--"+parseData.getClass());
    if (parseData != null) {
      if (parseData instanceof HtmlParseData) {
        HtmlParseData htmlParseData = (HtmlParseData) parseData;
        System.out.println("Title: " + htmlParseData.getTitle());
        System.out.println("Text length: " + htmlParseData.getText().length());
        System.out.println("Html length: " + htmlParseData.getHtml().length());
        //System.out.println(htmlParseData.getHtml());
        
/*        FileWriter dataFileWriter =null;
        File file = null;
        String url = page.getWebURL().getURL();
        try {
          file = new File(dir, Utils.uniqueId(url));
          file.createNewFile();
        } catch (CommonException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
        try {
          dataFileWriter = new FileWriter(file);
        } catch (IOException e) {
          e.printStackTrace();
        }
        String html  = htmlParseData.getHtml();
        System.out.println("html: size:"+html.length() + " for url:"+url);
        try {
          dataFileWriter.write(html);
          dataFileWriter.flush();
          dataFileWriter.close();
        } catch (IOException e) {
          e.printStackTrace();
        }*/

        for (WebURL weburl : htmlParseData.getOutgoingUrls()) {
          try {
            String href = weburl.getURL();
            if (!FILTERS.matcher(href).matches() && !isAlreadyRead(href) && isInDomain(href)) {
              //System.out.println("Adding:"+href);
              urls.put(href);
              urlsWriter.append(href+"\n");
              flush(urlsWriter);
            }
          } catch (IOException e) {
            e.printStackTrace();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }

        Document jsoupDoc = Jsoup.parse(htmlParseData.getHtml());

        Elements topicsElem = jsoupDoc.select("#bodyarea > div > div > a > p");
        try {
          topicsfileWriter.append(multiSelect(topicsElem)+"\n");
          flush(topicsfileWriter);
        } catch (IOException e) {
          e.printStackTrace();
        }
        
        topicsElem = jsoupDoc.select("#bodyarea > div > div.col-sm-9.col-lg-9.col-md-9 > p.tags > a");
        try {
          topicsfileWriter.append(multiSelect(topicsElem)+"\n");
          flush(topicsfileWriter);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    } else {
      System.out.println("Couldn't parse the content of the page.");
    }
  }
  
  private boolean isInDomain(String href) {
    if(href.startsWith("http://www.zippednews.com/")){
      return true;
    }
    return false;
  }

  private final static Pattern FILTERS = Pattern.compile(".*(\\.(ico|svg|css|js|bmp|gif|jpe?g"
      + "|png|tiff?|mid|mp2|mp3|mp4" + "|wav|avi|mov|mpeg|ram|m4v|pdf"
      + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
  
  public List<String> multiSelect( Elements elements ) {
    List<String> values = new ArrayList<String>();
    String val=null;
    for (Element e : elements) {
      values.add(e.ownText());
      for (Element e1 : e.children()) {
        val = e1.text();
        val = val.trim();
        if (val != null && !val.isEmpty()) {
          values.add(val);
        }
      }
    }
    return values;
  }
  
  public void flush(FileWriter fw){
    try {
      fw.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  public static void close(FileWriter fw) {
    try {
      fw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  public static BlockingQueue<String> urls = new ArrayBlockingQueue<>(100000);
  public static BlockingQueue<Page> parserQueue = new ArrayBlockingQueue<>(20);
  
  public static class Fetcher implements Runnable{
    
    private Downloader downloader;
    private Random rand;
    
    public Fetcher(Downloader downloader) {
      this.downloader = downloader;
      rand = new Random(1000);
    }
    
    @Override
    public void run() {
      System.out.println("Fetcher thread started");
      while(true){
        try {
          String url = urls.take();
          System.out.println("Fetching url:"+url);
          Page page = downloader.processPage(url);
          if (page != null) {
            parserQueue.put(page);
          }
          System.out.println("Fetch done url:"+url);
          int duration = rand.nextInt(2000);
          System.out.println("sleeping for:"+duration);
          try {
            Thread.sleep(duration);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }
  
  public static class ContentParser implements Runnable{
    
    private Downloader downloader;
    
    public ContentParser(Downloader downloader) {
      this.downloader = downloader;
    }
    
    @Override
    public void run() {
      System.out.println("ContentParser thread started");
      while(true){
        try {
          Page page = parserQueue.take();
          System.out.println("Parsing page:"+page.getWebURL().getURL());
          downloader.parsePage(page);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  FileWriter topicsfileWriter =null;
  FileWriter urlsWriter=null;
  
  private void addToQueue(String url){
    try {
      urls.put(url);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  
  public static void main(String[] args) {
    Downloader downloader = new Downloader();
    //downloader.processUrl("http://www.jobscore.com/jobs/shmoop/feed.atom");
    //List<String> urls = new ArrayList<>();

    try {
      if (!downloader.topics.exists()) {
        downloader.topics.createNewFile();
      }
      downloader.topicsfileWriter = new FileWriter(downloader.topics,true);
      downloader.urlsWriter = new FileWriter(downloader.pendingurls,true);
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    //downloader.addToQueue("http://www.zippednews.com/crsqsearch/m");
    
    for (int i = (int) 'c'; i <= (int) 'f'; i++) {
      System.out.println((char) i);
      downloader.addToQueue("http://www.zippednews.com/crsqsearch/" + (char) i);// http://www.zippednews.com/crsqsearch/m
    }

/*    for (int i = 0; i <= 9; i++) {
      System.out.println(i);
      downloader.addToQueue("http://www.zippednews.com/crsqsearch/" + i);// http://www.zippednews.com/crsqsearch/9
    }*/
    
    //downloader.addToQueue("http://www.zippednews.com/crsqsearch/d");

/*    for (String url : urls) {
      downloader.processUrl(url);
      try {
        Thread.sleep(10000L);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }*/
    //close(downloader.topicsfileWriter);

  }
}