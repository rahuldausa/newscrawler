package com.career9.c9news;

import java.net.UnknownHostException;
import java.util.Iterator;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MongoDao1 {

  private DB db;
  private DBCollection items;

  public MongoDao1() {
    try {
      // on constructor load initialize MongoDB and load collection
      initMongoDB();
      items = db.getCollection("newstweets");
    } catch (MongoException ex) {
      System.out.println("MongoException :" + ex.getMessage());
    }
  }
  /**
   * initMongoDB been called in constructor so every object creation this initialize MongoDB.
   */
  public void initMongoDB() throws MongoException {
    try {
      System.out.println("Connecting to Mongo DB..");
      Mongo mongo;
      mongo = new Mongo("127.0.0.1");
      db = mongo.getDB("tweetDB");
    } catch (UnknownHostException ex) {
      System.out.println("MongoDB Connection Errro :" + ex.getMessage());
    }
  }

  public void insert(DBObject... dbObject) {
    items.insert(dbObject);
  }
  
  public void update(DBObject q, DBObject u) {
    items.update(q, u);
  }

  /**
   * void method print fetched records from mongodb This method use the preloaded items (Collection) for fetching
   * records and print them on console.
   * @return
   */
  public Iterator<DBObject> findRecords(BasicDBObject queryFields) {
    DBCursor cursor = items.find(new BasicDBObject(), queryFields);
    return cursor.iterator();
  }
  
  public Iterator<DBObject> findByObjectId(BasicDBObject objId) {
    DBCursor cursor = items.find(objId);
    return cursor.iterator();
  }

  public Iterator<DBObject> streamResults(BasicDBObject queryCondition, BasicDBObject queryFields) {
    //DBObject query = BasicDBObjectBuilder.start("_id", "5360bfc092eac1a91eb6d378").get();
    //DBObject sortBy = BasicDBObjectBuilder.start("$natural", 1).get();
    DBCursor cursor = items.find(queryCondition,queryFields);
    return cursor.iterator();
  }
}
