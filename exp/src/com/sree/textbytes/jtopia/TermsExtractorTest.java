package com.sree.textbytes.jtopia;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TermsExtractorTest {

  private static File text_data_dir = new File("d:/work/c9news/text_data1/");

  public static void main(String[] args) {
    TermsExtractorTest test = new TermsExtractorTest();

    File[] list = text_data_dir.listFiles();
    for (File f1 : list) {
      File[] ls = f1.listFiles();
      System.out.println("-----------------------------------------");
      for (File f2 : ls) {
        test.terms(f2);
      }
    }
  }

  public void terms(File file) {
    TermsExtractor termExtractor = new TermsExtractor();
    TermDocument topiaDoc = new TermDocument();

    StringBuffer stringBuffer = new StringBuffer();

    FileInputStream fileInputStream = null;
    BufferedReader bufferedReader = null;
    try {
      fileInputStream = new FileInputStream(file);
    } catch (FileNotFoundException e) {
    }
    DataInputStream dataInputStream = new DataInputStream(fileInputStream);
    bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));
    String line = "";
    try {
      while ((line = bufferedReader.readLine()) != null) {
        stringBuffer.append(line + "\n");
      }
    } catch (IOException e) {
    }
    topiaDoc = termExtractor.extractTerms(stringBuffer.toString());
    //System.out.println("Extracted terms : " + topiaDoc.getExtractedTerms());
    System.out.println(topiaDoc.getFinalFilteredTerms());
  }
}
