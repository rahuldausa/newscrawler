#!/bin/sh

#if ""%1"" == ""compile"" ant -f ..\build.xml;

cd .

CLASSPATH=../lib/*:../lib/crawler4j/*:../target/*

JAVA_OPTS=-Dconfig.file=../resources/config.properties -Dlog4j.configuration=file:../resources/log4j.xml -Dlog4j.debug=true

JVM_ARGS=-XX:+UseConcMarkSweepGC -XX:+ExplicitGCInvokesConcurrent -XX:+AggressiveOpts -XX:+HeapDumpOnOutOfMemoryError -XX:OnOutOfMemoryError=kill -9 %p -XX:HeapDumpPath=heapdump.hprof -XX:OnOutOfMemoryError ="sh ~/cleanup.sh" -verbose:gc -Xloggc:../log/gc.log -XX:+PrintGCTimeStamps -XX:+PrintGCDetails

java -server -Xms1024m -Xmx1512m $JVM_ARGS $JAVA_OPTS -classpath $CLASSPATH com.rujhaan.rocky.bootstrap.RockyServer