if ""%1"" == ""compile"" ant -f ..\build.xml;

cd .
SET CLASSPATH=..\lib\*;..\lib\crawler4j\*;..\target\*

SET JAVA_OPTS=-Dconfig.file=..\src\main\resources\config.properties -Dlog4j.configuration=file:..\src\main\resources\log4j.xml -Dlog4j.debug=true

SET JVM_ARGS=
-server \
-Xmx1G \
-XX:+UseConcMarkSweepGC \
-XX:+ExplicitGCInvokesConcurrent \
-XX:+AggressiveOpts \
-XX:+HeapDumpOnOutOfMemoryError \
-XX:OnOutOfMemoryError=kill -9 %p
-XX:HeapDumpPath=heapdump.hprof
-XX:OnOutOfMemoryError ="sh ~/cleanup.sh"
-verbose:gc
-Xloggc:<JIRA_HOME>/log/gc.log
-XX:+PrintGCTimeStamps
-XX:+PrintGCDetails

java -server -Xms1024m -Xmx1512m %JVM_ARGS% %JAVA_OPTS% -classpath %CLASSPATH% com.rujhaan.rocky.bootstrap.RockyServer
