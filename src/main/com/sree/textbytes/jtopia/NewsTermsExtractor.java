package com.sree.textbytes.jtopia;

import java.util.ArrayList;
import java.util.Map;

public class NewsTermsExtractor {

  private TermsExtractor termExtractor = new TermsExtractor();

  public NewsTermsExtractor() {
    // for default lexicon POS tags
    Configuration.setTaggerType("default");
    // for openNLP POS tagger
    // Configuration.setTaggerType("openNLP");
    // for Stanford POS tagger
    //Configuration.setTaggerType("stanford");
    Configuration.setSingleStrength(3);
    Configuration.setNoLimitStrength(2);
    // if tagger type is "openNLP" then give the openNLP POS tagger path
    // Configuration.setModelFileLocation("model/openNLP/en-pos-maxent.bin");
    // if tagger type is "default" then give the default POS lexicon file
    Configuration.setModelFileLocation("model/default/english-lexicon.txt");
    // if tagger type is "stanford "
    //Configuration.setModelFileLocation("model/stanford/english-left3words-distsim.tagger");
  }

  public Map<String, ArrayList<Integer>> topics(String text) {
    TermDocument topiaDoc = termExtractor.extractTerms(text);
    if (topiaDoc == null) {
      return null;
    }
    return topiaDoc.getFinalFilteredTerms();
  }

  public static void main(String[] args) {
    NewsTermsExtractor termsExtractor = new NewsTermsExtractor();
    String text="Why did you build Rao Tula Ram as a single flyover? asks High Court to PWD http://t.co/XieIzBzQkz";
    text="Mumbai: Union Minister of Road Transport Nitin Gadkari on Tuesday congratulated Devendra Fadvanis after he was unanimously elected as the leader of the BJP legislature party in Maharashtra. Fadvanis is now officially appointment as the first BJP chief minister of the state. “Am sure that under Devendra Fadnavis Maharashtra will get a new direction, will develop under his leadership,” Gadkari told reporters here. On Sunday, the BJP had announced that the party was willing to form the government in Maharashtra in alliance with the Shiv Sena. ";
    System.out.println(termsExtractor.topics(text));
  }
}
