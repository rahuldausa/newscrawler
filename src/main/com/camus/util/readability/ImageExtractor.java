package com.camus.util.readability;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;


public class ImageExtractor {

  public static final Logger logger = Logger.getLogger(ImageExtractor.class);

  private DefaultHttpClient httpClient;

  public ImageExtractor() {
    httpClient = new DefaultHttpClient();
  }

  public static String confDir = "resources/";

  public String downloadImageToTempFile(String storyURL, String imageURL) {
    String tempImageFilePath = null;
    HttpGet httpget = null;
    try {

      logger.info("ImageExtractor imageURL: " + imageURL);
      if (imageURL.indexOf("http://") > 0) {
        imageURL = "http://" + imageURL;
      }

      System.out.println(imageURL);
      URLConnection connection = new URL(imageURL).openConnection();
      String contentType = connection.getHeaderField("Content-Type");
      logger.info("Content-Type in downloadImageToTempFile: " + contentType);

      // isImage = null;//contentType.startsWith("image/");
      boolean isImage = true;

      HttpContext localContext = new BasicHttpContext();
      httpget = new HttpGet(imageURL);
      
      if (isImage) {
        HttpResponse response = httpClient.execute(httpget, localContext);
        String respStatus = response.getStatusLine().toString();
        if (!respStatus.contains("200")) {
          // throw Exception
          // return null;
        }

        // find from file name
        int dotPosition = imageURL.lastIndexOf(".") + 1;
        contentType = imageURL.substring(dotPosition, imageURL.length());
        // }

        String fileName = ContentExtractor.md5(storyURL) + "." + contentType;
        logger.info("fileName contentType: " + contentType);

        Properties p = new Properties();
        try {
          InputStream stream = ImageExtractor.class.getClassLoader().getResourceAsStream(confDir+"config.properties");
          p.load(new InputStreamReader(stream));
          System.out.println("--"+p.getProperty("crawler.download-images-dir"));
        } catch (Exception e) {
          e.printStackTrace();
        }
        
        tempImageFilePath = p.getProperty("crawler.download-images-dir") + fileName;
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          File f = new File(tempImageFilePath);
          if(!f.exists()){
            return null;
          }
          InputStream instream = entity.getContent();
          OutputStream outstream = new FileOutputStream(tempImageFilePath);
          logger.info("saving story image file to " + tempImageFilePath);
          try {
            IOUtils.copy(instream, outstream);
          } catch (Exception e) {
            throw e;
          } finally {
            instream.close();
            outstream.close();
          }

        }
        logger.info("downloadImageToTempFile ended");
      }

    } catch (Exception e) {
      e.printStackTrace();
      logger.error("Error in downloadImageToTempFile: " + e);
    } finally {
      if (httpget != null) {
        httpget.releaseConnection();
      }
    }
    return tempImageFilePath;
  }

  public void destroy() {
    httpClient.getConnectionManager().shutdown();
  }

  public static void main(String[] args) {
    Properties p = new Properties();
    try {
      InputStream stream = ImageExtractor.class.getClassLoader().getResourceAsStream(confDir+"rocky.properties");
      p.load(new InputStreamReader(stream));
      System.out.println("--"+p.getProperty("crawler.download-images-dir"));
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    ImageExtractor imgEx = new ImageExtractor();
    String storyURL = "http://indianexpress.com/article/india/politics/no-contradiction-between-party-govt-on-saradha-chit-fund-scam-bjp/";
    String imageURL = "http://images.indianexpress.com/2014/10/amit-shah-t1.jpg";
    String localImgPath = imgEx.downloadImageToTempFile(storyURL, imageURL);
    System.out.println(localImgPath);
  }
}
