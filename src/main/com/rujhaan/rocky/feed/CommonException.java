package com.rujhaan.rocky.feed;


public class CommonException extends Exception {

  private static final long serialVersionUID = 1L;

  public CommonException() {
  }

  public CommonException(Throwable e) {
    super(e);
  }

  public CommonException(String message) {
    super(message);
  }

}
