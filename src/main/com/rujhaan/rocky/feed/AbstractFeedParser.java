package com.rujhaan.rocky.feed;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndPersonImpl;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public abstract class AbstractFeedParser implements FeedParser {

  protected abstract void parseIndividual(SyndEntry entry, KeyValuePair param);

  public List<KeyValuePair> parseFeed(InputStream is) throws CommonException {
    try {
      SyndFeedInput input = new SyndFeedInput();
      SyndFeed feed = input.build(new XmlReader(is));

      List<KeyValuePair> params = new ArrayList<KeyValuePair>();
      parse(feed, params);
      return params;
    } catch (Exception e) {
      throw new CommonException(e);
    }
  }

  protected void parse(SyndFeed feed, List<KeyValuePair> params) {
    List<SyndEntry> entries = feed.getEntries();
    Iterator<SyndEntry> itEntries = entries.iterator();

    String author = feed.getAuthor();

    if (author == null || author.isEmpty()) {
      for (Object obj : feed.getAuthors()) {
        SyndPersonImpl syndObj = ((SyndPersonImpl) obj);
        author = syndObj.getName();
        System.out.println(author);
        if (author != null && !author.isEmpty()) {
          break;// come out of loop
        }
      }
    }
    if (author != null && author.endsWith(" HR")) {
      author = author.replace(" HR", "");
    }
    while (itEntries.hasNext()) {
      KeyValuePair param = new KeyValuePair();
      SyndEntry entry = itEntries.next();
      String entryAuthor = entry.getAuthor();
      if (entryAuthor != null && !entryAuthor.isEmpty()) {
        param.add(AnalysisParams.AUTHOR, entryAuthor);
      } else {
        param.add(AnalysisParams.AUTHOR, author);
      }

      param.add(AnalysisParams.TITLE, entry.getTitle());
      param.add(AnalysisParams.URL, entry.getLink());
      param.add(AnalysisParams.PUBLISHED_DATE, entry.getPublishedDate());
      param.add(AnalysisParams.UPDATED_DATE, entry.getUpdatedDate());

      SyndContent content = entry.getDescription();
      if (content != null) {
        String value = content.getValue();
        param.add(AnalysisParams.DESC, value);
      }

      // Some feed may not have descrption tag, they may have only content like jobscore
      List<SyndContent> contents = entry.getContents();
      if (contents != null && contents.size() > 0) {
        SyndContent syndcontent = contents.get(0);
        String type = syndcontent.getType();
        String value = syndcontent.getValue();;
        if (type != null && type.equals("html")) {
          // TODO: apply html filtering here for value
          Document doc = Jsoup.parse(value);
          String text = doc.text();
          // System.out.println(text);
          value = text;
        }
        param.add(AnalysisParams.DESC, value);
      }

      List<SyndCategoryImpl> categories = entry.getCategories();
      if (categories != null) {
        int cnt=0;
        for (SyndCategoryImpl category : categories) {
          if (category.getTaxonomyUri() != null) {
            param.add(category.getTaxonomyUri(), category.getName());
          } else {
            param.add("category-"+cnt++, category.getName());
          }
        }
      }

      // Give a chance for Individual parases to set values
      this.parseIndividual(entry, param);
      params.add(param);
    }
  }
}
