package com.rujhaan.rocky.feed;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.rujhaan.rocky.util.v1.FileUtil;

public class TestFeedParser {

  @Test
  public void testFeedParse() {
    C9FeedParser feedParser = new C9FeedParser();
    String urlstr = "D:/work/c9news/indiatimes_entertainment.rss";
    File f = new File(urlstr);
    try {
      List<KeyValuePair> results = feedParser.parse(
          "http://www.indiatimes.com/entertainment.rss", FileUtil.contentAsString(f));
      for (KeyValuePair kv : results) {
        System.out.println(kv);
      }
    } catch (CommonException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
