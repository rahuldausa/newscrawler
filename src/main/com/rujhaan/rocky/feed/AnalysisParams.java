package com.rujhaan.rocky.feed;

public interface AnalysisParams  {

  String TITLE = "title";
  String URL = "url";
  String DESC = "desc";
  String AUTHOR = "company";
  String SALARY_RANGE = "salary_range";
  String LOCATION = "location";
  String EMPLOYMENT_TYPE = "employment_type";
  String PUBLISHED_DATE = "published_at";
  String UPDATED_DATE = "updated_at";
  String DEPARTMENT = "job_department";
  
}
