package com.rujhaan.rocky.bootstrap;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.rujhaan.rocky.bootstrap.RockyServer.ConfigProp;

public class CompomentScheduler {

  private static final Logger log = Logger.getLogger(CompomentScheduler.class);

  private ScheduledExecutorService executorService;
  private List<Component> components;

  public CompomentScheduler(List<Component> components) {
    this.components = components;
  }

  public void open(ConfigProp configProp) {
    executorService = Executors.newScheduledThreadPool(1);
  }

  public void start() {
    ComponentScheduleJob job = new ComponentScheduleJob(components);
    executorService.schedule(job, 1, TimeUnit.DAYS);
  }

  public void stop() {
    executorService.shutdown();
  }

  class ComponentScheduleJob implements Runnable {

    private List<Component> components;

    public ComponentScheduleJob(List<Component> components) {
      this.components = components;
    }

    @Override
    public void run() {
      log.info("CrawlScheduler is running at: " + new Date());
      for (Component component : components) {
        component.restart();
      }
    }
  }

}
