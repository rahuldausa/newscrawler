package com.rujhaan.rocky.bootstrap;

import com.rujhaan.rocky.bootstrap.RockyServer.ConfigProp;

public interface Component {

  public void open(ConfigProp configProp);

  public void start();

  public void stop();

  // Called by scheduler
  public void restart();
}
