package com.rujhaan.rocky.bootstrap;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.rujhaan.rocky.twitter.CelebsTwitterList;
import com.rujhaan.rocky.twitter.news.bootstrap.TwitterFeeder;
import com.rujhaan.rocky.twitter.news.bootstrap.NewsTweetFetcher;
import com.rujhaan.rocky.twitter.trends.bootstrap.TrendsTweetFetcher;

public class RockyServer {

  private static Logger log = Logger.getLogger(RockyServer.class);

  private List<Component> components;

  private static ConfigProp configProp;
  private CompomentScheduler componentScheduler;
  public static String confDir = "resources/";

  static {
    configProp = loadConfig();
  }

  public static void main(String[] args) {
    // /BasicConfigurator.configure();
    RockyServer crawlServer = new RockyServer();
    Runtime.getRuntime().addShutdownHook(crawlServer.new ShutdownHandler(crawlServer));
    crawlServer.init();
    crawlServer.start();
  }

  public static ConfigProp getConfig() {
    return configProp;
  }

  private static ConfigProp loadConfig() {
    Properties props = new Properties();
    String configFilePath = System.getProperty("config.file", confDir+"config.properties");
    if (configFilePath == null) {
      throw new IllegalArgumentException("Can not find the config file at:" + configFilePath);
    }

    File file = new File(configFilePath);
    try {
      props.load(new InputStreamReader(RockyServer.class.getClassLoader().getResourceAsStream(configFilePath)));
      System.out.println("Loded configFilePath " + configFilePath + "successfully...");
    } catch (IOException e) {
      log.warn("Can not load configuration for Crawler. trying with classpath", e);
      try {
        props.load(RockyServer.class.getClassLoader().getResourceAsStream(configFilePath));
        System.out.println("Loded configFilePath " + configFilePath + " successfully...");
      } catch (IOException e1) {
        log.fatal("Can not load configuration for Crawler", e1);
      }
    }
    return new ConfigProp(props);
  }

  public void init() {
    components = new ArrayList<>();

    Component twitterNewsTweetComponent = new NewsTweetFetcher();
    Component tweetFeeder = new TwitterFeeder();
    Component trendsTweetFetcher = new TrendsTweetFetcher();
    Component celebsTweetFetcher = new CelebsTwitterList();

    components.add(twitterNewsTweetComponent);
    components.add(tweetFeeder);
    components.add(trendsTweetFetcher);
    components.add(celebsTweetFetcher);

    for (Component component : components) {
      component.open(configProp);
    }

    componentScheduler = new CompomentScheduler(components);
    componentScheduler.open(configProp);
  }

  public void start() {
    for (Component component : components) {
      component.start();
    }
    //componentScheduler.start();
  }

  public void stop() {
    for (Component component : components) {
      component.stop();
    }
  }

  public class ShutdownHandler extends Thread {

    private RockyServer crawlServer;

    public ShutdownHandler(RockyServer crawlServer) {
      this.crawlServer = crawlServer;
    }

    @Override
    public void run() {
      System.out.println("Received a shutdown signal");
      crawlServer.stop();
    }
  }

  public static class ConfigProp {

    private Properties props;

    public ConfigProp(Properties props) {
      this.props = props;
    }

    public String getProperty(String key) {
      return props.getProperty(key);
    }

    public String getStringProperty(String key, String defaultVal) {
      String val = getProperty(key);
      if (val == null || val.isEmpty()) {
        return defaultVal;
      }
      return val;
    }

    public Integer getIntProperty(String key, Integer defaultVal) {
      String val = getProperty(key);
      if (val == null || val.isEmpty()) {
        return defaultVal;
      }
      int finalval = Integer.parseInt(val);
      return finalval;
    }

    public Boolean getBooleanProperty(String key, Boolean defaultVal) {
      String val = getProperty(key);
      if (val == null || val.isEmpty()) {
        return defaultVal;
      }
      boolean finalval = Boolean.parseBoolean(val);
      return finalval;
    }
  }

}
