package com.rujhaan.rocky.domain;

import java.io.Serializable;
import java.util.List;

import com.rujhaan.news.domain.Topic;

public class TrendsInfo implements Serializable {

  private static final long serialVersionUID = 1L;
  int cIid;
  List<String> trends;
  List<Topic> trendingTopics;

  public int getcIid() {
    return cIid;
  }
  public void setcIid(int cIid) {
    this.cIid = cIid;
  }
  public List<String> getTrends() {
    return trends;
  }
  public void setTrends(List<String> trends) {
    this.trends = trends;
  }
  public List<Topic> getTrendingTopics() {
    return trendingTopics;
  }
  public void setTrendingTopics(List<Topic> trendingTopics) {
    this.trendingTopics = trendingTopics;
  }


}
