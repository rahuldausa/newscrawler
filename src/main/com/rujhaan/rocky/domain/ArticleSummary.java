package com.rujhaan.rocky.domain;

import java.util.List;

import com.sree.textbytes.readabilityBUNDLE.Article;

public class ArticleSummary {
  
  private Article article;
  private List<Text> texts;

  public Article getArticle() {
    return article;
  }
  public void setArticle(Article article) {
    this.article = article;
  }
  public List<Text> getTexts() {
    return texts;
  }
  public void setTexts(List<Text> texts) {
    this.texts = texts;
  }
}