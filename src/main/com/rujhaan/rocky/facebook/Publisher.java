package com.rujhaan.rocky.facebook;

import com.rujhaan.news.domain.NewsFeed;

public interface Publisher {

  public void publish(NewsFeed feed);
}
