package com.rujhaan.rocky.facebook;

import java.net.MalformedURLException;
import java.net.URL;

import com.rujhaan.news.domain.NewsFeed;
import com.rujhaan.news.domain.Response;

import facebook4j.Account;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.PostUpdate;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;

public class FacebookPublisher implements Publisher {
  
  static Facebook facebook;
  static Account yourPageAccount;
  
  public FacebookPublisher() {
    facebook = new FacebookFactory().getInstance();
    facebook.setOAuthAppId("1484087258490995", "7333247ec30b0371334a40e86a37a666");
    facebook.setOAuthPermissions("publish_stream,create_event");

   // AccessToken accessToken = facebook.getOAuthAppAccessToken();

    facebook.setOAuthAccessToken(new AccessToken("CAACEdEose0cBAMSQZAQT14qZCMN4eMgSP6eX6aeVn6nXPWnnZCJ15QuCptOzZAUdM1H2jQLiw3tNZAhmm20pfnZACDcCB8xrCMZAzPk1pbD7XykNWqm3yZBAzF1AHWUX3pwzevgia90Xv6pUTf3iYj3aOuZBiug8bZBtzPIQwgZC0ikpCsQGuLPFZCVcCWauBCjkHvZBVZAKvEW7KmOvexdozXJXKZB", null));
    //facebook.setOAuthAccessToken(accessToken);
    ResponseList<Account> accounts = null;
    try {
      accounts = facebook.getAccounts();
    } catch (FacebookException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    for (Account account : accounts) {
      System.out.println(account.getId() + "--"+account.getName()+"--"+account.getAccessToken());
    }
    //738811356179913--Rujhaan.com
    //375525422569883--Career9
    
    yourPageAccount = accounts.get(0); // if index 0 is your page account.
    String pageAccessToken = yourPageAccount.getAccessToken();
    System.out.println(pageAccessToken);
  }

  public static String URL_FORMAT = "http://www.rujhaan.com/news/%s-%s.html";
    
  @Override
  public void publish(NewsFeed feed) {
    PostUpdate post;
    try {
//      post = new PostUpdate(new URL("http://www.rujhaan.com/news/Rajnath-Singh-suggests-exemption-of-environment-clearance-for-border-projects-8e29f9333e8a001040e0229a2b12e58e.html"))
      String articleurl = Response.applyEncoding(feed.getTitle());
      
      String newsUrl = String.format(URL_FORMAT, articleurl, feed.getId());
      post = new PostUpdate(new URL(newsUrl))

      //.picture(new URL("http://facebook4j.org/images/hero.png"))
      //.name("7-Year-Old Fighting Cancer Has Completely Appropriate Reaction To Learning He Can Go Home").caption("rujhaan.com")
      //.description("7-Year-Old Fighting Cancer Has Completely Appropriate Reaction To Learning He Can Go Home");
      .message(feed.getTitle());
      //facebook.postFeed(post);
      System.out.println("Publishing to facebook +"+feed.getTitle() + " with link:"+newsUrl);
      facebook.postFeed(yourPageAccount.getId(), post);
    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (FacebookException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    
  }

}
