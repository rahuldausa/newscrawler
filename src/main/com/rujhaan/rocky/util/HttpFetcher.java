package com.rujhaan.rocky.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import twitter4j.Logger;

public class HttpFetcher {

  public static final Logger log = Logger.getLogger(HttpFetcher.class);

  protected HttpClient proxyClient;

  public HttpFetcher() {
    this(1000,1000);
  }
  
  public HttpFetcher(int soTimeout, int connecttionTimeout) {
    HttpParams hcParams = new BasicHttpParams();
    hcParams.setParameter(ClientPNames.HANDLE_REDIRECTS, true);
    HttpConnectionParams.setConnectionTimeout(hcParams, connecttionTimeout); // http.connection.timeout;//100 ms
    HttpConnectionParams.setSoTimeout(hcParams, soTimeout); // http.socket.timeout;//100 ms
    proxyClient = createHttpClient(hcParams);
  }

  /**
   * Called from {@link #init(javax.servlet.ServletConfig)}. HttpClient offers many opportunities for customization.
   * @param hcParams
   */
  protected HttpClient createHttpClient(HttpParams hcParams) {
    TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
      @Override
      public boolean isTrusted(X509Certificate[] certificate, String authType) {
        return true;
      }
    };
    org.apache.http.conn.ssl.SSLSocketFactory sf = null;
    try {
      sf = new org.apache.http.conn.ssl.SSLSocketFactory(acceptingTrustStrategy,
          org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER) {
      };
    } catch (KeyManagementException | UnrecoverableKeyException | NoSuchAlgorithmException
        | KeyStoreException e) {
      e.printStackTrace();
    }
    SchemeRegistry registry = new SchemeRegistry();
    registry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
    registry.register(new Scheme("https", 443, sf));
    //registry.register(new Scheme("https", 443, sf));
    //registry.register(new Scheme("http", 80, sf));

    ClientConnectionManager ccm = new ThreadSafeClientConnManager(registry);
    //ClientConnectionManager ccm = new ThreadSafeClientConnManager();
    System.out.println(hcParams.getParameter(HttpConnectionParams.CONNECTION_TIMEOUT));
    DefaultHttpClient httpClient = new DefaultHttpClient(ccm, hcParams);
    return httpClient;
  }

  public void shutdown() {
    if (proxyClient != null) proxyClient.getConnectionManager().shutdown();
  }

  public String makeRequest(String url) {
    HttpGet request = new HttpGet(url);
    if (log.isDebugEnabled()) {
      log.debug("request:" + request);
    }
    HttpResponse response = null;
    boolean error = false;
    try {
      if (proxyClient == null) {
        System.out.println("===>proxyClient is null");
        if (log.isDebugEnabled()) {
          log.error("request:" + request);
        }
        return null;
      }
      response = proxyClient.execute(request);
      if (log.isDebugEnabled()) {
        log.debug("response:" + response);
      }

      // Process the response
      int statusCode = response.getStatusLine().getStatusCode();
      if (log.isDebugEnabled()) {
        log.debug("response:statusCode" + statusCode);
      }

      if (statusCode == HttpStatus.SC_OK) {
        StringBuilder buf = new StringBuilder();
        try {
          BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity()
              .getContent()));
          String line = "";
          while ((line = rd.readLine()) != null) {
            buf.append(line);
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
        String resp = buf.toString();
        return resp;
      }
    } catch (ClientProtocolException e) {
      error = true;
      e.printStackTrace();
    } catch (IOException e) {
      error = true;
      e.printStackTrace();
    } finally {
      if (response != null) {
        try {
          EntityUtils.consume(response.getEntity());
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      if (error) {
        request.releaseConnection();
      }
    }
    return null;
  }

  public static void main(String[] args) {
    /* LocationFetcher fetcher = new LocationFetcher(); LocationInfo li = fetcher.service("183.83.19.140", null);
     * System.out.println(li.city + "--" + li.regionName + "--" + li.countryName); */
   /* String json = "{location:%s}";

    System.out.println(String.format(json, "india"));*/
    
    HttpFetcher httpFetcher = new HttpFetcher();
    String url = "https://freegeoip.net/json/203.153.211.250";
    String json = httpFetcher.makeRequest(url);
    System.out.println(json);
  }
}
