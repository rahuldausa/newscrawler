package com.rujhaan.rocky.util;

import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.rujhaan.rocky.twitter.TwitterConfig;

import twitter4j.Logger;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterUtil {

  public static Logger logger = Logger.getLogger(TwitterUtil.class);

  public static Twitter getTwitterConnection(TwitterConfig config) {
    TrustManager trm = new BlindTrustManager();
    try {
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, new TrustManager[]{trm}, null);
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
      // System.setProperty("http.proxyHost", "localhost");
      // System.setProperty("http.proxyPort", "5865");

      ConfigurationBuilder cb = config.buildConfig();
      Twitter twitter = new TwitterFactory(cb.build()).getInstance();
      return twitter;
    } catch (Exception e) {
      logger.error("Error in creating twitter connection", e);
    }
    return null;
  }

  public static class BlindTrustManager implements X509TrustManager {
    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }

    public void checkClientTrusted(X509Certificate[] certs, String authType) {

    }

    public void checkServerTrusted(X509Certificate[] certs, String authType) {
    }
  }
}
