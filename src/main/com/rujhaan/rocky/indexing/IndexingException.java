package com.rujhaan.rocky.indexing;


public class IndexingException extends Exception {

  public IndexingException(Throwable e) {
    super(e);
  }

}
