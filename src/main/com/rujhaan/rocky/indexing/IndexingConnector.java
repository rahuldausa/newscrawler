package com.rujhaan.rocky.indexing;

import com.rujhaan.rocky.domain.Doc;

public interface IndexingConnector {

  public void index(Doc doc) throws IndexingException;

  public void commit() throws IndexingException;

}
