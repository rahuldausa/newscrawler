package com.rujhaan.rocky.indexing;


import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.BinaryResponseParser;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.params.UpdateParams;

import com.rujhaan.rocky.domain.Doc;

public class SolrConnector implements IndexingConnector {

  private static final Logger log = Logger.getLogger(SolrConnector.class);

  private SolrServer solrServer;

  public void open(String serverUrl) {
    createSolrServer(serverUrl);
  }

  public void createSolrServer(String serverUrl) {
    solrServer = new ConcurrentUpdateSolrServer(serverUrl, 100, 25);
  }

  public UpdateResponse addDocument(SolrInputDocument doc) throws SolrServerException, IOException {
    UpdateRequest req = new UpdateRequest();
    req.add(doc);
    req.setParam(UpdateParams.OVERWRITE, "true");
    req.setCommitWithin(-1);
    return req.process(solrServer);
  }

  public UpdateResponse addDocuments(List<SolrInputDocument> docs) throws SolrServerException,
      IOException {
    if (log.isDebugEnabled()) {
      log.debug("docs size;" + docs.size());
    }

    UpdateRequest req = new UpdateRequest();
    req.add(docs);
    req.setParam(UpdateParams.OVERWRITE, "true");
    req.setCommitWithin(-1);
    return req.process(solrServer);
  }

  public void commit() throws IndexingException {
    try {
      solrServer.commit();
    } catch (SolrServerException | IOException e) {
      e.printStackTrace();
      throw new IndexingException(e);
    }
  }

  public QueryResponse executeRequest(SolrParams params) {
    try {
      if (log.isDebugEnabled()) {
        log.debug(params.toNamedList());
      }
      QueryResponse resp = solrServer.query(params);
      if (log.isDebugEnabled()) {
        log.debug(resp.getResponseHeader() + "=>" + resp.getQTime());
      }
      return resp;
    } catch (SolrServerException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void stop() {
    solrServer.shutdown();
  }

  public void index(Doc inDoc) throws IndexingException {
    SolrInputDocument doc = new SolrInputDocument();
    Map<String, ? super Object> paramMap = inDoc.attributes();
    for (String param : paramMap.keySet()) {
      doc.addField(param, paramMap.get(param));
    }
    try {
      addDocument(doc);
    } catch (SolrServerException | IOException e) {
      throw new IndexingException(e);
    }
  }

  public void index(List<Doc> inDocs) throws IndexingException {
    List<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
    for (Doc inDoc : inDocs) {
      Map<String, ? super Object> paramMap = inDoc.attributes();

      SolrInputDocument doc = new SolrInputDocument();
      for (String param : paramMap.keySet()) {
        doc.addField(param, paramMap.get(param));
      }
      System.out.println("doc:" + doc);
      docs.add(doc);
    }
    try {
      addDocuments(docs);
    } catch (SolrServerException | IOException e) {
      throw new IndexingException(e);
    }
  }

  public HttpSolrServer createHttSolrServer(String serversURL) throws MalformedURLException {
    System.getProperty("http.keepAlive", "true");
    System.getProperty("http.maxConnections", "1500");

    ModifiableSolrParams params = new ModifiableSolrParams();
    params.set(HttpClientUtil.PROP_MAX_CONNECTIONS, 1500);
    params.set(HttpClientUtil.PROP_MAX_CONNECTIONS_PER_HOST, 1000);
    params.set(HttpClientUtil.PROP_FOLLOW_REDIRECTS, false);
    final HttpClient httpClient = HttpClientUtil.createClient(params);

    HttpClientUtil.setSoTimeout(httpClient, 60000);
    HttpClientUtil.setConnectionTimeout(httpClient, 6000);
    HttpClientUtil.setFollowRedirects(httpClient, false);
    if (httpClient instanceof DefaultHttpClient) {
      HttpClientUtil.setAllowCompression((DefaultHttpClient) httpClient, false);
    }

    final ClientConnectionManager connMgr = httpClient.getConnectionManager();
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    final Runnable cleanUp = new Runnable() {
      public void run() {

        connMgr.closeExpiredConnections();
        // Optionally, close connections that have been idle longer than 30 minutes
        connMgr.closeIdleConnections(15, TimeUnit.MINUTES);
      }
    };
    executor.scheduleAtFixedRate(cleanUp, 10, 5, TimeUnit.MINUTES);
    HttpSolrServer server = new HttpSolrServer(serversURL, httpClient, new BinaryResponseParser());
    return server;
  }

}
