package com.rujhaan.rocky.twitter;

import twitter4j.conf.ConfigurationBuilder;

public class TwitterConfig {

  boolean debugEnabled = false;

  String oAuthConsumerKey;
  String oAuthConsumerSecret;
  String oAuthAccessToken;
  String oAuthAccessTokenSecret;

  /**
   * Builds the configuration object.
   * 
   * @return
   */
  public ConfigurationBuilder buildConfig() {
    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb.setDebugEnabled(debugEnabled).setOAuthConsumerKey(oAuthConsumerKey).setOAuthConsumerSecret(oAuthConsumerSecret)
        .setOAuthAccessToken(oAuthAccessToken).setOAuthAccessTokenSecret(oAuthAccessTokenSecret);
    return cb;
  }

  public String getOAuthConsumerKey() {
    return oAuthConsumerKey;
  }

  public TwitterConfig setOAuthConsumerKey(String consumerKey) {
    this.oAuthConsumerKey = consumerKey;
    return this;

  }

  public String getOAuthConsumerSecret() {
    return oAuthConsumerSecret;
  }

  public TwitterConfig setOAuthConsumerSecret(String consumerSecret) {
    this.oAuthConsumerSecret = consumerSecret;
    return this;

  }

  public String getOAuthAccessToken() {
    return oAuthAccessToken;
  }

  public TwitterConfig setOAuthAccessToken(String accessToken) {
    this.oAuthAccessToken = accessToken;
    return this;

  }

  public String getOAuthAccessTokenSecret() {
    return oAuthAccessTokenSecret;
  }

  public TwitterConfig setOAuthAccessTokenSecret(String accessSecret) {
    this.oAuthAccessTokenSecret = accessSecret;
    return this;

  }

  public boolean isDebugEnabled() {
    return debugEnabled;
  }

  public TwitterConfig setDebugEnabled(boolean b) {
    this.debugEnabled = b;
    return this;
  }

}
