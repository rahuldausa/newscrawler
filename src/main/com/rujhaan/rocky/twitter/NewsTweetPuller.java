package com.rujhaan.rocky.twitter;

import com.rujhaan.news.impl.dao.MongoDao;

/**
 * Pull tweets from Databas after they are inserted by another worker guy fetched from Twitter
 * @author 
 *
 */
public class NewsTweetPuller extends AbstractTweetPuller implements TweetPuller {

  private MongoDao dao;

  public NewsTweetPuller() {
    String server = "127.0.0.1";
    // server = "63.142.240.102"; // TODO: to change

    this.dao = new MongoDao(server, "newstweet");
    // this.dao = new MongoDao("cctweetFeed");
  }

  public MongoDao getDatabaseService() {
    return dao;
  }

}
