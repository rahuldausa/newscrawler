package com.rujhaan.rocky.twitter;

import com.google.common.util.concurrent.RateLimiter;

public class RequestRateLimiter {

  final RateLimiter rateLimiter;

  public RequestRateLimiter(double permitsPerSecond) {
    rateLimiter = RateLimiter.create(permitsPerSecond);
  }

  public RequestRateLimiter() {
    this((400 / (60.0 * 15)));
  }

  public double limit() {
    rateLimiter.acquire();
    System.out.println(rateLimiter.getRate());
    return rateLimiter.getRate();
  }

}
