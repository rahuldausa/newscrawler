package com.rujhaan.rocky.twitter;

import com.rujhaan.news.impl.dao.MongoDao;

public class ContentTweetPuller extends AbstractTweetPuller {

  private MongoDao dao;

  public ContentTweetPuller() {
    String server = "127.0.0.1";
    // server = "63.142.240.102"; // TODO: to change

    // this.dao = new MongoDao(server, "newstweet");
    this.dao = new MongoDao("cctweetFeed");
  }

  @Override
  public MongoDao getDatabaseService() {
    return dao;
  }

}
