package com.rujhaan.rocky.twitter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.rujhaan.rocky.bootstrap.Component;
import com.rujhaan.rocky.bootstrap.RockyServer.ConfigProp;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class CelebsTwitterList implements Component {

  private static ConfigurationBuilder cb;
  private static Twitter twitter;

  private ScheduledExecutorService excutor;
  private TweetStatusHandler tweetStatusHandler;

  public CelebsTwitterList() {
    tweetStatusHandler = new TweetStatusHandler();
  }

  /**
   * static block used to construct a connection with tweeter with twitter4j
   * configuration with provided settings. This configuration builder will be
   * used for next search action to fetch the tweets from twitter.com.
   */
  @Override
  public void open(ConfigProp configProp) {
    // System.setProperty("http.proxyHost", "localhost");
    // System.setProperty("http.proxyPort", String.valueOf(5865));
    cb = new ConfigurationBuilder();
    cb.setDebugEnabled(false);
    cb.setOAuthConsumerKey("gFXimJ5hYCZ3swP54IRkkllY0");
    cb.setOAuthConsumerSecret("QLuhnotebXlsKnDx6frQJRUrbwvAUbjjW1e2u2SE0BIai1dCl3");
    cb.setOAuthAccessToken("1614546522-MEZYIEinvALpbQ3ALgKXWA2icR9tdAkoaAvCCpS");
    cb.setOAuthAccessTokenSecret("LCo7xcnHJ58WBeBwjgKEBFStQoy76rbW7XgORi2Lj9D5l");
    TwitterFactory tf = new TwitterFactory(cb.build());
    twitter = tf.getInstance();
    excutor = Executors.newScheduledThreadPool(1);
  }

  @Override
  public void start() {
    Runnable r = createFetchCelebsTweetsRunnable();
    excutor.scheduleAtFixedRate(r, 0, 15, TimeUnit.SECONDS);
  }

  public Runnable createFetchCelebsTweetsRunnable() {

    Runnable r = new Runnable() {

      @Override
      public void run() {

        System.out.println("Running Celebs tweets Fetcher...");
        long userId = getUserId(twitter);

        String indiaSlug = "celebs";
        String usSlug = "celebs-us";

        RequestRateLimiter rateLimiter = new RequestRateLimiter();
        System.out.println("Running Celebs tweets Fetcher...");

        UserListStatusTweetFetcher indiaCelebs = new UserListStatusTweetFetcher(indiaSlug, rateLimiter);
        UserListStatusTweetFetcher usCelebs = new UserListStatusTweetFetcher(usSlug, rateLimiter);

        int maxPages = 100;
        int maxPerPage = 100;

        indiaCelebs.fetchTweets(userId, maxPages, maxPerPage);
        usCelebs.fetchTweets(userId, maxPages, maxPerPage);
      }
    };
    return r;
  }

  private long getUserId(Twitter twitter) {
    long userId = 0L;
    try {
      userId = twitter.getId();
    } catch (IllegalStateException e1) {
      e1.printStackTrace();
    } catch (TwitterException e1) {
      e1.printStackTrace();
    }
    return userId;
  }

  public class UserListStatusTweetFetcher {

    private String slug;
    private RequestRateLimiter rateLimiter;

    public UserListStatusTweetFetcher(String slug, RequestRateLimiter rateLimiter) {
      this.slug = slug;
      this.rateLimiter = rateLimiter;
    }

    public void fetchTweets(long userId, int maxPages, int maxPerPage) {
      System.out.println("Fetching tweets for slug:"+slug);

      for (int i = 1; i < maxPages; i++) {
        Paging paging = new Paging(i, maxPerPage);
        ResponseList<Status> usersResponse = null;
        try {
          usersResponse = twitter.getUserListStatuses(userId, slug, paging);
        } catch (TwitterException e) {
          e.printStackTrace();
        }
        System.out.println("usersResponse:"+usersResponse);
        for (Status status : usersResponse) {
          System.out.println(status.getUser().getName() + "==>" + status.getText());
          tweetStatusHandler.handle(status, true, true);
        }
        rateLimiter.limit();
      }
    }
  }

  public static void main(String[] args) {
    CelebsTwitterList fetcher = new CelebsTwitterList();
    fetcher.open(null);
    fetcher.start();
    System.out.println("Done....");
    try {
      fetcher.excutor.awaitTermination(1, TimeUnit.HOURS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void stop() {
    if (excutor != null) {
      excutor.shutdown();
    }
  }

  @Override
  public void restart() {

  }
}
