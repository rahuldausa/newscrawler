package com.rujhaan.rocky.twitter;

public class DefaultTweetRateLimiter extends RequestRateLimiter {

  public DefaultTweetRateLimiter() {
    this(400 / (60.0 * 15));
  }

  public DefaultTweetRateLimiter(double permitsPerSecond) {
    super(permitsPerSecond);
  }

}
