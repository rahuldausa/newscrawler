package com.rujhaan.rocky.twitter.blog;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.io.Resources;
import com.rujhaan.news.util.FileUtil;
import com.rujhaan.rocky.crawl.PageDownloader;
import com.rujhaan.rocky.feed.CommonException;
import com.rujhaan.rocky.feed.CommonFeedParser;
import com.rujhaan.rocky.feed.FeedParser;
import com.rujhaan.rocky.feed.KeyValuePair;

import edu.uci.ics.crawler4j.crawler.Page;

public class BlogFeedReader {

  public void fetchBlogFeeds() {
    Logger.getRootLogger().setLevel(Level.ERROR);
    URL url = Resources.getResource(this.getClass(), "/resources/blogs1.txt");
    System.out.println(url.getPath());
    final String file = url.getFile();
    List<String> ls = null;
    try {
      ls = FileUtil.asList(new File(file));
    } catch (IOException e) {
      e.printStackTrace();
    }
    PageDownloader pageDownloader = new PageDownloader();

    for (String s : ls) {
      Page page = pageDownloader.processUrl(s);
      FeedParser parser = new CommonFeedParser();
      InputStream is = null;
      is = new ByteArrayInputStream(page.getContentData());
      try {
        List<KeyValuePair> kv = parser.parseFeed(is);
        for (KeyValuePair pair : kv) {
          System.out.println(pair.keySet());
          for (@SuppressWarnings("unused")
          String key : pair.keySet()) {
            System.out.println(key + "=" + pair.getAsString(key));
            if(key!=null && key.equals("url")){
              try {
                String redirectedUrl = PageDownloader.expandUrl(pair.getAsString(key));
                System.out.println("redirectedUrl:"+redirectedUrl);
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
        }
      } catch (CommonException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    BlogFeedReader blogFeedReader = new BlogFeedReader();
    blogFeedReader.fetchBlogFeeds();
  }
}
