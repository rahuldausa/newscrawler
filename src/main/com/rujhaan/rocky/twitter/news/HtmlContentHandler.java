package com.rujhaan.rocky.twitter.news;

import java.io.UnsupportedEncodingException;

import com.rujhaan.news.impl.dao.MongoDao;

public interface HtmlContentHandler {

  public void handleHtml(PageContext pageContext) throws UnsupportedEncodingException;

  public void setDBService(MongoDao dao);

  public MongoDao getDBService();

  public void setTextContentHandler(TextContentHandler textHandler);

  public TextContentHandler getTextContentHandler();

}
