package com.rujhaan.rocky.twitter.news;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.rocky.twitter.news.bootstrap.TwitterFeeder;

public class TweetURLProcessor extends AbstractURLProcessor {

  public static final Logger log = Logger.getLogger(TweetURLProcessor.class);

  public PageContext process(String link, TweetRecord tweet) throws UnsupportedEncodingException {
    PageContext pageContext = processURL(link, tweet);
    return pageContext;

    //summaizationHandler.summarizeAndAddToNewsFeed(crawlDatum);
  }
}
