package com.rujhaan.rocky.twitter.news;

import java.io.UnsupportedEncodingException;

import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.rocky.crawl.PageDownloader;
import com.rujhaan.rocky.twitter.news.bootstrap.TwitterFeeder;

public interface Processor {

  // Need refactoring as processor should not any twitter or facebook. he just
  // know to process it.
  PageContext process(String link, TweetRecord tweet) throws UnsupportedEncodingException;

  PageDownloader getPageDownloader();

  void setPageDownloader(PageDownloader pageDownloader);

}
