package com.rujhaan.rocky.twitter.news;

import edu.uci.ics.crawler4j.crawler.Page;

public class PageContext {

  String href;
  String link;
  Page page;
  // time Fetched
  long timestamp;

  public String getHref() {
    return href;
  }
  public void setHref(String href) {
    this.href = href;
  }
  public String getLink() {
    return link;
  }
  public void setLink(String link) {
    this.link = link;
  }

  public Page getPage() {
    return page;
  }
  public void setPage(Page page) {
    this.page = page;
  }
  public long getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

}
