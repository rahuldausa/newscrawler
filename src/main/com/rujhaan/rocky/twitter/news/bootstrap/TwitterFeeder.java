package com.rujhaan.rocky.twitter.news.bootstrap;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.rujhaan.news.impl.dao.MongoDao;
import com.rujhaan.rocky.bootstrap.Component;
import com.rujhaan.rocky.bootstrap.RockyServer.ConfigProp;
import com.rujhaan.rocky.crawl.PageDownloader;
import com.rujhaan.rocky.ml.textextraction.SummarizationHandler;
import com.rujhaan.rocky.twitter.NewsTweetPuller;
import com.rujhaan.rocky.twitter.news.TwitterFeedFetcher;

import edu.uci.ics.crawler4j.crawler.Page;

public class TwitterFeeder implements Component{

  private static final Logger log = Logger.getLogger(TwitterFeeder.class);

  private ScheduledExecutorService executorService;
  private PageDownloader pageDownloader;
  public MongoDao newsRawDataDao;
  private ConfigProp config;

  @Override
  public void open(ConfigProp configProp) {
    this.config = configProp;
    //BasicConfigurator.configure();
    executorService = Executors.newScheduledThreadPool(1);
    pageDownloader = new PageDownloader();
    
    String server = "127.0.0.1";
    //server = "63.142.240.102"; // TODO: to change
    
   // newsRawDataDao = new MongoDao(server, "newsrawdata");
    newsRawDataDao = new MongoDao(server, "newsrawdata");
    //newsRawDataDao = new MongoDao("ccrawdata");
  }

  public void start() {
    Logger.getRootLogger().setLevel(Level.INFO);
    NewsTweetPuller tweetFetcher = new NewsTweetPuller();
    SummarizationHandler summarizationHandler = new SummarizationHandler();
    TwitterFeedFetcher feedFetcher = new TwitterFeedFetcher(this.pageDownloader, tweetFetcher, this, summarizationHandler);
    executorService.scheduleAtFixedRate(feedFetcher, 0, 1, TimeUnit.MINUTES);
  }

  public void stop() {
    if (executorService != null) {
      executorService.shutdown();
      try {
        executorService.awaitTermination(1, TimeUnit.HOURS);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    TwitterFeeder feeder = new TwitterFeeder();
    feeder.open(null);
    feeder.start();
  }

  public void savePage(Page page, long timestamp) {
    BasicDBObject basicObj = new BasicDBObject();
    basicObj.put("url", page.getWebURL().getURL());
    String html = null;
    try {
      html = new String(page.getContentData(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    if(html==null || html.isEmpty()){
      System.out.println("no html found for :"+page.getWebURL().getURL());
      return;
    }
    basicObj.put("html", html);
    basicObj.put("ts", timestamp);
    newsRawDataDao.insert(basicObj);
  }

  public void restart() {

  }
}
