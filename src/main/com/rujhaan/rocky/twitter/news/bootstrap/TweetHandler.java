package com.rujhaan.rocky.twitter.news.bootstrap;

import twitter4j.Status;

import com.rujhaan.news.domain.TweetRecord;

public interface TweetHandler {

  public TweetRecord handleTweetMessage(Status status);

}
