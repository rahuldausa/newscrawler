package com.rujhaan.rocky.twitter.news.bootstrap;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;
import twitter4j.URLEntity;

import com.rujhaan.news.domain.TweetRecord;

public class StatusTweetHandler implements TweetHandler {

  @Override
  public TweetRecord handleTweetMessage(Status status) {
    TweetRecord tweet = new TweetRecord();
    tweet.id = status.getId();
    tweet.userName = status.getUser().getName();
    tweet.screenName = status.getUser().getScreenName();
    tweet.userFollowersCount = status.getUser().getFollowersCount();
    tweet.text = status.getText();
    if (status.getRetweetedStatus() != null) {
      tweet.retweetCount = status.getRetweetedStatus().getRetweetCount();
    }
    if (status.getMediaEntities().length > 0) {
      tweet.mediaURL = status.getMediaEntities()[0].getMediaURL();
    }
    if (status.getPlace() != null) {
      tweet.countryCode = status.getPlace().getCountryCode();
    }

    if (status.getUser() != null) {
      tweet.userProfileImageURL = status.getUser().getProfileImageURL();
      tweet.userMiniProfileImageURL = status.getUser().getMiniProfileImageURL();
      tweet.userURL = status.getUser().getURL();
    }
    if (status.getURLEntities() != null) {
      List<String> links = new ArrayList<>();
      for (URLEntity urlEntity : status.getURLEntities()) {
        links.add(urlEntity.getExpandedURL());
      }
      tweet.urlLinks = links;
    }
    if (status.getURLEntities() != null) {
      System.out.println(status.getUser().getName() + "--" + status.getText());
      for (URLEntity url : status.getURLEntities()) {
        System.out.println(url.getDisplayURL() + "--" + url.getExpandedURL() + "--" + url.getURL());
        /*
         * try { readibilityTest.fetchContent(url.getExpandedURL()); } catch
         * (Exception e) { e.printStackTrace(); }
         */
      }
    } else {
      System.out.println(status.getUser().getName() + "--" + status.getText());
    }
    return tweet;
  }
}
