package com.rujhaan.rocky.twitter.news.bootstrap;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.rujhaan.news.impl.dao.MongoDao;
import com.rujhaan.rocky.bootstrap.Component;
import com.rujhaan.rocky.bootstrap.RockyServer.ConfigProp;
import com.rujhaan.rocky.crawl.PageDownloader;
import com.rujhaan.rocky.indexing.SolrConnector;
import com.rujhaan.rocky.ml.textextraction.SummarizationHandler;
import com.rujhaan.rocky.twitter.NewsTweetPuller;
import com.rujhaan.rocky.twitter.news.CopyOfTwitterFeedFetcher;
import com.rujhaan.rocky.twitter.news.HtmlContentHandler;
import com.rujhaan.rocky.twitter.news.NewsHtmlContentHandler;
import com.rujhaan.rocky.twitter.news.NewsTextContentHandler;
import com.rujhaan.rocky.twitter.news.NewsURLProcessor;
import com.rujhaan.rocky.twitter.news.Processor;
import com.rujhaan.rocky.twitter.news.TextContentHandler;
import com.rujhaan.rocky.twitter.news.TweetURLProcessor;
import com.rujhaan.rocky.twitter.news.TwitterFeedFetcher;

import edu.uci.ics.crawler4j.crawler.Page;

public class CopyTwitterFeeder implements Component{

  private static final Logger log = Logger.getLogger(CopyTwitterFeeder.class);

  private ScheduledExecutorService executorService;
  private PageDownloader pageDownloader;
  public MongoDao newsRawDataDao;
  private ConfigProp config;

  @Override
  public void open(ConfigProp configProp) {
    this.config = configProp;
    //BasicConfigurator.configure();
    executorService = Executors.newScheduledThreadPool(1);
    pageDownloader = new PageDownloader();
    
    String server = "127.0.0.1";
    //server = "63.142.240.102"; // TODO: to change
    
   // newsRawDataDao = new MongoDao(server, "newsrawdata");
    newsRawDataDao = new MongoDao(server, "newsrawdata");
    //newsRawDataDao = new MongoDao("ccrawdata");
  }

  public void start() {
    Logger.getRootLogger().setLevel(Level.INFO);

    scheduleNewsFeedProcess();
    scheduleBlogFeedProcess();
  }
  
  public void scheduleNewsFeedProcess(){
    CopyOfTwitterFeedFetcher feedFetcher = buildNewsFeedFetcher();
    executorService.scheduleAtFixedRate(feedFetcher, 0, 1, TimeUnit.MINUTES);
  }
  
  public void scheduleBlogFeedProcess(){
    CopyOfTwitterFeedFetcher feedFetcher = buildBlogFeedFetcher();
    executorService.scheduleAtFixedRate(feedFetcher, 0, 1, TimeUnit.MINUTES);
  }
  
  public CopyOfTwitterFeedFetcher buildNewsFeedFetcher(){
    NewsTweetPuller tweetFetcher = new NewsTweetPuller();
    
    String server = "23.227.177.112";
    //server = "63.142.240.102"; // TODO: to change
    MongoDao newsfeeddao = new MongoDao(server, "newsfeed");
    
    SolrConnector connector = new SolrConnector();
    if (connector != null) {
      connector.open("http://" + server + ":8983/solr/c9news");
      //connector.open("http://localhost:8983/solr/c9news");
    }
    
    SummarizationHandler summarizationHandler = new SummarizationHandler(newsfeeddao, connector);
    Processor newsProcessor = new TweetURLProcessor();
    
    TextContentHandler newsTextContentHandler = new NewsTextContentHandler();
    newsTextContentHandler.setSummaizationHandler(summarizationHandler);

    HtmlContentHandler newsHtmlContentHandler = new NewsHtmlContentHandler();
    
    newsHtmlContentHandler.setTextContentHandler(newsTextContentHandler);
    
    CopyOfTwitterFeedFetcher feedFetcher = new CopyOfTwitterFeedFetcher(this.pageDownloader, tweetFetcher,
        newsProcessor, newsHtmlContentHandler);
    return feedFetcher;
        
  }
  
  public CopyOfTwitterFeedFetcher buildBlogFeedFetcher(){
    NewsTweetPuller tweetFetcher = new NewsTweetPuller();
    
    String server = "23.227.177.112";
    //server = "63.142.240.102"; // TODO: to change
    MongoDao newsfeeddao = new MongoDao(server, "ccfeed");
    
    SolrConnector connector = new SolrConnector();
    if (connector != null) {
      connector.open("http://" + server + ":8983/solr/c9news");
      //connector.open("http://localhost:8983/solr/c9news");
    }
    
    SummarizationHandler summarizationHandler = new SummarizationHandler(newsfeeddao, connector);
    Processor newsProcessor = new NewsURLProcessor();
    
    TextContentHandler newsTextContentHandler = new NewsTextContentHandler();
    newsTextContentHandler.setSummaizationHandler(summarizationHandler);
    
    HtmlContentHandler newsHtmlContentHandler = new NewsHtmlContentHandler();
    
    newsHtmlContentHandler.setTextContentHandler(newsTextContentHandler);
    
    CopyOfTwitterFeedFetcher feedFetcher = new CopyOfTwitterFeedFetcher(this.pageDownloader, tweetFetcher,
        newsProcessor, newsHtmlContentHandler);
    return feedFetcher;
        
  }

  @Override
  public void stop() {
    if (executorService != null) {
      executorService.shutdown();
      try {
        executorService.awaitTermination(1, TimeUnit.HOURS);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    CopyTwitterFeeder feeder = new CopyTwitterFeeder();
    feeder.open(null);
    feeder.start();
  }

  public void savePage(Page page, long timestamp) {
    BasicDBObject basicObj = new BasicDBObject();
    basicObj.put("url", page.getWebURL().getURL());
    String html = null;
    try {
      html = new String(page.getContentData(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    if(html==null || html.isEmpty()){
      System.out.println("no html found for :"+page.getWebURL().getURL());
      return;
    }
    basicObj.put("html", html);
    basicObj.put("ts", timestamp);
    newsRawDataDao.insert(basicObj);
  }

  public void restart() {

  }
}
