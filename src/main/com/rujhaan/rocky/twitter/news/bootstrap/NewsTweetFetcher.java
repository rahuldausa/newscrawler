package com.rujhaan.rocky.twitter.news.bootstrap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import twitter4j.Paging;
import twitter4j.RateLimitStatusEvent;
import twitter4j.RateLimitStatusListener;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.news.impl.dao.DBMapper;
import com.rujhaan.news.impl.dao.MongoDao;
import com.rujhaan.rocky.bootstrap.Component;
import com.rujhaan.rocky.bootstrap.RockyServer.ConfigProp;
import com.rujhaan.rocky.twitter.DefaultTweetRateLimiter;
import com.rujhaan.rocky.twitter.RequestRateLimiter;
import com.rujhaan.rocky.twitter.TwitterConfig;
import com.rujhaan.rocky.util.TwitterUtil;

public class NewsTweetFetcher implements Component {

  public static Logger logger = Logger.getLogger(NewsTweetFetcher.class);

  private Twitter twitter;
  private MongoDao mongoDao;
  private TweetHandler tweetHandler;
  private volatile boolean keepRunning;
  private boolean shouldLimit = true;

  public NewsTweetFetcher() {

  }

  private ScheduledExecutorService excutor = Executors.newScheduledThreadPool(1);

  public TweetHandler getTweetHandler() {
    return tweetHandler;
  }

  public void setTweetHandler(TweetHandler tweetHandler) {
    this.tweetHandler = tweetHandler;
  }

  public boolean isShouldLimit() {
    return shouldLimit;
  }

  public void setShouldLimit(boolean shouldLimit) {
    this.shouldLimit = shouldLimit;
  }

  @Override
  public void open(ConfigProp configProp) {
    try {
      Logger.getRootLogger().setLevel(Level.INFO);
      initializeTwitterConnection();
      initializeDatabase();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void start() {
    try {
      TweetHandler statusTweetHandler = new StatusTweetHandler();
      this.setTweetHandler(statusTweetHandler);
      RequestRateLimiter tweetRateLimiter = new DefaultTweetRateLimiter();
      ListTweetFetcher lsTweetFetcher = new ListTweetFetcher(this, tweetRateLimiter);
      keepRunning = true;
      excutor.scheduleAtFixedRate(lsTweetFetcher, 0, 10, TimeUnit.SECONDS);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void stop() {
    logger.info("Got a stop request, Stopping fetching...");
    keepRunning = false;
    if (excutor != null) {
      excutor.shutdown();
    }
  }

  @Override
  public void restart() {
    // No-op
  }

  private void initializeTwitterConnection() {
    TwitterConfig tc = new TwitterConfig();
    tc.setDebugEnabled(false).setOAuthConsumerKey("LWNBPJMWmCYEbM249z1JBLn7n")
        .setOAuthConsumerSecret("EmRUIOjyDbDrTa2cve1QYrMsaX1jsMRFDTkhNqCM9ip5GghsaU")
        .setOAuthAccessToken("1560501828-bkh9m55jVuJ7hqqVQWXosQNvSaVLPZCRHfpGcYB")
        .setOAuthAccessTokenSecret("4NlfQNfHOv18kSOIKSlzpofOTHcAWR0FTwjK6RpZoaeVI");

    twitter = TwitterUtil.getTwitterConnection(tc);
    twitter.addRateLimitStatusListener(new RateLimitStatusListener() {

      @Override
      public void onRateLimitStatus(RateLimitStatusEvent event) {
        logger.info("Rate limit status:" + event.toString());
      }

      @Override
      public void onRateLimitReached(RateLimitStatusEvent event) {
        logger.info("Rate limit status:" + event.toString());
      }
    });
  }

  private void initializeDatabase() {
    mongoDao = new MongoDao("127.0.0.1", "newstweet");
    // MongoDao mongoDao = new MongoDao("127.0.0.1", "cctweet");

    // final MongoDao mongoDao = new MongoDao("newstweet");
    BasicDBObject query = new BasicDBObject("tweet_ID", -1).append("unique", true).append("dropDups", true);
    mongoDao.addIndex(query);
  }

  private void addToDB(TweetRecord tweet) {
    DBObject dbObj = DBMapper.fromTweet(tweet);
    DBObject q = new BasicDBObject("tweet_ID", tweet.id);
    mongoDao.upsert(q, dbObj);
  }

  public class ListTweetFetcher implements Runnable {

    private NewsTweetFetcher newsFetcher;
    private RequestRateLimiter tweetRateLimiter;

    public ListTweetFetcher(NewsTweetFetcher newsFetcher, RequestRateLimiter tweetRateLimiter) {
      this.newsFetcher = newsFetcher;
      this.tweetRateLimiter = tweetRateLimiter;
    }

    @Override
    public void run() {
      logger.info("================News tweets fetcher is running=================");
      int maxPages = 10;
      int maxPerPage = 100;

      // curated_news==162485284
      // news==162481830
      // mylist==110988681

      for (int i = 1; i < maxPages; i++) {
        // If we have recived stop signal and this flag is set to false
        // then stop fetching.
        if (!keepRunning) {
          break;
        }
        Paging paging = new Paging(i, maxPerPage);
        ResponseList<Status> usersResponse = null;
        try {
          usersResponse = twitter.getUserListStatuses(162485284, paging);
        } catch (TwitterException e) {
          e.printStackTrace();
        }
        for (Status status : usersResponse) {
          TweetRecord tweet = newsFetcher.getTweetHandler().handleTweetMessage(status);
          try {
            newsFetcher.addToDB(tweet);
          } catch (Exception e) {
            logger.error(e.getMessage(), e);
          }
        }
        try {
          Thread.sleep(500L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        if (shouldLimit) {
          tweetRateLimiter.limit();
        }
      }
    }
  }

}
