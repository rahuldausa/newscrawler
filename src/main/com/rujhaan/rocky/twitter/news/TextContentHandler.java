package com.rujhaan.rocky.twitter.news;

import com.rujhaan.rocky.ml.textextraction.SummarizationHandler;

public interface TextContentHandler {

  public void setSummaizationHandler(SummarizationHandler summarizationHandler);
  
  public SummarizationHandler getSummarizationHandler();

  // SummarizationHandler summarizationHandler
}
