package com.rujhaan.rocky.twitter.news;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.rocky.crawl.PageDownloader;
import com.rujhaan.rocky.twitter.NewsTweetPuller;

public class CopyOfTwitterFeedFetcher implements Runnable {

  private static final Logger log = Logger.getLogger(CopyOfTwitterFeedFetcher.class);
  private PageDownloader pageDownloader;
  private NewsTweetPuller tweetFetcher;
  private Properties fetchTime;
  private HtmlContentHandler htmlContentHandler;
  private Processor processor;

  public CopyOfTwitterFeedFetcher(PageDownloader pageDownloader, NewsTweetPuller tweetFetcher, Processor processor, HtmlContentHandler htmlContentHandler) {
    this.pageDownloader = pageDownloader;
    this.tweetFetcher =tweetFetcher;
    this.htmlContentHandler = htmlContentHandler;
    this.processor = processor;
    processor.setPageDownloader(pageDownloader);
    
    fetchTime = new Properties();
    File f = new File("fetch_time");
    if (!f.exists()) {
      try {
        f.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    try {
      fetchTime.load(new FileReader(f));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void run() {
    log.info("Twitter Feed fetcher is running");
    try {
      long lastTimestamp = fetchLastFetchTime();
      long diff = System.currentTimeMillis() - lastTimestamp;
      
      // If the diff is more than 1 day old then resetting to before 1 day
      // as we wont be interested in crawling news those are older than 1 day.
      int maxBeforeHours = 4;
      if(diff > TimeUnit.HOURS.toMillis(maxBeforeHours)){
        long origlastTimestamp = lastTimestamp;
        lastTimestamp = System.currentTimeMillis() - TimeUnit.HOURS.toMillis(maxBeforeHours);
        System.out.println("Resetting the fetch time to :"+lastTimestamp +" from:"+origlastTimestamp);
      }
      
      long fetchedTillTimestamp = lastTimestamp;
      if (log.isDebugEnabled()) {
        log.debug("firing query for getting tweets from : " + lastTimestamp);
      }
      Iterator<TweetRecord> tweetsItr = tweetFetcher.fetchTweets(lastTimestamp);
      int i = 0;
      while (tweetsItr.hasNext()) {
        TweetRecord tweet = tweetsItr.next();
        if (tweet == null) {
          log.error("Tweet is null");
          continue;
        }
        if (tweet.getUrlLinks() == null || tweet.getUrlLinks().size() == 0) {
          log.error("Tweet href is null");
          continue;
        }
        for (String link : tweet.getUrlLinks()) {
          PageContext pageContext = processor.process(link, tweet);
          htmlContentHandler.handleHtml(pageContext);
        }

        fetchedTillTimestamp = tweet.getTimestamp();
        if (fetchedTillTimestamp == 0) {
          fetchedTillTimestamp = lastTimestamp;
        }
        
        i++;
        // save the fetch time stamp in every 10 iterations
        if (i % 10 == 0) {
          if (fetchedTillTimestamp > lastTimestamp) {
            if (log.isDebugEnabled()) {
              log.debug("Going to update:" + fetchedTillTimestamp + ", Last update was on: "
                  + lastTimestamp);
            }
            sync(fetchedTillTimestamp);
          }
        }
      }
      // If jobs run second time and it didnt find any tweet, it update the file with zero
      // => this means we didnt get any tweet from the lastTimeStamp, so we dont need to update
      if (fetchedTillTimestamp > lastTimestamp) {
        if (log.isDebugEnabled()) {
          log.debug("Going to update:" + fetchedTillTimestamp + ", Last update was on: "
              + lastTimestamp);
        }
        sync(fetchedTillTimestamp);
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      e.printStackTrace();
    }
  }
  
//  public void processURL(TwitterFeeder feeder, String link, TweetRecord tweet) throws UnsupportedEncodingException{
//    String href = null;
//    try {
//      href = URLNormalizer.normalize(link);
//    } catch (Exception e) {
//      log.error(e);
//      href = link;
//    }
//    
//    if(pageDownloader.isBlackListed(href)){
//      return;
//    }
//    
//    // Fetching the page
//    Page page =null;
//    try {
//      page = pageDownloader.processUrl(href);
//    } catch (Exception e) {
//      e.printStackTrace();
//      return;
//    }
//
//    if (page == null) {
//      System.out.println("page is null");
//      return;
//    }
//    
//    if (feeder != null) {
//      feeder.savePage(page, tweet.getTimestamp());
//    }
//    
//    // CrawlDatum builder activity
//    String url = page.getWebURL().getURL();
//    String id = null;
//    try {
//      id = Utils.uniqueId(url);
//    } catch (CommonException e1) {
//      e1.printStackTrace();
//    }
//    CrawlDatum crawlDatum = new CrawlDatum();
//
//    crawlDatum.id = id;
//    crawlDatum.url = url;
//    crawlDatum.data = new String(page.getContentData(), "UTF-8");
//    crawlDatum.crawledAt = tweet.getTimestamp();
//
//    summaizationHandler.summarizeAndAddToNewsFeed(crawlDatum);
//  }

  public void sync(long lastFetchTime) {
    fetchTime.put("crawler.tweet.last.fetchtime", String.valueOf(lastFetchTime));
    try {
      fetchTime.store(new FileWriter(new File("fetch_time")), "");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public long fetchLastFetchTime() {
    Object val = fetchTime.get("crawler.tweet.last.fetchtime");
    if (val == null) {
      return System.currentTimeMillis() - (2 * 60 * (60 * 1000));// before 30 mins.
    }
    long time = 0L;
    if (val instanceof Long) {
      time = (Long) val;
    } else {
      time = Long.parseLong(String.valueOf(val));
    }
    return time;
  }
}
