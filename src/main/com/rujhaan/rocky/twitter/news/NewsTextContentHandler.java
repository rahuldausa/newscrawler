package com.rujhaan.rocky.twitter.news;

import com.rujhaan.rocky.ml.textextraction.SummarizationHandler;

public class NewsTextContentHandler implements TextContentHandler {

  private SummarizationHandler summarizationHandler;

  @Override
  public void setSummaizationHandler(SummarizationHandler summarizationHandler) {
    this.summarizationHandler = summarizationHandler;
  }

  @Override
  public SummarizationHandler getSummarizationHandler() {
    return summarizationHandler;
  }

}
