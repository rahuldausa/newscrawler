package com.rujhaan.rocky.twitter.news;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

import com.rujhaan.news.domain.TweetRecord;
import com.rujhaan.rocky.crawl.PageDownloader;
import com.rujhaan.rocky.util.v1.URLNormalizer;

import edu.uci.ics.crawler4j.crawler.Page;

public abstract class AbstractURLProcessor implements Processor {

  public static final Logger log = Logger.getLogger(AbstractURLProcessor.class);

  public PageDownloader pageDownloader;

  @Override
  public PageDownloader getPageDownloader() {
    return pageDownloader;
  }

  @Override
  public void setPageDownloader(PageDownloader pageDownloader) {
    this.pageDownloader = pageDownloader;
  }

  // public void preProcess(ProcessContext context) {
  // String href = context.getHref();
  // String link = context.getLink();
  // try {
  // href = URLNormalizer.normalize(link);
  // } catch (Exception e) {
  // log.error(e);
  // href = link;
  // }
  // context.setHref(href);
  // }
  //
  // public void postProcess() {
  //
  // }
  //
  // public void process(ProcessContext context) {
  // // Fetching the page
  // String href = context.getHref();
  // Page page = null;
  // try {
  // page = pageDownloader.processUrl(href);
  // } catch (Exception e) {
  // e.printStackTrace();
  // }
  // context.setPage(page);
  // }

  public PageContext processURL(String link, TweetRecord tweet)
      throws UnsupportedEncodingException {
    String href = null;
    try {
      href = URLNormalizer.normalize(link);
    } catch (Exception e) {
      log.error(e);
      href = link;
    }

    if (pageDownloader.isBlackListed(href)) {
      return null;
    }

    // Fetching the page
    Page page = null;
    try {
      page = pageDownloader.processUrl(href);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

    if (page == null) {
      System.out.println("page is null");
      return null;
    }
    
    PageContext pageContext = new PageContext();
    pageContext.href = href;
    pageContext.link = link;
    pageContext.page  = page;
    pageContext.timestamp = tweet.getTimestamp();
    return pageContext;

//    if (feeder != null) {
//      feeder.savePage(page, tweet.getTimestamp());
//    }
//
//    // CrawlDatum builder activity
//    String url = page.getWebURL().getURL();
//    String id = null;
//    try {
//      id = Utils.uniqueId(url);
//    } catch (CommonException e1) {
//      e1.printStackTrace();
//    }
//    CrawlDatum crawlDatum = new CrawlDatum();
//
//    crawlDatum.id = id;
//    crawlDatum.url = url;
//    crawlDatum.data = new String(page.getContentData(), "UTF-8");
//    crawlDatum.crawledAt = tweet.getTimestamp();
//
//    return crawlDatum;

    // summaizationHandler.summarizeAndAddToNewsFeed(crawlDatum);
  }
}
