package com.rujhaan.rocky.twitter.news;

import java.io.UnsupportedEncodingException;

import com.mongodb.BasicDBObject;
import com.rujhaan.news.impl.dao.MongoDao;
import com.rujhaan.rocky.crawl.CrawlDatum;
import com.rujhaan.rocky.feed.CommonException;
import com.rujhaan.rocky.util.Utils;

import edu.uci.ics.crawler4j.crawler.Page;

public abstract class AbstractHtmlContentHandler implements HtmlContentHandler {

  private MongoDao dbService;
  private TextContentHandler textContentHandler;

  public void setDBService(MongoDao dao) {
    this.dbService = dao;
  }

  public MongoDao getDBService() {
    return this.dbService;
  }

  public CrawlDatum buildCrawlDatum(PageContext pageContext) throws UnsupportedEncodingException {
    // if (feeder != null) {
    // feeder.savePage(page, tweet.getTimestamp());
    // }

    Page page = pageContext.getPage();

    // CrawlDatum builder activity
    String url = page.getWebURL().getURL();
    String id = null;
    try {
      id = Utils.uniqueId(url);
    } catch (CommonException e1) {
      e1.printStackTrace();
    }
    CrawlDatum crawlDatum = new CrawlDatum();

    crawlDatum.id = id;
    crawlDatum.url = url;
    crawlDatum.data = new String(page.getContentData(), "UTF-8");
    crawlDatum.crawledAt = pageContext.getTimestamp();

    return crawlDatum;
  }

  protected void savePage(PageContext pageContext) {
    Page page = pageContext.getPage();
    long timestamp = pageContext.getTimestamp();
    BasicDBObject basicObj = new BasicDBObject();
    basicObj.put("url", page.getWebURL().getURL());
    String html = null;
    try {
      html = new String(page.getContentData(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    if (html == null || html.isEmpty()) {
      System.out.println("no html found for :" + page.getWebURL().getURL());
      return;
    }
    basicObj.put("html", html);
    basicObj.put("ts", timestamp);
    getDBService().insert(basicObj);
  }

  @Override
  public void setTextContentHandler(TextContentHandler textHandler) {
    this.textContentHandler = textHandler;
  }

  @Override
  public TextContentHandler getTextContentHandler() {
    return textContentHandler;
  }

}
