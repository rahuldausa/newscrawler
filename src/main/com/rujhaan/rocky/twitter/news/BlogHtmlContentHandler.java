package com.rujhaan.rocky.twitter.news;

import java.io.UnsupportedEncodingException;

import com.rujhaan.rocky.crawl.CrawlDatum;
import com.rujhaan.rocky.ml.textextraction.SummarizationHandler;

/**
 * Fetched from blog pages may be using rss feed.
 * 
 * @author
 *
 */
public class BlogHtmlContentHandler extends AbstractHtmlContentHandler implements HtmlContentHandler {

  @Override
  public void handleHtml(PageContext pageContext) throws UnsupportedEncodingException {

    // save the page raw data into db.
    savePage(pageContext);
    
    CrawlDatum crawlDatum = buildCrawlDatum(pageContext);
    TextContentHandler contextHandler = this.getTextContentHandler();
    SummarizationHandler summarizationHandler = contextHandler.getSummarizationHandler();
    summarizationHandler.summarizeAndAddToNewsFeed(crawlDatum);
  }


}
