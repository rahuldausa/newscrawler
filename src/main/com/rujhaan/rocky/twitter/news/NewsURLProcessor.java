package com.rujhaan.rocky.twitter.news;

import java.io.UnsupportedEncodingException;

import com.rujhaan.news.domain.TweetRecord;

public class NewsURLProcessor extends AbstractURLProcessor implements Processor {

  @Override
  public PageContext process(String link, TweetRecord tweet) throws UnsupportedEncodingException {
    PageContext pageContext = processURL(link, tweet);
    return pageContext;
  }

}
