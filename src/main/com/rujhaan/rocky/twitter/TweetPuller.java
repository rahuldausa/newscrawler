package com.rujhaan.rocky.twitter;

import com.rujhaan.news.impl.dao.MongoDao;

/**
 * 
 * Terminology:
 * 
 * <pre>
 * Fetcher : Fetches from twitter -- 
 *              TweetFetcher: fetches tweets, 
 *              RssFeedFetcher: fetches rss feed
 *              Puller: pulls from database
 *              Processor :process those tweets and fetchs html content of those urls
 * @author
 *
 */
public interface TweetPuller {

  public MongoDao getDatabaseService();
}
