package com.rujhaan.rocky.twitter.trends;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.internal.StringMap;
import com.rujhaan.news.impl.TrendsImpl;
import com.rujhaan.rocky.domain.TrendsInfo;
import com.rujhaan.rocky.twitter.TweetStatusHandler;
import com.rujhaan.rocky.twitter.TwitterTask;
import com.rujhaan.rocky.util.HttpFetcher;

//1=United States
//10=Hong Kong
//12=Taiwan
//13=Canada
//14=Russia
//15=Germany
//16=France
//17=Netherlands
//18=Brazil
//19=Indonesia
//21=Mexico
//23=South Korea
//24=Turkey
//25=Philippines
//26=Spain
//27=Italy
//28=Vietnam
//29=Egypt
//3=India
//30=Argentina
//31=Poland
//32=Colombia
//33=Thailand
//34=Malaysia
//35=Ukraine
//36=Saudi Arabia
//37=Kenya
//38=Chile
//39=Romania
//4=Japan
//40=South Africa
//41=Belgium
//42=Sweden
//43=Czech Republic
//44=Austria
//45=Hungary
//46=Switzerland
//47=Portugal
//48=Greece
//49=Denmark
//5=Singapore
//50=Finland
//51=Norway
//52=Nigeria
//6=Israel
//8=Australia
//9=United Kingdom
public class TrendsTweetFetcher {

  private HttpFetcher httpFetcher;
  private TwitterTask twitterTask;
  TweetStatusHandler tweetStatusHandler;
  private static final Gson gson = new Gson();

  public TrendsTweetFetcher() {
    httpFetcher = new HttpFetcher();
    twitterTask = new TwitterTask();
    tweetStatusHandler = new TweetStatusHandler();
  }

  // http://hawttrends.appspot.com/api/terms/

  public static final String G_TRENDS_URL = "http://hawttrends.appspot.com/api/terms/";

  public void fetchTweets(Map<Integer, Set<String>> map, int cId) {
    Set<String> topics = map.get(cId);
    twitterTask.getTweetByQuery(tweetStatusHandler, topics);
  }

  private Map<Integer, Set<String>> fetchTrends() {
    String json = httpFetcher.makeRequest(G_TRENDS_URL);

    Map<Integer, Set<String>> trendsMap = new HashMap<>();
    if (json != null) {
      // com.google.gson.internal.LinkedTreeMap<Integer, Object> trends =
      // (LinkedTreeMap<Integer, Object>) gson.fromJson(json,Object.class);

      StringMap trends = (StringMap) gson.fromJson(json, Object.class);
      for (Object cId : trends.keySet()) {
        System.out.println("cId:" + cId);

        // only consider trends for currencly enabled countries
        if (!TrendsImpl.enabledCountryCodeVsTrendsCode.containsValue(Integer.parseInt(String.valueOf(cId)))) {
          continue;
        }
        System.out.println("--cId:" + cId);

        TrendsInfo trendInfo = new TrendsInfo();
        trendInfo.setcIid(Integer.parseInt(String.valueOf(cId)));
        trendInfo.setTrends((List<String>) trends.get(cId));
        Set<String> trendsSet = new HashSet<String>();
        trendsSet.addAll(trendInfo.getTrends());
        trendsMap.put(trendInfo.getcIid(), trendsSet);
      }
      return trendsMap;
    }
    return null;
  }

  public static void main(String[] args) {
    TrendsTweetFetcher fetcher = new TrendsTweetFetcher();
    Map<Integer, Set<String>> map = fetcher.fetchTrends();
    fetcher.fetchTweets(map, 1);
    fetcher.fetchTweets(map, 3);
  }
}
