package com.rujhaan.rocky.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

  public static final String DB_SERVER_PORT = "db.url";// locahost:3306
  public static final String DB_NAME = "db.name";
  public static final String USERNAME = "db.username";
  public static final String PASSWORD = "db.password";

  public static final String RUN_MODE = "run_mode";// test,prod

  public static final String SOLR_INSTANCES = "solr.servers";// localhost:8983/solr

  public static final String UPDATE_REQUEST_OVERWRITE = "solr.overwrite";// true, false

  public static final String FLUSH_COUNT_DISPLAY = "importer.flushCount";
  public static final String MAX_RECORDS_LIMIT = "importer.maxLimit";

  public static final String MAX_NUM_TASKS_ALLOWED = "importer.maxNumTaskAllowed";
  public static final String MAX_INDEXING_THREADS = "importer.maxIndexingThreads";
  public static final String MIN_INDEXING_THREADS = "importer.minIndexingThreads";

  private static Config config = new Config();

  private Properties props;

  private Config() {
    // String configPath = System.getProperty("configPath", "/conf/crawl.config");
    String configPath = System.getProperty("configPath", "crawl.config");
    props = new Properties();
    try {
      props.load(new FileReader(new File(configPath)));
      System.out.println("Config file loaded successfully:" + props);
    } catch (FileNotFoundException e) {
      throw new RuntimeException(
          "Unable to start application, Couldnt find the importer.config at " + configPath, e);
    } catch (IOException e) {
      throw new RuntimeException(
          "Unable to start application, Couldnt read the importer.config at " + configPath, e);
    }
  }

  public static Config getInstance() {
    return config;
  }

  public Object getProperty(String proppertyName) {
    return props.getProperty(proppertyName);
  }

  public String getStringProperty(String proppertyName) {
    return String.valueOf(getProperty(proppertyName));
  }

  public String get(String proppertyName) {
    String val = getStringProperty(proppertyName);
    return val;
  }

  public String get(String proppertyName, String defaultValue) {
    String val = getStringProperty(proppertyName);
    if (val == null) {
      return defaultValue;
    }
    return val;
  }

  public int getIntProperty(String proppertyName) {
    return Integer.parseInt(props.getProperty(proppertyName));
  }

  public String getDBServerAndPort() {
    return getStringProperty(DB_SERVER_PORT);
  }

  public String getDBName() {
    return getStringProperty(DB_NAME);
  }

  public String getDBUserName() {
    return getStringProperty(USERNAME);
  }

  public String getDBPassword() {
    return getStringProperty(PASSWORD);
  }

  public String getRunMode() {
    return getStringProperty(RUN_MODE);
  }

  public String getSolrServers() {
    return getStringProperty(SOLR_INSTANCES);
  }

  public String shouldOverwrite() {
    return getStringProperty(UPDATE_REQUEST_OVERWRITE);
  }

  public InputStream getConfResourceAsInputStream(Object object) {
    try {
      return this.getClass().getResourceAsStream((String) object);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean getBoolean(String propertyName, boolean b) {
    String val = (String) getProperty(propertyName);
    if (val == null) {
      return b;
    }
    return Boolean.parseBoolean(val);
  }

  public int getInt(String propertyName, int i) {
    String val = (String) getProperty(propertyName);
    if (val == null) {
      return i;
    }
    return Integer.parseInt(val);
  }

}
