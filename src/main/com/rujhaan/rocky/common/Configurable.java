package com.rujhaan.rocky.common;

public interface Configurable {

  public void setConf(Config conf);

  public Config getConf();
}
