package com.rujhaan.rocky.common;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class KeyValuePair implements Serializable, Iterable<String> {

  private static final long serialVersionUID = 1L;

  protected Map<String, ? super Object> params;

  public KeyValuePair() {
    this.params = new HashMap<String, Object>();
  }

  public String getAsString(String key) {
    Object val = get(key);
    if (val != null) {
      return String.valueOf(val);
    }
    return null;
  }

  public Object get(String key) {
    return params.get(key);
  }

  public void add(String key, Object val) {
    params.put(key, val);
  }

  public Map<String, ? super Object> attributes() {
    return Collections.unmodifiableMap(params);
  }

  @Override
  public Iterator<String> iterator() {
    return attributes().keySet().iterator();
  }

  public Set<String> keySet() {
    return params.keySet();
  }

  public Collection<? super Object> valueSet() {
    return params.values();
  }

  @Override
  public String toString() {
    StringBuilder debug = new StringBuilder();
    for (String key : params.keySet()) {
      debug.append(key + "=" + params.get(key) + "\n");
    }
    return debug.toString();
  }
}
