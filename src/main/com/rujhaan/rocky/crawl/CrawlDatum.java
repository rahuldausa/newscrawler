package com.rujhaan.rocky.crawl;

import java.io.Serializable;
import java.util.Date;

public class CrawlDatum implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  public String id;
  public String url;
  public String data = "";
  public String dataPath = "";
  public Long crawledAt;
  public String domain;
  public String subdomain;
  public String fulldomain;
  public int expired;

  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public String getUrl() {
    return url;
  }
  public void setUrl(String url) {
    this.url = url;
  }
  public String getData() {
    return data;
  }
  public void setData(String data) {
    this.data = data;
  }
  public String getDataPath() {
    return dataPath;
  }
  public void setDataPath(String dataPath) {
    this.dataPath = dataPath;
  }
  public Long getCrawledAt() {
    return crawledAt;
  }
  public void setCrawledAt(Long crawledAt) {
    this.crawledAt = crawledAt;
  }
  public String getDomain() {
    return domain;
  }
  public void setDomain(String domain) {
    this.domain = domain;
  }
  public String getSubdomain() {
    return subdomain;
  }
  public void setSubdomain(String subdomain) {
    this.subdomain = subdomain;
  }
  public String getFulldomain() {
    return fulldomain;
  }
  public void setFulldomain(String fulldomain) {
    this.fulldomain = fulldomain;
  }
  public int getExpired() {
    return expired;
  }
  public void setExpired(int expired) {
    this.expired = expired;
  }

}
