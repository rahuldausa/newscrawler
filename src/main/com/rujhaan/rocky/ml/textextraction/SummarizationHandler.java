package com.rujhaan.rocky.ml.textextraction;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import twitter4j.Status;

import com.camus.util.readability.ImageExtractor;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rujhaan.news.domain.NewsFeed;
import com.rujhaan.news.impl.dao.DBMapper;
import com.rujhaan.news.impl.dao.MongoDao;
import com.rujhaan.rocky.crawl.CrawlDatum;
import com.rujhaan.rocky.domain.ArticleSummary;
import com.rujhaan.rocky.facebook.Publisher;
import com.rujhaan.rocky.indexing.IndexingException;
import com.rujhaan.rocky.indexing.SolrConnector;
import com.rujhaan.rocky.indexing.SolrDocMapper;
import com.rujhaan.rocky.ml.classification.ILocationClassifier;
import com.rujhaan.rocky.ml.classification.INewsClassifier;
import com.rujhaan.rocky.ml.classification.LocationClassifier;
import com.rujhaan.rocky.ml.classification.NewsClassifierV2;
import com.rujhaan.rocky.ml.langdetect.LanguageDetector;
import com.rujhaan.rocky.ml.scoring.Scorer;
import com.rujhaan.rocky.ml.scoring.Scorer.ImageInfo;
import com.rujhaan.rocky.twitter.TweetStatusHandler;
import com.rujhaan.rocky.twitter.TwitterTask;
import com.rujhaan.rocky.util.v1.FileUtil;
import com.sree.textbytes.jtopia.NewsTermsExtractor;
import com.sree.textbytes.readabilityBUNDLE.Article;
import com.sree.textbytes.readabilityBUNDLE.image.BestImageGuesser;

public class SummarizationHandler {

  private NewsTermsExtractor newsTermExtractor;
  private SolrConnector connector;
  // private MongoDao newsRawDataDao;
  private MongoDao newsfeeddao;
  private INewsClassifier newsClassifier;
  private ISummarization summarization;
  private Set<String> filterSentences;
  private ILocationClassifier locationClassifier;
  PublisherThread publisherThread;
  private NERExtractor nerExtractor;
  private LanguageDetector langDetector;
  private ImageExtractor imageExtractor;
  
  private TweetStatusHandler tweetStatusHandler = new TweetStatusHandler();
  private TwitterTask twitterSearch = new TwitterTask();
  boolean tweetsRequied = false;

  public SummarizationHandler() {
    publisherThread = new PublisherThread();
    new Thread(publisherThread).start();
    
    PublisherThreadQueueSizePrinter queueSizePrinter = new PublisherThreadQueueSizePrinter();
    new Thread(queueSizePrinter).start();
    
    summarization = new GooseSummarization();
    langDetector = new LanguageDetector();
    newsClassifier = new NewsClassifierV2();
    if (newsClassifier != null) {
      try {
        newsClassifier.trainAndLoadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }
    locationClassifier= new LocationClassifier();
    if (locationClassifier != null) {
      try {
        locationClassifier.trainAndLoadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }
    
    nerExtractor = new NERExtractor();
    
    String server = "23.227.177.112";
    //server = "63.142.240.102"; // TODO: to change

    connector = new SolrConnector();
    if (connector != null) {
      connector.open("http://" + server + ":8983/solr/c9news");
      //connector.open("http://localhost:8983/solr/c9news");
    }
    newsTermExtractor = new NewsTermsExtractor();
    imageExtractor = new ImageExtractor();

    newsfeeddao = new MongoDao(server, "newsfeed");
    //newsfeeddao = new MongoDao("127.0.0.1", "ccfeed");
    //newsfeeddao = new MongoDao("127.0.0.1", "newsfeed");
    
    // Index creation
    newsfeeddao.addIndex(new BasicDBObject("category_label", -1));//descending
    BasicDBObject query = new BasicDBObject("id", -1).append("unique", true);
    newsfeeddao.addIndex(query);
    
    filterSentences = new HashSet<String>();
    
    List<String> ls = FileUtil.loadResource(SummarizationHandler.class, "resources/filter_sentences.txt", true);
    for (String s : ls) {
      s = s.trim();
      if (s.length() > 0) {
        if(s.startsWith("#")){
          continue;//skip that line
        }
        filterSentences.add(s);
      }
    }
    //filterSentences.addAll(ls);
  }

  /*public SummarizationHandler() {
    String server = "23.227.177.112";
    //server = "63.142.240.102"; // TODO: to change
    newsfeeddao = new MongoDao(server, "newsfeed");
    
    connector = new SolrConnector();
    if (connector != null) {
      connector.open("http://" + server + ":8983/solr/c9news");
      //connector.open("http://localhost:8983/solr/c9news");
    }
  }*/
  
  public SummarizationHandler(MongoDao db, SolrConnector indexer) {
    this.newsfeeddao = db;
    this.connector = indexer;
    
    publisherThread = new PublisherThread();
    new Thread(publisherThread).start();
    
    PublisherThreadQueueSizePrinter queueSizePrinter = new PublisherThreadQueueSizePrinter();
    new Thread(queueSizePrinter).start();
    
    summarization = new GooseSummarization();
    langDetector = new LanguageDetector();
    newsClassifier = new NewsClassifierV2();
    if (newsClassifier != null) {
      try {
        newsClassifier.trainAndLoadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }
    locationClassifier= new LocationClassifier();
    if (locationClassifier != null) {
      try {
        locationClassifier.trainAndLoadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }
    
    nerExtractor = new NERExtractor();
    
    newsTermExtractor = new NewsTermsExtractor();
    imageExtractor = new ImageExtractor();

    // Index creation
    db.addIndex(new BasicDBObject("category_label", -1));//descending
    BasicDBObject query = new BasicDBObject("id", -1).append("unique", true);
    db.addIndex(query);
    
    filterSentences = new HashSet<String>();
    
    List<String> ls = FileUtil.loadResource(SummarizationHandler.class, "resources/filter_sentences.txt", true);
    for (String s : ls) {
      s = s.trim();
      if (s.length() > 0) {
        if(s.startsWith("#")){
          continue;//skip that line
        }
        filterSentences.add(s);
      }
    }
  }
  
  // BHEL, HSL and Midhani join hands for submarine project : India, News
  public String normalizeTiltle(String title) {
    String newTitle = title;
    if (title.contains(" : India, News")) {
      int index = title.indexOf(" : India, News");
      newTitle = title.substring(0, index);
    }
    return newTitle;
  }
  
  public void summarizeAndAddToNewsFeed(CrawlDatum crawlDatum) {

    String url = crawlDatum.getUrl();
    String html = crawlDatum.getData();
    Long timestamp = crawlDatum.getCrawledAt();
    String id = crawlDatum.getId();

    DBObject q = new BasicDBObject("id", id);

    if (newsfeeddao.exists(q)) {
      return;
    }

    NewsFeed newsFeed = buildNewsFeed(id, url, html, timestamp);

//    if (newsFeed != null) {
//      newsFeed.title = normalizeTiltle(newsFeed.title);
//    }

    if (newsFeed != null) {
      addToDB(newsFeed);
      index(newsFeed);
      
//      List<Status> tweets = twitterSearch.getTweetTextsWithOR(newsFeed.getKeywords());
//      searchAndAddTweets(tweets);
//      try {
//        Thread.sleep(1000L);
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//      tweets = twitterSearch.getTweetImageWithOR(newsFeed.getKeywords());
//      searchAndAddTweets(tweets);
      
      if (tweetsRequied) {
        for (String t : newsFeed.getKeywords()) {
          List<Status> tweets = twitterSearch.getTweetTexts(t);
          searchAndAddTweets(tweets);
          try {
            Thread.sleep(1000L);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          tweets = twitterSearch.getTweetImagesQuery(t);
          searchAndAddTweets(tweets);
        }
      }

/*      if (newsFeed.getImagePath() != null) {
        publishToSocialChannels(newsFeed);
      }*/
    } else {
      System.out.println("Ignoring as newsFeed was empty");
    }
  }
  
  public String getBestImage(Article article) {
    String bestImageSrc = null;

    if (article.getTopImage() != null) {
      System.out.println("top image : " + article.getTopImage().getImageSrc());
      bestImageSrc = article.getTopImage().getImageSrc();
    }

    // best Image return by readabiliRty is more reliable than our stuff
    if (bestImageSrc == null) {
      String metaImageUrl = null;
      Document doc = article.getCleanedDocument();
      Elements metaOgImage = doc.select("meta[property=og:image]");
      if (metaOgImage != null) {
        String imgUrl = metaOgImage.attr("content");
        BestImageGuesser.knownJunkImageMatcher.reset(imgUrl);
        if (BestImageGuesser.knownJunkImageMatcher.find()) {
          System.out.println("Known Junk image found in top node , skipping " + imgUrl);
        } else {
          // it means image is proper and not junk.
          System.out.println("Img source: " + metaImageUrl);
          metaImageUrl = imgUrl;
        }
      }

      Element e = article.getTopNode();
      if (e != null) {
        Elements elems = e.select("img");
        for (Element element : elems) {
          String imgSrc = element.absUrl("src");
          BestImageGuesser.knownJunkImageMatcher.reset(imgSrc);
          BestImageGuesser.matchBadImageNames.reset(imgSrc);
          
          if (BestImageGuesser.knownJunkImageMatcher.find() || BestImageGuesser.matchBadImageNames.find()) {
            System.out.println("Known Junk image found in top node , skipping " + imgSrc);
          } else {
            System.out.println("Img source: " + imgSrc);
            bestImageSrc = imgSrc;
          }
        }
      }
      if (bestImageSrc == null) {
        bestImageSrc = metaImageUrl;
      } else {
        if (bestImageSrc.equals(metaImageUrl)) {
          // do nothing as both are pointing to same
        } else {
          // TODO: should I give more weight to meta
          //bestImageSrc = metaImageUrl;// for website like
          // denverpost.com it works well
        }
      }
    }
    return bestImageSrc;
  }

  public NewsFeed buildNewsFeed(String id, String url, String html, long timestamp) {
    try{
    // ArticleSummary summary = summarization.getSummary(url, html);
    ArticleSummary articleSummary = summarization.getArticleSummary(html);

    Article article = articleSummary.getArticle();
    
/*    String text = article.getCleanedDocument().text();
    try {
      // If lang is other than en then dont process it.
      String lang = langDetector.detectLanguage(text);
      if (lang!=null && !lang.equals("en")) {
        return null;
      }
    } catch (LangDetectException e) {
      e.printStackTrace();
    }
*/
    NewsFeed newsFeed = new NewsFeed();
    newsFeed.title = article.getTitle();
    // newsFeed.source = article.get();

    newsFeed.imagePath = getBestImage(article);

    String normalized = null;
    try {
      normalized = com.rujhaan.rocky.util.v1.URLNormalizer.normalize(url);
    } catch (MalformedURLException e2) {
      e2.printStackTrace();
      // if normalization fails
      normalized = url;
    }

    if (normalized == null) {
      // if normalization fails
      normalized = url;
    }

    newsFeed.link = normalized;
    Set<String> tags = article.getTags();
    if (tags.contains("STAY CONNECTED")) {
      tags.remove("STAY CONNECTED");
    }
    if (tags.contains("ALSO READ")) {
      tags.remove("ALSO READ");
    }

    newsFeed.tags = tags;

    String keywords = article.getMetaKeywords();

    Set<String> values = new HashSet<>();
    if (keywords != null) {
      String[] arr = keywords.split(",");
      for (String a : arr) {
        values.add(a.trim());
      }
    }
    newsFeed.keywords = values;
    newsFeed.description = article.getMetaDescription();
    newsFeed.postedAt = timestamp;

    String summary = summarization.getTextSummary(article);
    
    if (summary != null) {
      //System.out.println("summary11:"+summary);

      summary = summary.replaceAll("\u00a0"," ");
      //System.out.println("summary22:"+summary);

      for (String sentence : filterSentences) {
        //System.out.println("sentence:"+sentence);
        summary = summary.replaceAll(sentence, " ");
        //System.out.println("summary:"+summary);
      }
      //System.out.println("summary33:"+summary);
      summary = summary.replaceAll("\\s+", " ");
      summary = summary.trim();
      //System.out.println("summary:"+summary);
    }
    //
    newsFeed.summary = summary;
   
    if (newsFeed.summary.length() < 300) {
      System.out.println("Skipping this record as data isnot enough:" + url);
      return null;
    }
    
    String socialMeasureUrl=null;
    
    String canoncial = article.getCanonicalLink();
    if (canoncial != null) {
      socialMeasureUrl = canoncial;
    } else {
      socialMeasureUrl = newsFeed.link;
    }
    System.out.println("----------------socialMeasureUrl:"+socialMeasureUrl);
    try {
      double textScore = Scorer.getTextQualityScore(newsFeed.summary);
      double socialScore = Scorer.getTextQualityScore(socialMeasureUrl);

      System.out.println("----------------textScore:"+textScore+"--"+socialScore);

      double imageBoost = 0;
      if (newsFeed.imagePath != null && newsFeed.imagePath.trim().length() > 0) {
        String imageFilePath = imageExtractor.downloadImageToTempFile(socialMeasureUrl, newsFeed.imagePath);
        if (imageFilePath != null) {
          try {
            ImageInfo imgInfo = Scorer.getImageQualityScore(imageFilePath);
            // drop this records, as image size is not proper.
            if (imgInfo != null && imgInfo.isDropRecord() == true) {
              return null;
            }
            imageBoost = imgInfo.getImageBoost();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      System.out.println("----------------textScore:"+textScore+"--"+socialScore+"--"+imageBoost);
      
      // temporary only text score
      newsFeed.score = Scorer.getCombinedScore(socialScore, textScore, imageBoost);
      System.out.println("newsFeed.score:" + newsFeed.score);
    } catch (Exception e) {
      e.printStackTrace();
    }

    Set<String> t_keywords = new HashSet<>();
    
    Map<String, Map<String, Integer>> entityMap = nerExtractor.extractEntities(newsFeed.summary);
    if (entityMap != null && !entityMap.isEmpty()) {
      for (String k : entityMap.keySet()) {
        Map<String, Integer> im = entityMap.get(k);
        t_keywords.addAll(im.keySet());
      }
    }
    
    Map<String, ArrayList<Integer>> list = newsTermExtractor.topics(newsFeed.summary);
    if (list != null && !list.isEmpty()) {
      // t_keywords.addAll(list.keySet());
      for (String t : list.keySet()) {
        if (t != null && !t.isEmpty()) {
          // if the keyword have more than 3 words than discard it, as from the
          // prev experience, more than 3 words are not good keywords.
          String[] parts = t.split(" ");
          if (parts.length <= 3) {
            t_keywords.add(t);
          }
        }
      }
    }
    
    newsFeed.keywords = t_keywords;

    if (newsClassifier != null) {
      try {
        newsFeed.label = newsClassifier.classify(newsFeed.summary);
        System.out.println("Classified as :" + newsFeed.label + "===>" + newsFeed.summary);
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    }
    
    if (locationClassifier != null) {
      try {
        newsFeed.country = locationClassifier.classify(newsFeed.summary);
        System.out.println("Classified as :" + newsFeed.country + "===>" + newsFeed.summary);
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    }
    
    newsFeed.id = id;
    return newsFeed;
    }catch(Exception e){
      e.printStackTrace();
    }
    return null;
  }
  
  private void addToDB(NewsFeed newsFeed) {
    DBObject q = new BasicDBObject("id", newsFeed.id);
    DBObject u = DBMapper.fromNewsFeed(newsFeed);
    newsfeeddao.upsert(q, u);
    System.out.println("========================<><><>===============================");
  }
  
  private void searchAndAddTweets(List<Status> tweets) {
    // If there is no valid tweets.
    if (tweets == null || tweets.size() == 0) {
      return;
    }
    for (Status tweet : tweets) {
      tweetStatusHandler.handle(tweet, false, true);
    }
    System.out.println("========================<><><>===============================");
  }

  private static AtomicInteger counter = new AtomicInteger();

  private void index(NewsFeed newsFeed) {
    SolrInputDocument solrDoc = SolrDocMapper.toSolrDoc(newsFeed);
    if (connector != null) {
      try {
        connector.addDocument(solrDoc);
      } catch (SolrServerException | IOException e) {
        e.printStackTrace();
      }
      if (counter.get() % 10 == 0) {
        try {
          connector.commit();
        } catch (IndexingException e) {
          e.printStackTrace();
        }
      }
    }
  }
  
  public void publishToSocialChannels(NewsFeed newsFeed) {
    try {
      queue.put(newsFeed);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  
  public static BlockingQueue<NewsFeed> queue = new ArrayBlockingQueue<NewsFeed>(100);
  
  public static Publisher[] publishers = null;//{new FacebookPublisher()};

  public class PublisherThread implements Runnable {

    @Override
    public void run() {
      if (publishers != null) {
        while (true) {
          try {
            NewsFeed feed = queue.take();
            System.out.println("queue size:"+queue.size());
            for (Publisher publisher : publishers) {
              publisher.publish(feed);
            }
            Thread.sleep(60000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }
  }
  
  public class PublisherThreadQueueSizePrinter implements Runnable {

    @Override
    public void run() {
      if (publishers != null) {
        while (true) {
          try {
            System.out.println("queue size:"+queue.size());
            Thread.sleep(30000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }
  }

  public static void main(String[] args) {
    
    List<String> ls = FileUtil.loadResource(SummarizationHandler.class, "resources/filter_sentences.txt", true);
    for (String s : ls) {
      s = s.trim();
      if (s.length() > 0) {
        if(s.startsWith("#")){
          continue;//skip that line
        }
      }
    }
    System.out.println(ls);
    
//    String summary = "File photo of Tamil Nadu Chief Minister #Kindle Headlines Today | J Jayalalithaa. ";
//    summary = summary.replaceAll("Headlines Today \\|", " ");
//    summary = summary.replaceAll("DEVELOPING:", " ");
//    summary = summary.replaceAll("#Kindle", " ");
//    summary = summary.replaceAll("\\s+", " ");
//    
//   /* SummarizationHandler handler = new SummarizationHandler();
//    
//    for (String s : handler.filterSentences) {
//      summary = summary.replaceAll(s, " ");
//      System.out.println(summary);
//      System.out.println(s);
//    }*/
//    System.out.println(summary);
  }

}
