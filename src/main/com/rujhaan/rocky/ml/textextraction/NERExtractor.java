package com.rujhaan.rocky.ml.textextraction;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;

/**
 * This is a demo of calling CRFClassifier programmatically.
 * <p>
 * Usage: {@code java -mx400m -cp "stanford-ner.jar:." NERDemo [serializedClassifier [fileName]] }
 * <p>
 * If arguments aren't specified, they default to classifiers/english.all.3class.distsim.crf.ser.gz and some hardcoded
 * sample text.
 * <p>
 * To use CRFClassifier from the command line:
 * </p>
 * <blockquote>
 * {@code java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier [classifier] -textFile [file] }
 * </blockquote>
 * <p>
 * Or if the file is already tokenized and one word per line, perhaps in a tab-separated value format with extra columns
 * for part-of-speech tag, etc., use the version below (note the 's' instead of the 'x'):
 * </p>
 * <blockquote>
 * {@code java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier [classifier] -testFile [file] }
 * </blockquote>
 * 
 * @author Jenny Finkel
 * @author Christopher Manning
 */

public class NERExtractor {

  private static AbstractSequenceClassifier<CoreLabel> classifier;

  public NERExtractor() {

  }
  
  public static String confDir = "resources/";

  static {
    String serializedClassifier = confDir+"resclassifiers/english.all.3class.distsim.crf.ser.gz";
    // this seems to be working better than above
    serializedClassifier = confDir+"classifiers/english.nowiki.3class.distsim.crf.ser.gz";
    if (classifier == null) {
      try {
        classifier = CRFClassifier.getClassifier(serializedClassifier);
      } catch (ClassCastException | ClassNotFoundException | IOException e) {
        e.printStackTrace();
      }
    }
  }

  public Map<String, Map<String, Integer>> extractEntities(String text) {
    Map<String, Map<String, Integer>> entities = new HashMap<String, Map<String, Integer>>();

    for (List<CoreLabel> lcl : classifier.classify(text)) {
      Iterator<CoreLabel> iterator = lcl.iterator();
      if (!iterator.hasNext()) continue;
      CoreLabel cl = iterator.next();

      while (iterator.hasNext()) {
        String answer = cl.getString(CoreAnnotations.AnswerAnnotation.class);

        if (answer.equals("O")) {
          cl = iterator.next();
          continue;
        }

        if (!entities.containsKey(answer)) entities.put(answer, new HashMap<String, Integer>());

        String value = cl.getString(CoreAnnotations.ValueAnnotation.class);

        while (iterator.hasNext()) {
          cl = iterator.next();
          if (answer.equals(cl.getString(CoreAnnotations.AnswerAnnotation.class)))
            value = value + " " + cl.getString(CoreAnnotations.ValueAnnotation.class);
          else {
            if (!entities.get(answer).containsKey(value)) {
              entities.get(answer).put(value, 0);
            }
            entities.get(answer).put(value, entities.get(answer).get(value) + 1);
            break;
          }
        }
        if (!iterator.hasNext()) {
          break;
        }
      }
    }
    return entities;
  }

  public static void main(String[] args) {
    NERExtractor extractor = new NERExtractor();
    //System.out.println(extractor.extractEntities("Why did you build Rao Tula Ram as a single flyover? asks High Court to PWD http://t.co/XieIzBzQkz"));

    String text= "Why did you build Rao Tula Ram as a single flyover? asks High Court to PWD http://t.co/XieIzBzQkz";
    text="Safeguarding social security benefits of building and construction workers — the second largest worker group in the country — the Delhi High Court has ruled that employers can not withhold PF contributions of such workers on the grounds that the Employees’ Provident Fund Organisation (EPFO) does not have adequate facilities for such migrant employees. The move is a major blow to the building and construction industry, which has contended for many years that provident fund deductions for the mobile and casual workers in the sector are not feasible as the government can’t keep track of them. ";
    
    Map<String, Map<String, Integer>> entityMap = extractor.extractEntities(text);
    if (entityMap != null && !entityMap.isEmpty()) {
      for (String k : entityMap.keySet()) {
        Map<String, Integer> im = entityMap.get(k);
        System.out.println(k + "=>" + im.keySet());
      }
    } else {
      System.out.println("No NER found");
    }
  }
}
