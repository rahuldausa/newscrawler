package com.rujhaan.rocky.ml.textextraction;

import java.util.Set;

import com.rujhaan.rocky.domain.ArticleSummary;
import com.sree.textbytes.readabilityBUNDLE.Article;

public interface ISummarization {

  Set<String> getTerms(String text);

  String getTextSummary(Article article);

  ArticleSummary getArticleSummary(String html);

}
