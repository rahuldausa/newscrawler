package com.rujhaan.rocky.ml.textextraction;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.bson.types.ObjectId;
import org.jsoup.nodes.Element;

import twitter4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.rujhaan.news.domain.NewsFeed;
import com.rujhaan.news.impl.dao.DBMapper;
import com.rujhaan.news.impl.dao.MongoDao;
import com.rujhaan.rocky.domain.ArticleSummary;
import com.rujhaan.rocky.domain.Text;
import com.rujhaan.rocky.feed.CommonException;
import com.rujhaan.rocky.indexing.IndexingException;
import com.rujhaan.rocky.indexing.SolrConnector;
import com.rujhaan.rocky.indexing.SolrDocMapper;
import com.rujhaan.rocky.ml.classification.LocationClassifier;
import com.rujhaan.rocky.ml.classification.NewsClassifierV2;
import com.rujhaan.rocky.util.Utils;
import com.rujhaan.rocky.util.v1.FileUtil;
import com.sree.textbytes.jtopia.NewsTermsExtractor;
import com.sree.textbytes.jtopia.TermDocument;
import com.sree.textbytes.jtopia.TermsExtractor;
import com.sree.textbytes.readabilityBUNDLE.Article;
import com.sree.textbytes.readabilityBUNDLE.ContentExtractor;
import com.sree.textbytes.readabilityBUNDLE.image.Image;

public class GooseSummarization implements ISummarization{

  public static final Logger log = Logger.getLogger(GooseSummarization.class);
  
 /* public ArticleSummary getSummary(String url, String html) {
    Article article = new Article();

    ContentExtractor ce = new ContentExtractor();
    //article = ce.extractContent(html, "ReadabilitySnack");
    // article = ce.extractContent(html, "ReadabilityGoose");
    article = ce.extractContent(html, "ReadabilityCore");

 /*   System.out.println("url:"+url);
    System.out.println("Image:"+article.getTopImage());
    System.out.println("topNode:"+article.getTopNode());
    System.out.println("tags:"+article.getTags());
    System.out.println("title:"+article.getTitle());
    System.out.println("keywords:"+article.getMetaKeywords());
    System.out.println("descrition:"+article.getMetaDescription());*/
    
    /*ArticleSummary summary = new ArticleSummary();
    summary.article = article;

    List<Text> texts = new ArrayList<Text>();
    for (Element e : article.getCleanedDocument().getAllElements()) {
      // System.out.println(e.attr("algoscore") + "--" + e.text());
      Text t = new Text();
      String val = e.attr("algoscore");
      if (val != null && !val.isEmpty()) {
        t.score = Double.parseDouble(val);
        t.content = e.text();
        texts.add(t);
      } else {
        // System.err.println(e.attr("algoscore") + "--" + e.text());
      }
    }
    
    summary.texts = texts;
*/
/*    Collections.sort(texts, Collections.reverseOrder(new Comparator<Text>() {

      @Override
      public int compare(Text t1, Text t2) {
        if (t1.score > t2.score)
          return 1;
        else if (t1.score < t2.score)
          return -1;
        else return 0;
      }
    }));*/

    /*return summary;
  }*/
  
  @Override
  public ArticleSummary getArticleSummary(String html) {
    Article article = new Article();

    ContentExtractor ce = new ContentExtractor();
    article = ce.extractContent(html, "ReadabilityGoose");

    Image topImage = article.getTopImage();
    
    if (topImage != null) {
      System.out.println("topImage:" + topImage);
/*      System.out.println("Image src:" + topImage.getImageSrc() + " topImage node "
          + topImage.getTopImageNode());*/
      //System.out.println("topNode:" + article.getTopNode());
    }
    /*   System.out.println("tags:"+article.getTags());
    System.out.println("title:"+article.getTitle());
    System.out.println("keywords:"+article.getMetaKeywords());
    System.out.println("descrition:"+article.getMetaDescription());*/
    
    ArticleSummary summary = new ArticleSummary();
    summary.setArticle(article);

    List<Text> texts = new ArrayList<Text>();
    for (Element e : article.getCleanedDocument().getAllElements()) {
      //System.out.println(e.attr("algoscore") + "--" + e.text());
      Text t = new Text();
      String val = e.attr("algoscore");
      if (val != null && !val.isEmpty()) {
        t.score = Double.parseDouble(val);
        t.content = e.text();
        texts.add(t);
      } else {
        // System.err.println(e.attr("algoscore") + "--" + e.text());
      }
    }
    summary.setTexts(texts);
    return summary;
  }

  @Override
  public String getTextSummary(Article article){
    List<Text> texts = new ArrayList<Text>();
    if (article.getTopNode() != null) {
      for (Element e : article.getTopNode().getAllElements()) {
        //System.out.println(e.attr("algoscore") + "--" + e.text());
        Text t = new Text();
        String val = e.attr("algoscore");
        if (val != null && !val.isEmpty()) {
          t.score = Double.parseDouble(val);
          t.content = e.text();
          texts.add(t);
        } else {
          // System.err.println(e.attr("algoscore") + "--" + e.text());
        }
      }
    }

    StringBuilder contentBuf = new StringBuilder();
    // System.out.println("url" + "=>" + url);
    for (Text text : texts) {
      // System.out.println(text.score + "=>" + text.content);
      String content = text.content;
      // combined length
      if (contentBuf.length() + content.length() < 600) {
        contentBuf.append(content).append(" ");
      }
    }

    String summary = contentBuf.toString();
    return summary;
  }
  
  @Override
  public Set<String> getTerms(String text) {
    if (text == null || text.isEmpty()) {
      return null;
    }
    TermsExtractor termExtractor = new TermsExtractor();
    TermDocument topiaDoc = new TermDocument();
    topiaDoc = termExtractor.extractTerms(text);
    if (log.isDebugEnabled()) {
      log.debug("Extracted terms : " + topiaDoc.getExtractedTerms());
      log.debug("Final Filtered terms : " + topiaDoc.getFinalFilteredTerms());
    }
    return topiaDoc.getFinalFilteredTerms().keySet();
  }
  
  public static void main(String[] args) {
    //BasicConfigurator.configure();
    NewsTermsExtractor newsTermExtractor = new NewsTermsExtractor();
    GooseSummarization summarization = new GooseSummarization();
    SolrConnector connector = new SolrConnector();
    if (connector != null) {
      connector.open("http://23.227.177.112:8983/solr/c9news");
      //connector.open("http://localhost:8983/solr/c9news");
    }
    MongoDao dao = new MongoDao("newsrawdata");
    //MongoDao dao = new MongoDao("ccrawdata");
    BasicDBObject index = new BasicDBObject("id", -1).append("unique", true);
    dao.addIndex(index);
    
    MongoDao newsfeeddao = new MongoDao("23.227.177.112", "newsfeed");
    //MongoDao newsfeeddao = new MongoDao("127.0.0.1", "newsfeed");
    //MongoDao newsfeeddao = new MongoDao("127.0.0.1", "ccfeed");

    newsfeeddao.addIndex(new BasicDBObject("category_label", -1));//descending
    
    BasicDBObject query = new BasicDBObject("id", -1).append("unique", true);
    newsfeeddao.addIndex(query);

    //DBObject object = new BasicDBObject();
    //Iterator<DBObject> itr = dao.streamResults(object, null);
    BasicDBObject sortBy = new BasicDBObject("_id", -1);
    //BasicDBObject cond =  new BasicDBObject("ts", new BasicDBObject("$gt", 1408831801000L));
    BasicDBObject cond =  new BasicDBObject("_id", new ObjectId("5422c61930043a27a5ddf125"));

    Iterator<DBObject> itr = dao.streamResults(cond, null, sortBy, -1, -1);
    
    NewsClassifierV2 classifier = new NewsClassifierV2(); 
    if (classifier != null) {
      try {
        classifier.trainAndLoadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }
    
    LocationClassifier locationClassifier = new LocationClassifier();
    if (locationClassifier != null) {
      try {
        locationClassifier.trainAndLoadClassifier(false);
      } catch (ClassNotFoundException | IOException e2) {
        e2.printStackTrace();
      }
    }
    Set<String> filterSentences = new HashSet<String>();
    
    List<String> ls = FileUtil.loadResource(SummarizationHandler.class, "filter_sentences.txt", true);
    for (String s : ls) {
      s = s.trim();
      if (s.length() > 0) {
        filterSentences.add(s);
      }
    }
    while (itr.hasNext()) {
      BasicDBObject obj = (BasicDBObject) itr.next();
      String url = obj.getString("url");
      String html = obj.getString("html");
      
      String temp = obj.getString("ts");
      Long timestamp = null;
      if(temp!=null){
        timestamp = Long.parseLong(temp);
      }

      String id = null;
      try {
        id = Utils.uniqueId(url);
      } catch (CommonException e1) {
        e1.printStackTrace();
      }
      
      DBObject q = new BasicDBObject("id", id);

  /*    if (newsfeeddao.exists(q)) {
        continue;
      }*/

      //ArticleSummary summary = summarization.getSummary(url, html);
      ArticleSummary asummary = summarization.getArticleSummary(html);

      Article article = asummary.getArticle();
      
      NewsFeed newsFeed = new NewsFeed();
      newsFeed.title = article.getTitle();
     // newsFeed.source = article.get();
      
      if (article.getTopImage() != null) {
        newsFeed.imagePath = article.getTopImage().getImageSrc();
        System.out.println("img src:"+article.getTopImage().getImageSrc());
      }
      
      String normalized = null;
      try {
        normalized = com.rujhaan.rocky.util.v1.URLNormalizer.normalize(url);
      } catch (MalformedURLException e2) {
        e2.printStackTrace();
        // if normalization fails
        normalized = url;
      }

      if (normalized == null) {
        // if normalization fails
        normalized = url;
      }
      
      newsFeed.link = normalized;
      Set<String> tags = article.getTags();
      if (tags.contains("STAY CONNECTED")) {
        tags.remove("STAY CONNECTED");
      }
      if (tags.contains("ALSO READ")) {
        tags.remove("ALSO READ");
      }

      newsFeed.tags = tags;      
      
      
      String keywords = article.getMetaKeywords();
      
      Set<String> values = new HashSet<>();
      if (keywords != null) {
        String[] arr = keywords.split(",");
        for (String a : arr) {
          values.add(a.trim());
        }
      }
      newsFeed.keywords = values;
      newsFeed.description = article.getMetaDescription();
      newsFeed.postedAt = timestamp;

      String textsummary = summarization.getTextSummary(article);
      System.out.println("summary:"+textsummary);
      
      List<Text> texts = new ArrayList<Text>();
      if (article.getTopNode() != null) {
        for (Element e : article.getTopNode().getAllElements()) {
          // System.out.println(e.attr("algoscore") + "--" + e.text());
          Text t = new Text();
          String val = e.attr("algoscore");
          if (val != null && !val.isEmpty()) {
            t.score = Double.parseDouble(val);
            t.content = e.text();
            texts.add(t);
          } else {
            // System.err.println(e.attr("algoscore") + "--" + e.text());
          }
        }
      }
      
      StringBuilder contentBuf = new StringBuilder();
      //System.out.println("url" + "=>" + url);
      for (Text text : texts) {
        //System.out.println(text.score + "=>" + text.content);
        String content = text.content;
        // combined length
        if (contentBuf.length() + content.length() < 600) {
          contentBuf.append(content).append(" ");
        }
      }
      
      //newsFeed.summary = contentBuf.toString();
      
      String summary = contentBuf.toString();
      
      if (summary != null) {
        //System.out.println("summary11:"+summary);

        summary = summary.replaceAll("\u00a0"," ");
        //System.out.println("summary22:"+summary);

        for (String sentence : filterSentences) {
          //System.out.println("sentence:"+sentence);
          summary = summary.replaceAll(sentence, " ");
          //System.out.println("summary:"+summary);
        }
        //System.out.println("summary33:"+summary);
        summary = summary.replaceAll("\\s+", " ");
      }
      newsFeed.summary = summary;

      if (newsFeed.summary.length() < 100) {
        System.out.println("Skipping this record as data isnot enough:" + url);
        continue;
      }
      
      Map<String, ArrayList<Integer>> list = newsTermExtractor.topics(newsFeed.summary);
      if (list != null && !list.isEmpty()) {
        newsFeed.keywords = list.keySet();
      }
      
      if (classifier != null) {
        try {
          newsFeed.label = classifier.classify(newsFeed.summary);
          System.out.println("Classified as :" + newsFeed.label + "===>" + newsFeed.summary);
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }
      
      if (locationClassifier != null) {
        try {
          newsFeed.country = locationClassifier.classify(newsFeed.summary);
          System.out.println("Classified as :" + newsFeed.country + "===>" + newsFeed.summary);
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }

      newsFeed.id = id;
      
      DBObject u = DBMapper.fromNewsFeed(newsFeed);

      newsfeeddao.upsert(q, u);
      
      System.out.println("========================<><><>===============================");
/*      Map<String, Object> map = u.toMap();
      Doc d = new Doc();
      for (String key : map.keySet()) {
        Object val = map.get(key);
        //System.out.println(key + " => " + val);
        if (!(key.equals("id") || key.equals("summary"))) {
          d.add(key + "_ss", val);
        } else {
          if (key.equals("summary")) {
            d.add(key + "_fss", val);
          } else {
            d.add(key, val);
          }
        }
        //System.out.println("params:"+d.params);
      }*/
      SolrInputDocument solrDoc = SolrDocMapper.toSolrDoc(newsFeed);
      if (connector != null) {
        try {
          connector.addDocument(solrDoc);
        } catch (SolrServerException | IOException e) {
          e.printStackTrace();
        }
      }
    }

    if (connector != null) {
      try {
        connector.commit();
      } catch (IndexingException e) {
        e.printStackTrace();
      }
    }
  }
}
