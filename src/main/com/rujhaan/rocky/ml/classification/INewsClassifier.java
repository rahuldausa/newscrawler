package com.rujhaan.rocky.ml.classification;

import java.io.IOException;

import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.lm.NGramProcessLM;

public interface INewsClassifier extends IClassifier{

  public DynamicLMClassifier<NGramProcessLM> train() throws IOException, ClassNotFoundException;
  

}
