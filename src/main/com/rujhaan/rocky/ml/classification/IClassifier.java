package com.rujhaan.rocky.ml.classification;

import java.io.IOException;

public interface IClassifier {

  public String classify(String text) throws IOException;

  public void trainAndLoadClassifier(boolean b) throws IOException, ClassNotFoundException;
}
