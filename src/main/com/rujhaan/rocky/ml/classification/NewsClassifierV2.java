package com.rujhaan.rocky.ml.classification;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.ConfusionMatrix;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.JointClassifier;
import com.aliasi.classify.JointClassifierEvaluator;
import com.aliasi.classify.NaiveBayesClassifier;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.tokenizer.NGramTokenizerFactory;
import com.aliasi.tokenizer.RegExFilteredTokenizerFactory;
import com.aliasi.tokenizer.Tokenizer;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.Files;

public class NewsClassifierV2 implements INewsClassifier{

  
  private static File TRAINING_DIR_20NEWSGROUP_DATASET = new File("/Users/ivy4488/Documents/d/work/c9news/text_data1");
  
  private static File TRAINING_DIR = TRAINING_DIR_20NEWSGROUP_DATASET;//new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/fourNewsGroups/4news-train");

  private JointClassifier<CharSequence> compiledClassifier;
  
//  private File trainedClassifiedModel = new File("/User/ivy4488/d/ProjectsHub/lingpipe-4.1.0/demos/data/classifier_model5");
  private File trainedClassifiedModel = new File("model/classifier_model5");

  //private static File TESTING_DIR = new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/fourNewsGroups/4news-test");

/*  private static String[] CATEGORIES = {"soc.religion.christian", "talk.religion.misc",
      "alt.atheism", "misc.forsale"};*/
  
/*  private static String[] CATEGORIES = {"alt.atheism", "comp.graphics", "comp.os.ms-windows.misc",
      "comp.sys.ibm.pc.hardware", "comp.sys.mac.hardware", "comp.windows.x", "misc.forsale",
      "rec.autos", "rec.motorcycles", "rec.sport.baseball", "rec.sport.hockey", "sci.crypt",
      "sci.electronics", "sci.med", "sci.space", "soc.religion.christian", "talk.politics.guns",
      "talk.politics.mideast", "talk.politics.misc", "talk.religion.misc"};
*/
  // "asia", "bollywood", "books", "business", "cricket", "crime", "economy", "education", "entertainment", "environment", "food", "football", "health", "hollywood", "india", "lifestyle", "money", "politics", "religion", "science", "sports", "technology", "travel", "uk", "usa", "women", "world"

 // private static String[] CATEGORIES = {"asia", "bollywood", "books", "business", "cricket", "crime", "economy", "education", "entertainment", "environment", "food", "football", "health", "hollywood", "india", "lifestyle", "money", "politics", "religion", "science", "sports", "technology", "travel", "uk", "usa", "women", "world"};
  
  private static String[] CATEGORIES = {"fashion", "finance", "world", "bollywood", "books", "business", "cricket",
      "crime", "economy", "education", "entertainment", "environment", "food", "football", "health", "hollywood",
      "lifestyle", "money", "politics", "religion", "science", "sports", "technology", "travel", "women"};
  
  private static int NGRAM_SIZE = 6;

  public static void main(String[] args) throws ClassNotFoundException, IOException {
    String[] f = TRAINING_DIR_20NEWSGROUP_DATASET.list();
    System.out.println(Arrays.toString(f));
    long start = System.currentTimeMillis();
    NewsClassifierV2 classifier = new NewsClassifierV2();
    classifier.trainAndLoadClassifier(false);
    System.out.println("Time taken in loading classifier:"+(System.currentTimeMillis()-start));
    
    start = System.currentTimeMillis();
    String text = "I want to buy a motorcycle to start in to United States";
    //text = "The ASUS Z97-DELUXE is a mid-range socket LGA1150 motherboard based on the new Intel Z97 chipset, supporting the fourth and the forthcoming fifth generation Core i processors. It brings a high-end audio codec, 10 SATA-600 ports (allowing two SATA Express connections), 10 USB 3.0 ports, and Wi-Fi interface. Let�s take a good look at it.";
    //text="LinkedIn today announced that it has acquired Pulse, the popular newsreader for the web and mobile. The transaction, LinkedIn says, is valued at approximately $90 million in a combination of about 90 percent stock and 10 percent cash. The acquisition is expected to close in the second quarter of 2013.";
    //text="LinkedIn argues that it is acquiring Pulse because it wants the site to �be the definitive professional publishing platform � where all professionals come to consume content and where publishers come to share their content. Millions of professionals are already starting their day on LinkedIn to glean the professional insights and knowledge they need to make them great at their jobs.�";
    //text="Stop whatever you're doing, turn off the television, wake the children and salute the flag. A West Los Angeles man has fought City Hall and won. His name is James Buch and he doesn't like me saying it, but he's 83, not that you'd know it from the way he dances salsa. It was his love of dancing, in fact, that tripped him up last month. You may have seen Buch featured in this column a week ago. He'd gone dancing at the Los Angeles County Museum of Art's Saturday night salsa fest, and returned to an empty space where he'd parked his car on 6th Street near Fairfax. Huh? DOT came to its senses. ";
    //text="By Anthony Deutsch AMSTERDAM (Reuters) - The Netherlands or Malaysia is likely to try those responsible for the downing in Ukraine of Malaysia Airlines flight MH17, not the International Criminal Court in The Hague, the Dutch said on Wednesday. Cases are only referred to the world's permanent war crimes court if countries involved are unable or unwilling to prosecute atrocities, Justice Minister Ivo Opstelten said in a letter to parliament. The Netherlands has universal jurisdiction for war crimes and can prosecute suspects in other countries. (Editing by Janet Lawrence and Susan Fenton) ";
    text="New Delhi : \"Scandalous allegations\" made against Prime Minister Narendra Modi must be deleted, the Supreme Court has ordered Pradeep Sharma, the suspended bureaucrat from Gujarat who has accused Mr Modi of ordering the police to illegally spy on a woman. . The controversy was seized by the opposition Congress in the campaign for the national elections to allege that Mr Modi broke the law.";
    //text="Summary Commonwealth Games 2014: Rajeev Mehta Mulling Legal Action Against Scotland Police. New Delhi: Indian Olympic Association (IOA) secretary general Rajeev Mehta on Wednesday reitirated that there was some serious confusion as no charges were framed against him in the alleged drunk-driving case in Glasgow and is now mulling legal action against Scotland Police for damaging his reputation.";
    //text=" Philips, the Sang Fei-owned smartphone brand, has launched two new handsets in the Indian market - the Philips S388 and Philips I928. The Philips S388 comes with a 5-megapixel rear camera alongside a built-in flash.";
    text=".@JanelParrish met Baymax from #BigHero6! Don't miss the move in theaters today! http://t.co/JlOzpPx76P";
    String category = classifier.classify(text);
    System.out.println("Time taken in classifying:"+(System.currentTimeMillis()-start));

    System.out.println(category);
  }

  public void trainAndLoadClassifier(boolean shouldTrain) throws IOException, ClassNotFoundException {
    // compiling
    System.out.println("Loading classifier");
    // we created object so know it's safe
    //compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable.compile(classifier);
    if (shouldTrain) {
      compiledClassifier = train();
    } else {
      if (!trainedClassifiedModel.exists()) {
        compiledClassifier = train();
      } else {
        compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable
            .readObject(trainedClassifiedModel);
      }
    }
  }
  
  public DynamicLMClassifier<NGramProcessLM> train() throws IOException, ClassNotFoundException {
    System.out.println(TRAINING_DIR.getAbsolutePath());
    DynamicLMClassifier<NGramProcessLM> classifier = DynamicLMClassifier.createNGramProcess(
        CATEGORIES, NGRAM_SIZE);
    
//    Pattern regexPattern = Pattern.compile("");
//    TokenizerFactory defaultFactory = new TokenizerFactory() {
//
//      @Override
//      public Tokenizer tokenizer(char[] cs, int start, int length) {
//        Tokenizer tokenizer = new NGramTokenizerFactory(2, 6).tokenizer(cs, start, length);
//        return tokenizer;
//      }
//    };
//    TokenizerFactory tF = new RegExFilteredTokenizerFactory(defaultFactory, regexPattern);
    
    //DynamicLMClassifier<NGramProcessLM> classifier = NaiveBayesClassifier.createTokenized(arg0, arg1, arg2)

    for (int i = 0; i < CATEGORIES.length; ++i) {
      File classDir = new File(TRAINING_DIR, CATEGORIES[i]);
      if (!classDir.isDirectory()) {
        String msg = "Could not find training directory=" + classDir
            + "\nHave you unpacked 4 newsgroups?";
        System.out.println(msg); // in case exception gets lost in shell
        throw new IllegalArgumentException(msg);
      }

      String[] trainingFiles = classDir.list();
      for (int j = 0; j < trainingFiles.length; ++j) {
        File file = new File(classDir, trainingFiles[j]);
        String text = Files.readFromFile(file, "ISO-8859-1");
        System.out.println("Training on " + CATEGORIES[i] + "/" + trainingFiles[j]);
        Classification classification = new Classification(CATEGORIES[i]);
        Classified<CharSequence> classified = new Classified<CharSequence>(text, classification);
        classifier.handle(classified);
      }
    }
    // compiling
    System.out.println("Compiling");
    // we created object so know it's safe
    //compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable.compile(classifier);
    AbstractExternalizable.compileTo(classifier, trainedClassifiedModel);
    return classifier;
  }

  public String classify(String text) throws IOException {
    JointClassification jc = compiledClassifier.classify(text);
    String bestCategory = jc.bestCategory();
    //System.out.println("Got best category of: " + bestCategory);
    /*String details = jc.toString();
    System.out.println(jc.toString());
    System.out.println("---------------");*/
    return bestCategory;
  }
}
