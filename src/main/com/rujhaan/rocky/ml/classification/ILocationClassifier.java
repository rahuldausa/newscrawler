package com.rujhaan.rocky.ml.classification;

import java.io.IOException;

import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.lm.NGramProcessLM;

public interface ILocationClassifier extends IClassifier {

  // may be we want to choose another classsifer for Location other than
  // NGramProcess
  public DynamicLMClassifier<NGramProcessLM> train() throws IOException, ClassNotFoundException;

}
