package com.rujhaan.rocky.ml.classification;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.JointClassifier;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.Files;

public class LocationClassifier implements ILocationClassifier {
  
  //private static File TRAINING_DIR_20NEWSGROUP_DATASET = new File("d:/work/c9news/text_data1");
  private static File TRAINING_DIR_20NEWSGROUP_DATASET = new File("/Users/ivy4488/Documents/d/work/c9news/text_data1");

  private static File TRAINING_DIR = TRAINING_DIR_20NEWSGROUP_DATASET;//new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/fourNewsGroups/4news-train");

  private static JointClassifier<CharSequence> compiledClassifier;
  
  //private File trainedClassifiedModel = new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/location_classifier_model5");
  private File trainedClassifiedModel = new File("model/location_classifier_model5");

  //private static File TESTING_DIR = new File("D:/ProjectsHub/lingpipe-4.1.0/demos/data/fourNewsGroups/4news-test");

/*  private static String[] CATEGORIES = {"soc.religion.christian", "talk.religion.misc",
      "alt.atheism", "misc.forsale"};*/
  
/*  private static String[] CATEGORIES = {"alt.atheism", "comp.graphics", "comp.os.ms-windows.misc",
      "comp.sys.ibm.pc.hardware", "comp.sys.mac.hardware", "comp.windows.x", "misc.forsale",
      "rec.autos", "rec.motorcycles", "rec.sport.baseball", "rec.sport.hockey", "sci.crypt",
      "sci.electronics", "sci.med", "sci.space", "soc.religion.christian", "talk.politics.guns",
      "talk.politics.mideast", "talk.politics.misc", "talk.religion.misc"};
*/
  // "asia", "bollywood", "books", "business", "cricket", "crime", "economy", "education", "entertainment", "environment", "food", "football", "health", "hollywood", "india", "lifestyle", "money", "politics", "religion", "science", "sports", "technology", "travel", "uk", "usa", "women", "world"

 // private static String[] CATEGORIES = {"asia", "bollywood", "books", "business", "cricket", "crime", "economy", "education", "entertainment", "environment", "food", "football", "health", "hollywood", "india", "lifestyle", "money", "politics", "religion", "science", "sports", "technology", "travel", "uk", "usa", "women", "world"};
  
//  private static String[] CATEGORIES = {"fashion","finance","world", "bollywood", "books", "business", "cricket", "crime", "economy", "education", "entertainment", "environment", "food", "football", "health", "hollywood", "lifestyle", "money", "politics", "religion", "science", "sports", "technology", "travel", "women"};
  
  private static String[] CATEGORIES = {"india","usa","uk", "world"};

  private static int NGRAM_SIZE = 6;

  public static void main(String[] args) throws ClassNotFoundException, IOException {
    String[] f = TRAINING_DIR_20NEWSGROUP_DATASET.list();
    System.out.println(Arrays.toString(f));
    long start = System.currentTimeMillis();
    LocationClassifier classifier = new LocationClassifier();
    classifier.trainAndLoadClassifier(false);
    System.out.println("Time taken in loading classifier:"+(System.currentTimeMillis()-start));
    
    start = System.currentTimeMillis();
    String text = "I want to buy a motorcycle to start in to United States";
    //text = "The ASUS Z97-DELUXE is a mid-range socket LGA1150 motherboard based on the new Intel Z97 chipset, supporting the fourth and the forthcoming fifth generation Core i processors. It brings a high-end audio codec, 10 SATA-600 ports (allowing two SATA Express connections), 10 USB 3.0 ports, and Wi-Fi interface. Let�s take a good look at it.";
    //text="LinkedIn today announced that it has acquired Pulse, the popular newsreader for the web and mobile. The transaction, LinkedIn says, is valued at approximately $90 million in a combination of about 90 percent stock and 10 percent cash. The acquisition is expected to close in the second quarter of 2013.";
    //text="LinkedIn argues that it is acquiring Pulse because it wants the site to �be the definitive professional publishing platform � where all professionals come to consume content and where publishers come to share their content. Millions of professionals are already starting their day on LinkedIn to glean the professional insights and knowledge they need to make them great at their jobs.�";
    //text="Stop whatever you're doing, turn off the television, wake the children and salute the flag. A West Los Angeles man has fought City Hall and won. His name is James Buch and he doesn't like me saying it, but he's 83, not that you'd know it from the way he dances salsa. It was his love of dancing, in fact, that tripped him up last month. You may have seen Buch featured in this column a week ago. He'd gone dancing at the Los Angeles County Museum of Art's Saturday night salsa fest, and returned to an empty space where he'd parked his car on 6th Street near Fairfax. Huh? DOT came to its senses. ";
    //text="By Anthony Deutsch AMSTERDAM (Reuters) - The Netherlands or Malaysia is likely to try those responsible for the downing in Ukraine of Malaysia Airlines flight MH17, not the International Criminal Court in The Hague, the Dutch said on Wednesday. Cases are only referred to the world's permanent war crimes court if countries involved are unable or unwilling to prosecute atrocities, Justice Minister Ivo Opstelten said in a letter to parliament. The Netherlands has universal jurisdiction for war crimes and can prosecute suspects in other countries. (Editing by Janet Lawrence and Susan Fenton) ";
    text="New Delhi : \"Scandalous allegations\" made against Prime Minister Narendra Modi must be deleted, the Supreme Court has ordered Pradeep Sharma, the suspended bureaucrat from Gujarat who has accused Mr Modi of ordering the police to illegally spy on a woman. . The controversy was seized by the opposition Congress in the campaign for the national elections to allege that Mr Modi broke the law.";
    text="Scandalous allegations\" made against Prime Minister Narendra Modi must be deleted, the Supreme Court has ordered Pradeep Sharma, the suspended bureaucrat from Gujarat who has accused Mr Modi of ordering the police to illegally spy on a woman. . The controversy was seized by the opposition Congress in the campaign for the national elections to allege that Mr Modi broke the law.";
    text = "A footballer has died after being hit in the head by an object thrown from the crowd at a top-flight league game in Algeria. The Confederation of African Football said Cameroonian striker Albert Ebosse died in hospital Saturday night after he was struck at the match between JS Kabylie and USM Alger in the northern city of Tizi Ouzou. Ebosse's club JS Kabylie said the 24-year-old forward suffered the head injury when he was hit at the end of the game. He had scored in the 2-1 home loss and was the leading scorer in the Algerian league last season. STAY CONNECTED WITH US ON read more...";
    text = "DUBAI, Aug 24 (Reuters) - Kidnappers in Syria have freed a U.S. journalist missing since 2012, U.S. Secretary of State John Kerry said on Sunday, following what Qatari-owned Al Jazeera said were efforts by the Gulf Arab state to win his release. Kerry said in a statement announcing the release of Theo Curtis that the United States was using \"every diplomatic, intelligence and military tool\" at its disposal to secure the release of other Americans held hostage in Syria. A Qatari source told Reuters Curtis had been handed over to a representative of the United Nations in Syria. read more...";
    text = "To find out more about Facebook commenting please read the Conversation Guidelines and FAQs An American held captive for two years by an al-Qaeda-linked group in Syria was released Sunday, according to the Obama administration. \"For two years, we have kept Peter Theo Curtis, a U.S. citizen held hostage in Syria, in our thoughts and prayers,\" White House National Security Adviser Susan Rice said in a statement. \"Today, we join his family and loved ones in welcoming his freedom.\" The New York Times reported Curtis is a journalist and was captured on the Syria-Turkey border in October 2012. read more...";
    text = "The shooting occurred the night before MTV�s Video Music Awards on Sunday evening in Inglewood. MTV said the party was not affiliated with its awards event. The R&B star Chris Brown co-hosted the party at the nightclub with Pia Mia and was inside the club at the time of the shooting, but wasn�t hit by gunfire. Knight has been engaged in a rap feud with East Coast artists over the past two decades and has been in and out of jail due to related parole violations and physical attacks. read more...";
    //text="Summary Commonwealth Games 2014: Rajeev Mehta Mulling Legal Action Against Scotland Police. New Delhi: Indian Olympic Association (IOA) secretary general Rajeev Mehta on Wednesday reitirated that there was some serious confusion as no charges were framed against him in the alleged drunk-driving case in Glasgow and is now mulling legal action against Scotland Police for damaging his reputation.";
    //text=" Philips, the Sang Fei-owned smartphone brand, has launched two new handsets in the Indian market - the Philips S388 and Philips I928. The Philips S388 comes with a 5-megapixel rear camera alongside a built-in flash.";
    String category = classifier.classify(text);
    System.out.println("Time taken in classifying:"+(System.currentTimeMillis()-start));

    System.out.println(category);
  }

  @Override
  public void trainAndLoadClassifier(boolean shouldTrain) throws IOException, ClassNotFoundException {
    // compiling
    System.out.println("Loading classifier");
    // we created object so know it's safe
    //compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable.compile(classifier);
    if (shouldTrain) {
      compiledClassifier = train();
    } else {
      if (!trainedClassifiedModel.exists()) {
        compiledClassifier = train();
      } else {
        compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable
            .readObject(trainedClassifiedModel);
      }
    }
  }
  
  @Override
  public DynamicLMClassifier<NGramProcessLM> train() throws IOException, ClassNotFoundException {
    System.out.println(TRAINING_DIR.getAbsolutePath());
    DynamicLMClassifier<NGramProcessLM> classifier = DynamicLMClassifier.createNGramProcess(
        CATEGORIES, NGRAM_SIZE);

    for (int i = 0; i < CATEGORIES.length; ++i) {
      File classDir = new File(TRAINING_DIR, CATEGORIES[i]);
      if (!classDir.isDirectory()) {
        String msg = "Could not find training directory=" + classDir
            + "\nHave you unpacked 4 newsgroups?";
        System.out.println(msg); // in case exception gets lost in shell
        throw new IllegalArgumentException(msg);
      }

      String[] trainingFiles = classDir.list();
      for (int j = 0; j < trainingFiles.length; ++j) {
        File file = new File(classDir, trainingFiles[j]);
        String text = Files.readFromFile(file, "ISO-8859-1");
        System.out.println("Training on " + CATEGORIES[i] + "/" + trainingFiles[j]);
        Classification classification = new Classification(CATEGORIES[i]);
        Classified<CharSequence> classified = new Classified<CharSequence>(text, classification);
        classifier.handle(classified);
      }
    }
    // compiling
    System.out.println("Compiling");
    // we created object so know it's safe
    //compiledClassifier = (JointClassifier<CharSequence>) AbstractExternalizable.compile(classifier);
    AbstractExternalizable.compileTo(classifier, trainedClassifiedModel);
    return classifier;
  }

  public String classify(String text) throws IOException {
    JointClassification jc = compiledClassifier.classify(text);
    String bestCategory = jc.bestCategory();
    //System.out.println("Got best category of: " + bestCategory);
    /*String details = jc.toString();
    System.out.println(jc.toString());
    System.out.println("---------------");*/
    return bestCategory;
  }
}
