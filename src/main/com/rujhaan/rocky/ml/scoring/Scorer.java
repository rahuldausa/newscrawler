package com.rujhaan.rocky.ml.scoring;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;

import com.camus.feeders.FeedTaskUtil;
import com.ipeirotis.readability.engine.BagOfReadabilityObjects;
import com.ipeirotis.readability.engine.Readability;

public class Scorer {

  //https://www.linkedin.com/countserv/count/share?format=jsonp&url=http%3A%2F%2Fwww.entrepreneur.com%2Farticle%2F241294&callback=gig_pc_linkedin_1420196817745_11389685934409499
  public static class ImageInfo {
    boolean isLandscape;
    double imageBoost;
    boolean dropRecord;
    public boolean isLandscape() {
      return isLandscape;
    }
    public double getImageBoost() {
      return imageBoost;
    }
    public boolean isDropRecord() {
      return dropRecord;
    }
    
    
  }

  //https://www.linkedin.com/countserv/count/share?url=http://www.mstarz.com/articles/44656/20141219/rajon-rondo-dallas-mavericks-boston-celtics-bill-simmons-jalen-rose.htm
  public static ImageInfo getImageQualityScore(String localImagePath) throws IOException {
    double imageBoost = 0;
    File f = new File(localImagePath);
    if (!f.exists()) {
      System.out.println("f does not exists");
      return null;
    }
    // get the BufferedImage, using the ImageIOclass
    BufferedImage image = ImageIO.read(f);
    ImageInfo imageInfoObj = new ImageInfo();

    double pixelsNum = 0;
    pixelsNum = getPixelsCount(image);
    int width = image.getWidth();
    int height = image.getHeight();
    boolean isLandscape = false;
    if (width > height) {
      // is landscape
      isLandscape = true;
    } else {
      isLandscape = false;
    }
    imageInfoObj.isLandscape = isLandscape;
    if (width < 100) {
      imageInfoObj.dropRecord = true;
      return imageInfoObj;
    }
    // use some arbitrary size to determine image is "good", need to take pixel
    // into accounts
    if ((width > 600) || (height > 500)) {
      imageBoost += 0.6;
    }
    // a standard instagram photo is 350k, a typical techcrunch thumbnail is 40k
    System.out.println(localImagePath + ": width, height, pixelsNum: " + width + ", " + height + ", " + pixelsNum);
    if (pixelsNum > 10000) {
      imageBoost += 0.4;
    }
    imageInfoObj.imageBoost = imageBoost;
    return imageInfoObj;
  }
  
  private static int getPixelsCount(BufferedImage image) {
    int w = image.getWidth();
    int h = image.getHeight();
    int count=0;
    for (int i = 0; i < h; i++) {
      for (int j = 0; j < w; j++) {
        count++;
      }
    }
    return count;
  }

  public static double getTextQualityScore(String readabilityText) {
    if (readabilityText == null || readabilityText.trim().length() == 0) {
      return 0;
    }
    BagOfReadabilityObjects readabilityStats = new Readability(readabilityText).getMetrics();
    double normalizedFlesch = (100 - Math.abs(readabilityStats.getFleschReading() - 50)) / 100;
    double normalizedFleschKincaid = (16 - Math.abs(readabilityStats.getFleschReading() - 13)) / 16;
    double normalizedARI = (16 - Math.abs(readabilityStats.getARI() - 13)) / 16;
    double normalizedLiau = (16 - Math.abs(readabilityStats.getColemanLiau() - 13)) / 16;
    double normalizedGunning = (16 - Math.abs(readabilityStats.getGunningFog() - 12)) / 16;
    double normalizedSMOG = (16 - Math.abs(readabilityStats.getSMOGIndex() - 12)) / 16;

    double readabilityMean = (normalizedFlesch + normalizedFleschKincaid + normalizedARI + normalizedLiau
        + normalizedGunning + normalizedSMOG) / 6;
    double mean = Double.valueOf(new DecimalFormat("#.####").format(readabilityMean));
    return mean;
  }

  public static double getSocialScore(String url) {
    // 3000 is very high - http://twitturly.com/ http://t.co/NTPVHVY
    double tweetCount = FeedTaskUtil.fetchTweetCount(url);
    double normalizedTweet = tweetCount / 2000;

    // 600 is very high. 400 is high, ?
    double facebookSharesCount = FeedTaskUtil.fetchFacebookShares(url);
    double normalizedShares = facebookSharesCount / 400;
    double meanSocial = (normalizedTweet + normalizedShares) / 2;

    return meanSocial;
  }

  public static double getCombinedScore(double meanSocial, double readabilityMean, double imageBoost) {
    double boostScore = meanSocial * 0.4 + readabilityMean * 0.3 + imageBoost * 0.3;
    return boostScore;
  }

}
