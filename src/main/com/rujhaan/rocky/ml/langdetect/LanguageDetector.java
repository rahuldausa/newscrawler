package com.rujhaan.rocky.ml.langdetect;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

public class LanguageDetector {

  public LanguageDetector() {
    // String dirname = "profiles/";
    // Enumeration<URL> en = Detector.class.getClassLoader().getResources(
    // dirname);
    // List<String> profiles = new ArrayList<>();
    // if (en.hasMoreElements()) {
    // URL url = en.nextElement();
    // JarURLConnection urlcon = (JarURLConnection) url.openConnection();
    // try (JarFile jar = urlcon.getJarFile();) {
    // Enumeration<JarEntry> entries = jar.entries();
    // while (entries.hasMoreElements()) {
    // String entry = entries.nextElement().getName();
    // if (entry.startsWith(dirname)) {
    // try (InputStream in = Detector.class.getClassLoader()
    // .getResourceAsStream(entry);) {
    // profiles.add(IOUtils.toString(in));
    // }
    // }
    // }
    // }
    // }
    try {
      DetectorFactory.loadProfile("/opt/rujhaan/langdetect-profiles/");
    } catch (LangDetectException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Imp: This method will not correctly detect languages if the text is short. like "Hello world"
   * @param text
   * @return
   * @throws LangDetectException
   */
  public String detectLanguage(String text) throws LangDetectException {
    if(text==null ||text.length()==0){
      return null;
    }
    Detector detector = DetectorFactory.create();
    detector.append(text);
    //detector.setVerbose();
    try {
      String lang = detector.detect();
      return lang;
    } catch (LangDetectException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void main(String[] args) {
    LanguageDetector langDetector = new LanguageDetector();
    for (int i = 0; i < 100; i++) {
      try {
        System.out.println("Lang:" + langDetector.detectLanguage("hallo welt"));
        System.out.println("Lang:" + langDetector.detectLanguage("You’ve got the content. We’ve got the audience."));
      } catch (LangDetectException e) {
        e.printStackTrace();
      }
    }
  }
}
