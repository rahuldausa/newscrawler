package com.ipeirotis.readability.engine;

import java.math.BigDecimal;

import com.ipeirotis.readability.enums.MetricType;

/**
 * Implements various readability indexes
 * 
 * @author Panos Ipeirotis
 * 
 */
public class Readability {

  private static SentenceExtractor se = new SentenceExtractor();

  Integer sentences;

  Integer complex;

  Integer words;

  Integer syllables;

  Integer characters;

  public BagOfReadabilityObjects getMetrics() {
    BagOfReadabilityObjects bo = new BagOfReadabilityObjects();

    bo.setARI(this.getARI());
    bo.setCharacters(this.getCharacters());
    bo.setColemanLiau(this.getColemanLiau());
    bo.setComplexwords(this.getComplex());
    bo.setFleschKincaid(this.getFleschKincaidGradeLevel());
    bo.setFleschReading(this.getFleschReadingEase());
    bo.setGunningFog(this.getGunningFog());
    bo.setSentences(this.getSentences());
    bo.setSMOG(this.getSMOG());
    bo.setSMOGIndex(this.getSMOGIndex());
    bo.setSyllables(this.getSyllables());
    bo.setWords(this.getWords());

    return bo;
  }

  public Readability(String text) {

    // We add the "." for the sentence extractor to pick the last sentence,
    // if it does not end with a punctuation mark.
    this.sentences = getNumberOfSentences(text + ".");
    this.complex = getNumberOfComplexWords(text);
    this.words = getNumberOfWords(text);
    this.syllables = getNumberOfSyllables(text);
    this.characters = getNumberOfCharacters(text);

  }

  public double getMetric(MetricType t) throws IllegalArgumentException {

    switch (t) {
      case SMOG :
        return getSMOG();
      case FLESCH_READING :
        return getFleschReadingEase();
      case FLESCH_KINCAID :
        return getFleschKincaidGradeLevel();
      case ARI :
        return getARI();
      case GUNNING_FOG :
        return getGunningFog();
      case COLEMAN_LIAU :
        return getColemanLiau();
      case SMOG_INDEX :
        return getSMOGIndex();
      case CHARACTERS :
        return getCharacters();
      case SYLLABLES :
        return getSyllables();
      case WORDS :
        return getWords();
      case COMPLEXWORDS :
        return getComplex();
      case SENTENCES :
        return getSentences();

      default :
        throw new IllegalArgumentException("Readability Metric '" + t.toString() + "' not supported");
    }
  }

  private int getCharacters() {
    return characters;
  }

  private int getComplex() {
    return complex;
  }

  private int getSentences() {
    return sentences;
  }

  private int getSyllables() {
    return syllables;
  }

  private int getWords() {
    return words;
  }

  /**
   * Returns true is the word contains 3 or more syllables
   * 
   * @param w
   * @return
   */
  private static boolean isComplex(String w) {
    int syllables = Syllabify.syllable(w);
    return (syllables > 2);
  }

  /**
   * Returns the number of letter characters in the text
   * 
   * @return
   */
  private static int getNumberOfCharacters(String text) {
    String cleanText = cleanLine(text);
    String[] word = cleanText.split(" ");

    int characters = 0;
    for (String w : word) {
      characters += w.length();
    }
    return characters;
  }

  /**
   * Returns the number of words with 3 or more syllables
   * 
   * @param text
   * @return the number of words in the text with 3 or more syllables
   */
  private static int getNumberOfComplexWords(String text) {
    String cleanText = cleanLine(text);
    String[] words = cleanText.split(" ");
    int complex = 0;
    for (String w : words) {
      if (isComplex(w))
        complex++;
    }
    return complex;
  }

  private static int getNumberOfWords(String text) {
    String cleanText = cleanLine(text);
    String[] word = cleanText.split(" ");
    int words = 0;
    for (String w : word) {
      if (w.length() > 0)
        words++;
    }
    return words;
  }

  /**
   * Returns the total number of syllables in the words of the text
   * 
   * @param text
   * @return the total number of syllables in the words of the text
   */
  private static int getNumberOfSyllables(String text) {
    String cleanText = cleanLine(text);
    String[] word = cleanText.split(" ");
    int syllables = 0;
    for (String w : word) {
      if (w.length() > 0) {
        syllables += Syllabify.syllable(w);
      }
    }
    return syllables;
  }

  private static String cleanLine(String line) {
    StringBuffer buffer = new StringBuffer();
    for (int i = 0; i < line.length(); i++) {
      char c = line.charAt(i);
      if (c < 128 && Character.isLetter(c)) {
        buffer.append(c);
      } else {
        buffer.append(' ');
      }
    }
    return buffer.toString().toLowerCase();
  }

  private static int getNumberOfSentences(String text) {
    int l = se.getSentences(text).length;
    if (l > 0)
      return l;
    else if (text.length() > 0)
      return 1;
    else
      return 0;
  }

  /**
   * 
   * http://en.wikipedia.org/wiki/SMOG_Index
   * 
   * @param text
   * @return The SMOG index of the text
   */
  private double getSMOGIndex() {
    double score = Math.sqrt(complex * (30.0 / sentences)) + 3;
    return round(score, 3);
  }

  /**
   * 
   * http://en.wikipedia.org/wiki/SMOG
   * 
   * @param text
   * @return Retugns the SMOG value for the text
   */
  private double getSMOG() {
    double score = 1.043 * Math.sqrt(complex * (30.0 / sentences)) + 3.1291;
    return round(score, 3);
  }

  /**
   * 
   * http://en.wikipedia.org/wiki/Flesch-Kincaid_Readability_Test
   * 
   * @param text
   * @return Returns the Flesch_Reading Ease value for the text
   */
  private double getFleschReadingEase() {

    double score = 206.835 - 1.015 * words / sentences - 84.6 * syllables / words;

    return round(score, 3);
  }

  /**
   * 
   * http://en.wikipedia.org/wiki/Flesch-Kincaid_Readability_Test
   * 
   * @param text
   * @return Returns the Flesch-Kincaid_Readability_Test value for the text
   */
  private double getFleschKincaidGradeLevel() {
    double score = 0.39 * words / sentences + 11.8 * syllables / words - 15.59;
    return round(score, 3);
  }

  /**
   * 
   * http://en.wikipedia.org/wiki/Automated_Readability_Index
   * 
   * @param text
   * @return the Automated Readability Index for text
   */
  private double getARI() {
    System.out.println("-->characters:"+characters+"words:"+words+"sentences:"+sentences);

    double score = 4.71 * characters / words + 0.5 * words / sentences - 21.43;
    System.out.println("-->score"+score);

    return round(score, 3);
  }

  /**
   * 
   * http://en.wikipedia.org/wiki/Gunning-Fog_Index
   * 
   * @param text
   * @return the Gunning-Fog Index for text
   */
  private double getGunningFog() {
    double score = 0.4 * (words / sentences + 100 * complex / words);
    return round(score, 3);
  }

  /**
   * 
   * http://en.wikipedia.org/wiki/Coleman-Liau_Index
   * 
   * @return The Coleman-Liau_Index value for the text
   */
  private double getColemanLiau() {
    System.out.println("-->characters:"+characters+"words:"+words+"sentences:"+sentences);

    double score = (5.89 * characters / words) - (30 * sentences / words) - 15.8;
    System.out.println("-->score"+score);

    return round(score, 3);
  }

  private static Double round(double d, int decimalPlace) {
    // see the Javadoc about why we use a String in the constructor
    // http://java.sun.com/j2se/1.5.0/docs/api/java/math/BigDecimal.html#BigDecimal(double)
    System.out.println("-->"+d);
    BigDecimal bd = new BigDecimal(Double.toString(d));
    System.out.println(bd);

    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
    return bd.doubleValue();
  }

  public static void main(String[] args) {
    String GreenEggsAndHam = "I do not like them in a box. " + "I do not like them with a fox. "
        + "I do not like them in a house. " + "I do not like them with a mouse. "
        + "I do not like them here or there. " + "I do not like them anywhere. " + "I do not like green eggs and ham. "
        + "I do not like them, Sam-I-am.";

    Readability r = new Readability(GreenEggsAndHam);

    System.out.println("SMOG Index :" + r.getSMOGIndex());
    System.out.println("SMOG :" + r.getSMOG());
    System.out.println("Flesch Reading Ease :" + r.getFleschReadingEase());
    System.out.println("Flesch-Kincaid Grade Level :" + r.getFleschKincaidGradeLevel());
    System.out.println("Automated Readability Index :" + r.getARI());
    System.out.println("Gunning-Fog Index :" + r.getGunningFog());
    System.out.println("Coleman-Liau Index :" + r.getColemanLiau());

    System.out.println("\n--------------------------------------------------------\n");
    String logorrhea = "The word logorrhoea is often used pejoratively "
        + "to describe prose that is highly abstract and " + "contains little concrete language. Since abstract "
        + "writing is hard to visualize, it often seems as though "
        + "it makes no sense and all the words are excessive. "
        + "Writers in academic fields that concern themselves mostly "
        + "with the abstract, such as philosophy and especially "
        + "postmodernism, often fail to include extensive concrete "
        + "examples of their ideas, and so a superficial examination "
        + "of their work might lead one to believe that it is all nonsense.";

    r = new Readability(logorrhea);

    System.out.println("SMOG Index :" + r.getSMOGIndex());
    System.out.println("SMOG :" + r.getSMOG());
    System.out.println("Flesch Reading Ease :" + r.getFleschReadingEase());
    System.out.println("Flesch-Kincaid Grade Level :" + r.getFleschKincaidGradeLevel());
    System.out.println("Automated Readability Index :" + r.getARI());
    System.out.println("Gunning-Fog Index :" + r.getGunningFog());
    System.out.println("Coleman-Liau Index :" + r.getColemanLiau());

  }

}