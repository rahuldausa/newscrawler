package com.rujhaan.rocky.ml.classification;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class NewsClassifierV2Test {

  /*@Test
  public void testNewsClassifyTrain() {
    NewsClassifierV2 classifier = new NewsClassifierV2();
    String text = ".@JanelParrish met Baymax from #BigHero6! Don't miss the move in theaters today! http://t.co/JlOzpPx76P";
    try {
      classifier.trainAndLoadClassifier(true);
      String category = classifier.classify(text);
      System.out.println(category);
      Assert.assertEquals("entertainment", category);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }*/
  
  @Test
  public void testNewsClassifyAlreadyTrained() {
    NewsClassifierV2 classifier = new NewsClassifierV2();
    String text = ".@JanelParrish met Baymax from #BigHero6! Don't miss the move in theaters today! http://t.co/JlOzpPx76P";
    try {
      classifier.trainAndLoadClassifier(false);
      String category = classifier.classify(text);
      System.out.println(category);
      Assert.assertEquals("entertainment", category);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }
}
