package com.rujhaan.rocky.ml.scoring;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class ScorerTest {

  @Test
  public void testGetImageQualityScore() {
    try {
      String file = "/Users/ivy4488/Downloads/rujhaan_campaign.jpg";
      file = "/Users/ivy4488/Downloads/i-want-you-transformers.jpg";
      file = "/Users/ivy4488/Downloads/haryana-sisters.jpg";
      file = "/Users/ivy4488/Downloads/565.jpg";

      File f = new File(file);
      double score = Scorer.getImageQualityScore(f.getAbsolutePath()).imageBoost;
      System.out.println(score);
      Assert.assertTrue(score > 0);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
