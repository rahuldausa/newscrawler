package com.rujhaan.rocky.twitter.news.bootstrap;

import org.junit.Test;

public class NewsTweetFetcherTest {

  @Test
  public void testStart() {
    NewsTweetFetcher fetcher = new NewsTweetFetcher();
    fetcher.open(null);
    fetcher.start();

    try {
      Thread.sleep(300*1000L);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
