package com.rujhaan.rocky.twitter.news.bootstrap;

import org.junit.Test;

import com.rujhaan.rocky.twitter.news.bootstrap.NewsTweetFetcher;

public class TwitterNewsFetcherTest {

  @Test
  public void testTwitterNewsFetcher() {
    NewsTweetFetcher newsFetcher = new NewsTweetFetcher();
    newsFetcher.open(null);
    newsFetcher.setShouldLimit(false);
    newsFetcher.start();
    try {
      Thread.sleep(1000000L);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    // now lets stop it.
    newsFetcher.stop();
  }
}
