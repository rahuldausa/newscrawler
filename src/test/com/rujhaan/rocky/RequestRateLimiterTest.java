package com.rujhaan.rocky;

import org.junit.Assert;

import org.junit.Test;

import com.rujhaan.rocky.twitter.RequestRateLimiter;

public class RequestRateLimiterTest {

  @Test
  public void testRateLimit() {
    RequestRateLimiter limiter = new RequestRateLimiter();
    double rate = limiter.limit();
    Assert.assertTrue(rate>0);
  }
}
